const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  publicPath: process.env.VUE_APP_BASE_URL,
  pluginOptions: {
    i18n: {
      locale: 'es',
      fallbackLocale: 'es',
      localeDir: 'lang',
      enableInSFC: true
    }
  },
  chainWebpack: config => {
    config.plugin('html')
      .tap(args => {
          args[0].title = "STORE";
          return args;
      })
  }
})
