export default {
  general: {
    lang: 'Idioma',
    es: 'Español',
    en: 'Ingles'
  },
  validator: {
    required: 'El campo es requerido',
    numeric: 'Solo se aceptan numeros',
    minLength: 'Debe tener por menos {0} digitos',
    maxLength: 'No debe tener mas de {0} digitos',
    decimal: 'Solo se aceptan numeros o numeros con decimales',
    email: 'El correo es incorrecto'
  },
  login: {
    text: 'Por favor iniciar sesión...',
    user: 'Nombre de Usuario',
    password: 'Contraseña',
    send: 'Enviar',
    signin: 'Ingresar'
  }
}
