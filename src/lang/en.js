export default {
  general: {
    lang: 'Language',
    es: 'Spanish',
    en: 'English'
  },
  validator: {
    required: 'Required field',
    numeric: 'Only numbers are accepted',
    minLength: 'Must have at least {0} digits',
    maxLength: 'Must not have more than {0} digits'
  },
  login: {
    textNit: 'Please enter your NIT...',
    text: 'Please login...',
    user: 'User Name',
    password: 'Password',
    send: 'Send',
    signin: 'Sign in'
  }
}
