import es from './es'
import en from './en'

const langs = {
  es,
  en
}

export default langs
