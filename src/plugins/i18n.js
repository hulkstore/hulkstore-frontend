import Vue from 'vue'
import VueI18n from 'vue-i18n'
import loadMessages from '@/lang'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: localStorage.getItem('currentLang') || 'es',
  fallbackLocale: localStorage.getItem('currentLang') || 'es',
  messages: loadMessages
})

export default i18n
