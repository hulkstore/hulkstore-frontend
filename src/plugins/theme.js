import colors from 'vuetify/es5/util/colors'
const theme = {
  light: true,
  themes: {
    light: {
      primary: {
        base: "#263238"
      },
      primary_optional: {
        base: colors.teal.lighten1
      },
      secondary: {
        base: "#424242",
      },
      accent: {
        base: "#82b1ff"
      },
      text: {
        base: "#424242"
      },
      error: {
        base: "#ff5252"
      },
      warning: {
        base: "#fb8c00"
      },
      success: {
        base: "#4caf50"
      },
      info: {
        base: "#2196f3"
      },
      red_light: {
        base: "#FFCDD2"
      },
    }
  }
};

export default theme;
