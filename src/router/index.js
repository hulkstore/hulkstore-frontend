import Vue from "vue";
import VueRouter from "vue-router";
import jwt_decode from "jwt-decode";
//import store from "../store";
Vue.use(VueRouter);
let auth = "";
const loginToAdmin = (to, from, next) => {
  auth = localStorage.getItem("isu-token");
  if (auth) {
    next();
    return;
  }
  next({ name: "login" });
};
const adminToLongin = (to, from, next) => {
  auth = localStorage.getItem("isu-token");
  if (auth) {
    //next({ name: "users" });
    next({ name: "usuarios" });
    return;
  }
  next();
};

const roleAuth = (to, from, next) => {
  const roles = to.meta.auth;
  const beforePath = from.path === "/" ? "/usuarios" : from.path;
  let authories = jwt_decode(auth).auth.split(",");
  //console.log(authories);
  let nro_permisos = 0;
  authories.forEach(el => {
    if (roles.includes(el)) nro_permisos += 1;
  });
  if (nro_permisos > 0) next();
  else {
    alert("No tiene permiso para realizar esta accion.");
    next(beforePath);
  }
  next();
};
export const routes = [
  /*{
    path: "/PageNotFound",
    name: "error",
    component: () => import("../views/error/NotFound.vue")
  },*/
  {
    path: "",
    redirect: "/login"
  },
  // Login
  {
    path: "/login",
    name: "login",
    meta: {
      auth: ["ADMINISTRADOR"]
    },
    beforeEnter: adminToLongin,
    component: () => import("../views/Login.vue")
  },
  {
    path: "/account/forgot-password",
    name: "forgot-password",
    meta: {
      auth: ["ADMINISTRADOR"]
    },
    beforeEnter: adminToLongin,
    component: () => import("@/components/forgot-password")
  },
  {
    path: "/account/reset-password",
    name: "reset-password",
    meta: {
      auth: ["ADMINISTRADOR"]
    },
    beforeEnter: adminToLongin,
    component: () => import("@/components/reset-password")
  },
  {
    path: "/",
    name: "principal",
    beforeEnter: loginToAdmin,
    redirect: "/usuarios",
    component: () => import("@/views/Admin.vue"),
    children: [
      //Descomentar para activar
      {
        path: "/almacen",
        name: "almacen",
        meta: {
          auth: ["ADMINISTRADOR"],
          title: "Almacen",
          icon: "mdi-archive"
        },
        beforeEnter: roleAuth,
        redirect: "/product-list",
        component: () => import("@/router-views/StocktakingRouterView"),
        children: [
          {
            path: "/product-list",
            name: "productos",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Productos"
            },
            beforeEnter: roleAuth,
            activated: true,
            component: () => import("@/components/admin/product")
          },
          {
            path: "/color",
            name: "colores",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Color"
            },
            beforeEnter: roleAuth,
            activated: true,
            component: () => import("@/components/admin/product-detail/color")
          },
          {
            path: "/talla",
            name: "tallas",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Talla"
            },
            beforeEnter: roleAuth,
            activated: true,
            component: () => import("@/components/admin/product-detail/talla")
          },
          {
            path: "/categoria-producto",
            name: "categoria producto",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Categoria Producto"
            },
            beforeEnter: roleAuth,
            activated: true,
            component: () => import("@/components/admin/product-detail/categoria")
          },
          {
            path: "/etiqueta-producto",
            name: "etiqueta producto",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Etiqueta Producto"
            },
            beforeEnter: roleAuth,
            activated: true,
            component: () => import("@/components/admin/product-detail/etiqueta")
          },
          {
            path: "/crear-producto",
            name: "crear producto",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Crear Producto"
            },
            beforeEnter: roleAuth,
            activated: false,
            component: () =>
              import("@/components/components-base/forms/product-form")
          },
          {
            path: "/editar-producto/:id",
            name: "editar producto",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Editar Producto"
            },
            beforeEnter: roleAuth,
            activated: false,
            component: () =>
              import("@/components/components-base/forms/product-form")
          }
        ]
      },
      {
        path: "/inventory",
        name: "inventario",
        meta: {
          title: "Inventario",
          icon: "mdi-transfer",
          auth: ["ADMINISTRADOR"]
        },
        beforeEnter: roleAuth,
        activated: true,
        redirect: "/administrar-inventario",
        component: () => import("@/router-views/InventoryRouterView"),
        children: [
          // {
          //   path: "/administrar-inventario",
          //   name: "administrar inventario",
          //   meta: {
          //     auth: ["ADMINISTRADOR"],
          //     title: "Administrar"
          //   },
          //   beforeEnter: roleAuth,
          //   activated: true,
          //   component: () => import("@/components/admin/inventory/Administrar")
          // },
          {
            path: "/entradas",
            name: "entradas",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Entradas"
            },
            beforeEnter: roleAuth,
            activated: true,
            component: () => import("@/components/admin/inventory/entradas")
          },
          // {
          //   path: "/salidas",
          //   name: "salidas",
          //   meta: {
          //     auth: ["ADMINISTRADOR"],
          //     title: "Salidas"
          //   },
          //   beforeEnter: roleAuth,
          //   activated: true,
          //   component: () => import("@/components/admin/inventory/salidas")
          // },
          // {
          //   path: "/movimiento-entradas",
          //   name: "movimiento de entradas",
          //   meta: {
          //     auth: ["ADMINISTRADOR"],
          //     title: "Movimiento de Entradas"
          //   },
          //   beforeEnter: roleAuth,
          //   activated: true,
          //   component: () =>
          //     import("@/components/admin/inventory/detalle-entradas")
          // },
          // {
          //   path: "/movimiento-salidas",
          //   name: "movimiento de salidas",
          //   meta: {
          //     auth: ["ADMINISTRADOR"],
          //     title: "Movimiento de Salidas"
          //   },
          //   beforeEnter: roleAuth,
          //   activated: true,
          //   component: () =>
          //     import("@/components/admin/inventory/detalle-salidas")
          // },
          // {
          //   path: "/detalle-movimiento-producto/:id",
          //   name: "detalle-movimiento-producto",
          //   meta: {
          //     auth: ["ADMINISTRADOR"],
          //     title: "Detalle Movimientos de Producto"
          //   },
          //   beforeEnter: roleAuth,
          //   activated: false,
          //   component: () =>
          //     import("@/components/admin/inventory/detail-movimiento-producto")
          // }
        ]
      },
      // USUARIOS.............
      {
        path: "/usuarios",
        name: "usuarios",
        meta: {
          auth: ["ADMINISTRADOR"],
          title: "Usuarios",
          icon: "mdi-account-multiple"
        },
        beforeEnter: roleAuth,
        activated: true,
        redirect: "/user-list",
        component: () => import("@/router-views/UserRouterView"),
        children: [
          {
            path: "/user-list",
            name: "lista de usuarios",
            icon: "",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Usuarios",
              icon: "mdi-account-details"
            },
            beforeEnter: roleAuth,
            activated: true,
            component: () => import("@/components/admin/user")
          },
          {
            path: "/roles",
            name: "roles",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Roles",
              icon: "mdi-clipboard-list"
            },
            beforeEnter: roleAuth,
            activated: true,
            component: () => import("@/components/admin/role")
          },
          {
            path: "/create-user",
            name: "crear usuario",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Crear Usuario",
              icon: "mdi-account-plus"
            },
            beforeEnter: roleAuth,
            activated: false,
            component: () =>
              import("@/components/components-base/forms/user-form")
          },
          {
            path: "/edit-user/:id",
            name: "editar usuario",
            meta: {
              auth: ["ADMINISTRADOR"],
              title: "Editar usuario",
              icon: "mdi-monitor"
            },
            beforeEnter: roleAuth,
            activated: false,
            component: () =>
              import("@/components/components-base/forms/user-form")
          }
        ]
      }
    ]
  }
];
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
