// import Vue from 'vue'
// import App from './App.vue'

// Vue.config.productionTip = false

// new Vue({
//   render: h => h(App),
// }).$mount('#app')
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import i18n from './plugins/i18n'
import vuelidate from './plugins/vuelidate'
import notifications from './plugins/notifications'

import titleMixin from '../src/mixins/titleMixin'
Vue.mixin(titleMixin)

Vue.config.productionTip = false

Vue.directive("uppercase", {
  bind (el, _, vnode) {
    el.addEventListener('input', (e) => {
      e.target.value = e.target.value.toUpperCase()
      vnode.componentInstance.$emit('input', e.target.value.toUpperCase())
    })
  }
})

new Vue({
  router,
  store,
  vuetify,
  i18n,
  vuelidate,
  notifications,
  render: h => h(App),
}).$mount('#app')
