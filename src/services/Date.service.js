export default {
  parseDateToString(date) {
    if (date === null) {
      return "";
    }
    const FECHA = new Date(date);
    const DAYS_OF_WEEK = ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"];
    /*let strTime = FECHA.toLocaleTimeString();
    let strDate = FECHA.toLocaleDateString();*/
    let dayOfWeek = DAYS_OF_WEEK[FECHA.getDay()];
    let strFecha = `${dayOfWeek}, ${this.getDate(date)}, ${this.getTime(date)}`;
    return strFecha;
  },
  getDate(date) {
    const FECHA = new Date(date);
    let strDate = FECHA.toLocaleDateString("it-IT");
    const [day, month, year] = strDate.split("/");
    return `${day.padStart(2, "0")}/${month.padStart(2, "0")}/${year}`;
  },
  getTime(date) {
    const FECHA = new Date(date);
    //return FECHA.toLocaleTimeString("it-IT");
    let strTime = FECHA.toLocaleTimeString("it-IT");
    const [hours, mins] = strTime.split(":");
    return `${hours.padStart(2, "0")}:${mins.padStart(2, "0")} hrs.`;
  },
  isToday(date) {
    const HOY = new Date();
    const FECHA = new Date(date);
    /*return {
      isToday: HOY.toLocaleDateString() === FECHA.toLocaleDateString(),
      hours: 24 - HOY.getHours(),
      mins: 60 - HOY.getMinutes(),
      secs: 60 - HOY.getSeconds()
    };*/
    return HOY.toLocaleDateString() === FECHA.toLocaleDateString();
  },
  getRemainingTime() {
    const HOY = new Date();
    return {
      hours: 24 - HOY.getHours(),
      mins: 60 - HOY.getMinutes(),
      secs: 60 - HOY.getSeconds()
    };
  }
};
