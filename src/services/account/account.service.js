import { instance } from '../api.service'
import authHeader from '../auth-header'

const resource = 'account'

export default {
    findAccount() {
        return instance.get(`${resource}`, { headers: authHeader()})
    },
    resetPasswordInit(email) {
        return instance.post(`${resource}/reset-password/init`, email)
    },
    resetPasswordFinish(item) {
        return instance.post(`${resource}/reset-password/finish`, item)
    }
}
