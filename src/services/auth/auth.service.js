import { instance } from '../api.service'

const resource = 'authenticate'

export default {
  login (user) {
    return instance.post(`${resource}`, user)
  },
  logout (data) {
    return instance.post(`${resource}`, data)
  },
}
