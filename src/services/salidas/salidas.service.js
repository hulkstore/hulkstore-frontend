import { instance } from '../api.service'
import authHeader from '../auth-header'

const resource = 'inv-salidas'

export default {
  findAll (page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc)
    return instance.get(`${resource}?page=${page}&size=${size}${sort}`, { headers: authHeader() })
  },
  create (data) {
    return instance.post(`${resource}`, data, { headers: authHeader() })
  },
  update (data) {
    return instance.put(`${resource}`, data, { headers: authHeader() })
  },
  findOne (id) {
    return instance.delete(`${resource}/id/${id}`, { headers: authHeader() })
  },
  orderByColumnsAscDesc (sortName, ascDesc) {
    let data = ''
    const res = sortName[0]
    if (res) {
      const data2 = ascDesc[0] === true ? 'asc' : 'desc'
      data = `&sort=${res},${data2}`
    }
    return data
  },
  findAllBySucursalId(page, size, sortBy, sortDesc, id) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc);
    return instance.get(
      `${resource}/filter-by-sucursal-id/${id}?page=${page}&size=${size}${sort}`,
      { headers: authHeader() }
    )
  },
  searchBySucursal(
    keyword,
    startDate,
    endDate,
    page,
    size,
    sortBy,
    sortDesc,
    id
  ) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc);
    return instance.get(
      `${resource}/searchId?keyword=${keyword}&startDate=${startDate}&endDate=${endDate}&id=${id}&page=${page}&size=${size}${sort}`,
      { headers: authHeader() }
    );
  },
  createInvSalida (data) {
    console.log('------------ La data pasa ----------------')
    console.log('------------ La data pasa ----------------')
    console.log(data)
    console.log('------------ La data pasa ----------------')
    console.log('------------ La data pasa ----------------')
    return instance.post(`${resource}/save-salida`, data, { headers: authHeader() })
  },
  createInvSalidaInvEntradaTransferencia (data) {
    console.log('------------ La data pasa ----------------')
    console.log('------------ La data pasa ----------------')
    console.log(data)
    console.log('------------ La data pasa ----------------')
    console.log('------------ La data pasa ----------------')
    return instance.post(`${resource}/save-transferencia`, data, { headers: authHeader() })
  },
  findAllDeudaByClienteId (clienteId, page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc)
    return instance.get(`${resource}/deudas-by-client-id?clienteId=${clienteId}&page=${page}&size=${size}${sort}`, { headers: authHeader() })
  }
}
