import { instance, download } from '../api.service'
import authHeader from '../auth-header'

const resource = 'inv-salidas'

export default {
  findAll (page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc)
    return instance.get(`${resource}?page=${page}&size=${size}${sort}`, { headers: authHeader() })
  },
  create (data) {
    return instance.post(`${resource}`, data, { headers: authHeader() })
  },
  update (data) {
    return instance.put(`${resource}`, data, { headers: authHeader() })
  },
  findOne (id) {
    return instance.get(`${resource}/id/${id}`, { headers: authHeader() })
  },
  delete (login) {
    return instance.delete(`${resource}/${login}`, { headers: authHeader() })
  },
  search (keyword, page, size, sortBy, sortDesc,id) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc)
    return instance.get(`${resource}/searchId?keyword=${keyword}&id=${id}&page=${page}&size=${size}${sort}`, { headers: authHeader() })
  },
  orderByColumnsAscDesc (sortName, ascDesc) {
    let data = ''
    const res = sortName[0]
    if (res) {
      const data2 = ascDesc[0] === true ? 'asc' : 'desc'
      data = `&sort=${res},${data2}`
    }
    return data
  },
  createInvoicing (data) {
    // let path = ''
    // item === 'FACTURA' ? path= '/save-invoicing': path= '/save-receipt'
    return instance.post(`${resource}/save-invoicing`, data, { headers: authHeader() })
  },
  countSales () {
    return instance.get(`${resource}/count`, { headers: authHeader() })
  },
  countReceipts () {
    return instance.get(`${resource}/countRecibo`, { headers: authHeader() })
  },
  countInvoices () {
    return instance.get(`${resource}/countFactura`, { headers: authHeader() })
  },
  generateInvoicePdf (facturaId) {
    return download.get(`${resource}/generar-factura-pdf/${facturaId}`)
  },
  findAllFacturasByCurrentUser(page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc)
    return instance.get(`${resource}/find-all-recibo-by-current-user?page=${page}&size=${size}${sort}`, { headers: authHeader() })
  },
  stadisticByDate(fecha) {
    return instance.get(`${resource}/stadisticSalesByDate/${fecha}`, { headers: authHeader() })
  },
  stadisticByDay() {
    return instance.get(`${resource}/stadisticSalesByDay`, { headers: authHeader() })
  },
  stadisticByMonth() {
    return instance.get(`${resource}/stadisticSalesByMonth`, { headers: authHeader() })
  },
  generateCurrentInvoice(invoice) {
    // let path = ''
    // item === 'FACTURA' ? path= '/save-invoicing': path= '/save-receipt'
    return download.post(`${resource}/generate-invoice-pdf`, invoice)
  },
    generatInvoiceByInvSalidaId(id) {
    // let path = ''
    // item === 'FACTURA' ? path= '/save-invoicing': path= '/save-receipt'
    return download.post(`${resource}/generate-invoice-pdf/${id}`)
  },
  generateReportInvoice(invoices) {
    return download.post(`${resource}/generate-invoice-report-pdf`, invoices)
  },
  generateXlsReportInvoice(invoices) {
    return download.post(`${resource}/generate-invoice-report-xls`, invoices)
  },
  findAllConciliacionCuentas() {
    return instance.get(`${resource}/findAllConciliacionCuentas`, { headers: authHeader() })
  },
  generatePDFReportConciliacion(conciliacion) {
    return download.post(`${resource}/generate-conciliacion-report-pdf`, conciliacion)
  },
  generateXlsReportConciliacion(conciliacion) {
    return download.post(`${resource}/generate-conciliacion-report-xls`, conciliacion)
  },

  findAllBySucursalId(page, size, sortBy, sortDesc, id) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc);
    return instance.get(
      `${resource}/filter-by-sucursal-id-and-mov/${id}?page=${page}&size=${size}${sort}`,
      { headers: authHeader() }
    )
  }, 
  searchRecibosByCurrentUser (keyword, page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc)
    return instance.get(`${resource}/search-recibo-by-user?keyword=${keyword}&page=${page}&size=${size}${sort}`, { headers: authHeader() })
  }, 
  findAllByTipoMov () {
    return instance.get(`${resource}/by-tipo-mov`, { headers: authHeader() })
  },  
}

