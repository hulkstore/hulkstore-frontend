import { instance } from "../api.service";
import authHeader from "../auth-header";

const resource = "usuarios";

export default {
  findAll(page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc);
    //console.log(`${resource}?page=${page}&size=${size}${sort}`);
    return instance.get(`${resource}?page=${page}&size=${size}${sort}`, {
      headers: authHeader()
    });
  },
  countAll() {
    return instance.get(`${resource}/count`, { headers: authHeader() });
  },
  create(data) {
    return instance.post(`${resource}`, data, { headers: authHeader() });
  },
  update(data) {
    return instance.put(`${resource}`, data, { headers: authHeader() });
  },
  findOne(id) {
    return instance.get(`${resource}/${id}`, { headers: authHeader() });
  },
  delete(login) {
    return instance.delete(`${resource}/${login}`, { headers: authHeader() });
  },
  uploadImage(image) {
    return instance.put(`${resource}/upload-avatar`, image, {
      headers: authHeader()
    });
  },
  search(keyword, page, size, sortBy, sortDesc) {
    //const sort = this.orderByColumnsAscDesc(sortBy, sortDesc);
    console.log(
      `${resource}/search?keyword=${keyword}&page=${page}&size=${size}&sort=${sortBy},${
        sortDesc === true ? "asc" : "desc"
      }`
    );
    return instance.get(
      `${resource}/search?keyword=${keyword}&page=${page}&size=${size}&sort=${sortBy},${
        sortDesc === true ? "asc" : "desc"
      }>>>>>>>`,
      { headers: authHeader() }
    );
  },
  orderByColumnsAscDesc(sortName, ascDesc) {
    let data = "";
    const res = sortName[0];
    if (res) {
      const data2 = ascDesc[0] === true ? "asc" : "desc";
      data = `&sort=${res},${data2}`;
    }
    return data;
  },
};
