import { instance } from '../api.service'
import authHeader from '../auth-header'

const resource = 'imagens'

export default {
  findAll (productId) {
    return instance.get(`${resource}/product-id/${productId}?page=${0}&size=${20}`, { headers: authHeader() })
  },
  createImage (data) {
    return instance.post(`${resource}/post/upload-product-images`, data, { headers: authHeader() })
  },
  updateImage (data) {
    return instance.put(`${resource}/put/upload-product-images`, data, { headers: authHeader() })
  },
  delete (id) {
    return instance.delete(`${resource}/${id}`, { headers: authHeader() })
  }
}
