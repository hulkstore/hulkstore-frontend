import axios from 'axios'
import Vue from 'vue'
import router from '@/router'

export const instance = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }
})

export const download = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL,
  responseType: 'arraybuffer'
})

instance.interceptors.response.use(
  response => {
    if (response.data) {
      if (response.headers['x-total-count'] === undefined) {
        return response.data
      } else {
        return {
          data: response.data,
          elements: response.headers['x-total-count']
        }
      }
    } else {
      return Promise.reject(response.data)
    }
  },
  error => {
    if (error.response === undefined) {
      Vue.prototype.$notify({
        group: 'token',
        text: 'El token del usuario a Expirado, porfavor entre de nuevo al sistema.' + '!',
        type: 'error',
        duration: 40000,
      });
      setTimeout(() => {
        if (router.history.current.name !== 'login') {
          router.push('/login')
        }
      }, 40000)
      localStorage.removeItem('isu-token');
      sessionStorage.removeItem('isu-token');
    }
    if (error.response.status === 401) {
      if (localStorage.getItem('isu-token') !== null) {
        setTimeout(() => {
          if (router.history.current.name !== 'login') {
            router.push('/login')
          }
        }, 40000)
        Vue.prototype.$notify({
          group: 'token',
          text: 'El token del usuario a Expirado, porfavor entre de nuevo al sistema.' + '!',
          type: 'error',
          duration: 40000,
        });
        localStorage.removeItem('isu-token');
        sessionStorage.removeItem('isu-token');
      }
    }
    return Promise.reject(error)
  }
)
