import { instance } from "../api.service";
import authHeader from "../auth-header";

const resource = "inv-detalle-entradas";

export default {
  //   countAll(){
  //     return instance.get(`${resource}/count`,{ headers: authHeader() })
  //   },
  create(data) {
    return instance.post(`${resource}`, data, { headers: authHeader() });
  },
  update(data) {
    return instance.put(`${resource}`, data, { headers: authHeader() });
  },
  findOne(id) {
    return instance.get(`${resource}/${id}`, { headers: authHeader() });
  },
  delete(id) {
    return instance.delete(`${resource}/${id}`, { headers: authHeader() });
  },
  findAll(page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc);
    return instance.get(`${resource}?page=${page}&size=${size}${sort}`, {
      headers: authHeader()
    });
  },
  findAllByEntradaId(id) {
    return instance.get(`${resource}/entrada/${id}`, { headers: authHeader() });
  },
  findAllByProductId(id) {
    return instance.get(`${resource}/entrada-by-product-id?productId=${id}`, {
      headers: authHeader()
    });
  },
  findAllProdById(id, page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc);
    return instance.get(
      `${resource}/movimiento?id=${id}&page=${page}&size=${size}${sort}`,
      { headers: authHeader() }
    );
  },
  orderByColumnsAscDesc(sortName, ascDesc) {
    let data = "";
    const res = sortName[0];
    if (res) {
      const data2 = ascDesc[0] === true ? "asc" : "desc";
      data = `&sort=${res},${data2}`;
    }
    return data;
  },
  search(keyword, startDate, endDate, page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc);
    return instance.get(
      `${resource}/search?keyword=${keyword}&startDate=${startDate}&endDate=${endDate}&page=${page}&size=${size}${sort}`,
      { headers: authHeader() }
    );
  },
  searchProdId(id, startDate, endDate, page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc);
    return instance.get(
      `${resource}/searchId?id=${id}&startDate=${startDate}&endDate=${endDate}&page=${page}&size=${size}${sort}`,
      { headers: authHeader() }
    );
  },
  findAllLoteDetalleEntradasByProductId(productId) {
    return instance.get(`${resource}/pre-entrada-vm-product-id?productId=${productId}`, {
      headers: authHeader()
    });
  },
};
