import { instance } from '../api.service'
import authHeader from '../auth-header'

const resource = 'colors'

export default {
  findAll (page, size) {
    return instance.get(`${resource}?page=${page}&size=${size}`, { headers: authHeader() })
  },
  create (data) {
    return instance.post(`${resource}`, data, { headers: authHeader() })
  },
  update (data) {
    return instance.put(`${resource}`, data, { headers: authHeader() })
  },
  findOne (id) {
    return instance.get(`${resource}/${id}`, { headers: authHeader() })
  },
  delete (id) {
    return instance.delete(`${resource}/${id}`, { headers: authHeader() })
  }
}
