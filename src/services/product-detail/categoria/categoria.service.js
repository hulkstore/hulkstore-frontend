import { instance } from '../../api.service'
import authHeader from '../../auth-header'

const resource = 'categoriaproductos'

export default {
  findAll (page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc)
    return instance.get(`${resource}?page=${page}&size=${size}${sort}`, { headers: authHeader() })
  },
  create (data) {
    return instance.post(`${resource}`, data, { headers: authHeader() })
  },
  update (data) {
    return instance.put(`${resource}`, data, { headers: authHeader() })
  },
  findOne (id) {
    return instance.get(`${resource}/${id}`, { headers: authHeader() })
  },
  delete (id) {
    return instance.delete(`${resource}/${id}`, { headers: authHeader() })
  },
  search (keyword, page, size, sortBy, sortDesc) {
    const sort = this.orderByColumnsAscDesc(sortBy, sortDesc)
    return instance.get(`${resource}/search?keyword=${keyword}&page=${page}&size=${size}${sort}`, { headers: authHeader() })
  },
  orderByColumnsAscDesc (sortName, ascDesc) {
    let data = ''
    const res = sortName[0]
    if (res) {
      const data2 = ascDesc[0] === true ? 'asc' : 'desc'
      data = `&sort=${res},${data2}`
    }
    return data
  },
}
