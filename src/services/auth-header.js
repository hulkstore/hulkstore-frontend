export default function authHeader() {
  let token = sessionStorage.getItem("isu-token")
    ? sessionStorage.getItem("isu-token")
    : localStorage.getItem("isu-token")
    ? localStorage.getItem("isu-token")
    : null;
  if (token) {
    // return { Authorization: 'Bearer ' + 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYxOTc2MDg3N30.TdUhLij0cVuzPcuyL2-2HpuIVwhRHW86sW6HwOpAMM1AV5oksCi6KJOsN0YDSYRPq9EDKYgt4Qas9a24msHMNg' };
    return { Authorization: "Bearer " + token };
  } else {
    return {};
  }
}
