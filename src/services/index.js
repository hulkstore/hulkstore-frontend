import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { instance } from './api.service'

Vue.use(VueAxios, axios)

Vue.prototype.$instance = instance

export default new VueAxios()
