import { instance } from '../api.service'
import authHeader from '../auth-header'

const resource = 'roles'

export default {
  findAllRoles () {
    return instance.get(`${resource}`, { headers: authHeader() })
  },
}

