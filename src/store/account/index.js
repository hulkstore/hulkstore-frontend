import { state } from './account.state'
import { mutations } from './account.mutations'
import { actions } from './account.actions'
import { getters } from './account.getters'

export default {
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}
