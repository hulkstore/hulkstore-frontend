import AccountService from '../../services/account/account.service'
// import router from '@/router'
// import Vue from 'vue'

export const actions = {
  currentAccount: async ({ commit }) => {
    try {
      const response = await AccountService.findAccount()
      console.log('response -> ', response)
      commit('CURRENT_ACCOUNT', response)
    } catch (error) {
      console.log('-----------------------')
      console.log('-----------------------')
      console.log('-----------------------')
      console.log('error ------>>> ', error);
      console.log('-----------------------')
      console.log('-----------------------')
      console.log('-----------------------')
    }
  },
}
