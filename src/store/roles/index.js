import { state } from './roles.state'
import { mutations } from './roles.mutations'
import { actions } from './roles.actions'
import { getters } from './roles.getters'

export default {
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}
