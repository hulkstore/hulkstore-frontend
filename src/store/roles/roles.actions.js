import RoleService from '../../services/role/role.service'

export const actions = {
  async findAllRoles ({ commit }) {
    try {
      const data = await RoleService.findAllRoles()
      console.log(data)
      commit('FIND_ALL_ROLES', data)
    } catch (error) {
      console.error(error)
    }
  }
}
