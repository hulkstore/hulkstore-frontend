import Vue from "vue";
import Vuex from "vuex";
import authModule from "./auth";
import langModule from "./lang";
import roleModule from "./roles";
import accountModule from "./account";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    drawer: null,
    accountX: {},
    // sucursal: {}
  },
  mutations: {
    SET_DRAWER(state, payload) {
      state.drawer = payload;
    },
    SET_ACCOUNT(state, data) {
      state.accountX = data;
    },
    // SET_SUCURSAL(state, data) {
    //   state.sucursal = data;
    // }
  },
  actions: {},
  modules: {
    authModule,
    langModule,
    roleModule,
    accountModule
  }
});
