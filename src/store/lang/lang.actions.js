export const actions = {
  setLang: ({ commit }, { currentLang }) => {
    commit('SET_LANG', { currentLang })
  }
}
