import { state } from './lang.state'
import { mutations } from './lang.mutations'
import { actions } from './lang.actions'
import { getters } from './lang.getters'

export default {
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}
