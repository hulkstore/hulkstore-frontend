export const mutations = {
  SET_LANG (state, data) {
    state.data = data.currentLang
    localStorage.setItem('currentLang', data.currentLang)
  }
}
