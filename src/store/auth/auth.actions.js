import AuthService from '../../services/auth/auth.service'
import router from '@/router'
import Vue from 'vue'

export const actions = {
  login: async ({ commit }, user) => {
    commit('LOGIN', { user })
    try {
      user.rememberMe = true
      const resp = await AuthService.login(user)
      console.log(resp)
      resp.rememberMe = true
      commit('LOGIN_SUCCESS', resp)
      router.push({ name: 'usuarios' })
      const object = {
        type: true,
        text: 'success'
      }
      return object
    } catch (error) {
      commit('LOGIN_FAIL', 'error')
      Vue.prototype.$notify({
        group: 'foo',
        text: 'El usuario o el password son incorrectos' + '!',
        type: 'error'
      });
      const object = {
        type: false,
        text: error
      }
      return object
    }
  },
  logout({ commit }) {
    commit('LOGOUT')
    router.push({ name: 'login' })
  }
}
