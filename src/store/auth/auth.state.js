const initialState = {
  token: localStorage.getItem('token') ? localStorage.getItem('token') : sessionStorage.getItem('token') ?  sessionStorage.getItem('token') : null,
}

export const state = {
  status: initialState.token ? { loggedIn: true } : {},
  data: initialState.token || null
}
