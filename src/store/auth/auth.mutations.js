export const mutations = {
  LOGIN (state, data) {
    state.status = { loggingIn: true }
    state.data = data
  },
  LOGIN_SUCCESS (state, data) {
    state.status = { loggedIn: true }
    state.data = data
    console.log(data.id_token)
    if (data.rememberMe) {
      localStorage.setItem('isu-token', data.id_token)
    } else {
      sessionStorage.setItem('isu-token', data.id_token)
    }
  },
  LOGIN_FAIL (state) {
    state.status = {}
    state.data = null
  },
  LOGOUT (state) {
    state.status = {}
    state.data = null
    localStorage.removeItem('isu-token');
    sessionStorage.removeItem('isu-token');
  }
}
