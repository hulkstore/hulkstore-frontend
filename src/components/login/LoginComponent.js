import { mapActions, mapGetters } from "vuex";
import LoginUserModel from "../components-base/models/LoginUserModel";

export default {
  title: 'login',
  name: "loginc-component",
  mixins: [LoginUserModel],
  data: () => ({
    showPassword: false,
    step: 1,
    selectedLang: localStorage.getItem("currentLang") || "es",
    availableLangs: [],
    valid: false,
    showPass: false,
    imagenUrl: null,
    defaultItem: {}
  }),
  props: {
    source: String
  },
  created() {
    this.translateDropdown(this.currentLang$);
  },
  computed: {
    ...mapGetters({ authCredentials: "authModule/authCredentials" }),
    ...mapGetters("langModule", ["currentLang$"])
  },
  watch: {
    selectedLang: {
      handler: function(newVal) {
        this.translateDropdown(newVal);
      },
      immediate: true
    }
  },
  methods: {
    ...mapActions("authModule", ["login"]),
    selectLang() {
      const currentLang = this.selectedLang;
      this.setLang({ currentLang });
      this.$i18n.locale = this.currentLang$;
    },
    translateDropdown(locale) {
      this.availableLangs = [
        {
          value: this.$i18n.t("general.en", locale),
          id: "en"
        },
        {
          value: this.$i18n.t("general.es", locale),
          id: "es"
        }
      ];
    },
    checkForm() {
      this.$v.$touch();
      console.log("pasa por qui");
      if (!this.$v.$invalid) {
        this.login({
          username: this.username,
          password: this.password,
          rememberMe: this.rememberMe
        });
      }
      console.log("-----------------------------------------");
      console.log("-----------------------------------------");
      console.log("Esta es una buena prueb");
      console.log(this.username);
      console.log(this.password, this.rememberMe);
      console.log("-----------------------------------------");
      console.log("-----------------------------------------");
    },
    redirectTo() {
      this.$router.push({ name: "forgot-password" });
    },
    // async getImageURL() {
    //   try {
    //     const response = await CompanyService.findOne(1);
    //     this.defaultItem = response;
    //     this.imagenUrl = response.logo1;
    //     console.log(this.defaultItem);
    //   } catch (error) {
    //     console.log(error);
    //   }
    // }
  },
  mounted() {
    // this.getImageURL();
  }
};
