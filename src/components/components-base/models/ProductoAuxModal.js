import { required } from 'vuelidate/lib/validators'

export default {
  data: () => ({
    defaultItem: {
      id: null,
      nombre: null,
      codigo: null,
      fecha: null,
      precioUnitario: null,
      costoUnitario: null,
      estado: null,
      colors: [],
      tallas: [],
      materials: [],
      tipoTejidos: [],
      tipoHilados: [],
      tipoPrendas: [],
    },
    defaultItemDEx: {
      id: null,
      costoUnitario: null,
      cantidad: null,
      entradaId: null,
      productoId: null,
    },
  }),
  validations: {
    defaultItem: {
      codigo: {
        required
      },
    },
    defaultItemDEx: {
      cantidad: {
        required
      }
    }
  },
  computed: {
    codigoErrors () {
      const errors = []
      if (!this.$v.defaultItem.codigo.$dirty) return errors
      !this.$v.defaultItem.codigo.required && errors.push(this.$t('validator.required'))
      return errors
    },
    cantidadErrors () {
      const errors = []
      if (!this.$v.defaultItemDEx.cantidad.$dirty) return errors
      !this.$v.defaultItemDEx.cantidad.required && errors.push(this.$t('validator.required'))
      return errors
    },
  },
}
