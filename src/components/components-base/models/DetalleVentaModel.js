export default{
    data: () =>({
        defaultItemDV: {
            id: null,
            cantidad: null,
            costoUnitario: null,
            precioUnitario: null ,
            facturaVentaId: null,
            productoId: null,
        },
    })
}