import {
  required,
  minLength,
  maxLength,
  email,
  numeric
} from "vuelidate/lib/validators";

export default {
  data: () => ({
    defaultItem: {
      id: null,
      nombre: null,
      nroTarjeta: null,
      username: null,
      password: null,
      email: null,
      estado: true,
      roles: [],
    }
  }),
  validations: {
    defaultItem: {
      nombre: { required, maxLength: maxLength(20) },
      nroTarjeta: { required, numeric},
      username: { required, minLength: minLength(3)},
      email: { required, email },
      roles:{ required }
    }
  },
  computed: {
    nombreErrors() {
      const errors = [];
      if (!this.$v.defaultItem.nombre.$dirty) {
        return errors;
      } else {
        !this.$v.defaultItem.nombre.required && errors.push("Nombre es requerido.");
        !this.$v.defaultItem.nombre.maxLength && errors.push("Nombre debe tener maximo 20 caracteres");
        return errors;
      }
    },
    nroTarjetaErrors() {
      const errors = [];
      if (!this.$v.defaultItem.nroTarjeta.$dirty) {
        return errors;
      } else {
        !this.$v.defaultItem.nroTarjeta.required && errors.push("Numero de Tarjeta es requerido");
        !this.$v.defaultItem.nroTarjeta.numeric && errors.push("Numero de Tarjeta debe ser un numero");
        return errors;
      }
    },
    usernameErrors() {
      const errors = [];
      if (!this.$v.defaultItem.username.$dirty) {
        return errors;
      } else {
        !this.$v.defaultItem.username.required && errors.push("Nombre usuario es requerido.");
        !this.$v.defaultItem.username.minLength && errors.push("Nombre usuario no debe tener mas de 3 caracteres");
        return errors;
      }
    },
    emailNameErrors() {
      const errors = [];
      if (!this.$v.defaultItem.email.$dirty) {
        return errors;
      } else {
        !this.$v.defaultItem.email.required && errors.push("El E-mail es requerido.");
        !this.$v.defaultItem.email.email && errors.push("El E-mail debe ser valido");
        return errors;
      }
    },
    rolesErrors() {
      const errors = [];
      if (!this.$v.defaultItem.roles.$dirty) {
        return errors;
      } else {
        !this.$v.defaultItem.roles.required &&
          errors.push("Se debe seleccionar por lo menos un rol.");
        return errors;
      }
    },
  }
};
