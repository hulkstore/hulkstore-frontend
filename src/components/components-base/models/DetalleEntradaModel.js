export default {
  data: () => ({
    defaultItemDE: {
      id: null,
      costoUnitario: null,
      cantidad: null,
      entradaId: null,
      productoId: null,
    },
  }),
}
