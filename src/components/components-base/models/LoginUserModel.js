import { required, minLength } from 'vuelidate/lib/validators'

export default {
  data: () => ({
    username: null,
    password: null,
    rememberMe: false,
    serverError: false
  }),
  validations: {
    username: {
      required
    },
    password: {
      required,
      minLength: minLength(4)
    }
  },
  computed: {
    usernameErrors () {
      const errors = []
      if (!this.$v.username.$dirty) return errors
      !this.$v.username.required && errors.push(this.$t('validator.required'))
      return errors
    },
    passwordErrors () {
      const errors = []
      if (!this.$v.password.$dirty) return errors
      !this.$v.password.required && errors.push(this.$t('validator.required'))
      !this.$v.password.minLength && errors.push(`${this.$t('validator.minLength', ['4'])}`)
      return errors
    }
  },
}
