import { required } from "vuelidate/lib/validators";

export default {
  data: () => ({
    defaultItem: {
      // id: null,
      nombre: null,
      codigo: null,
      // fecha: null,
      // precioUnitarioCompra: null,
      // cantidad: null,
      // estado: null,
      // temporada: null,
      // calidad: null,
      // sucursalId: null,
      // unidadMedidaId: null,
      colorId:null,
      // marcaId:null,
      categoriaProducto: null,
      id: null,
      nombre: null,
      descripcion: null,
      codigo: null,
      costoUnitarioCompra: null,
      precioUnitarioVenta: null,
      cantidad: null,
      estado: null,
      categoriaId: null,
      etiquetaId: null,
      imageUrl: null,
      // Producto Comic
      titulo: null,
      genero: null,
      volumen: null,
      anioLanzamiento: null,
      idioma: null,
      // Producto Camiseta
      sexo: null,
      colorId: null,
      tallaId: null,
    }
  }),
  validations: {
    // defaultItem: {
      // nombre: {
      //   required
      // },
      // categoriaId: {
      //   required
      // },
      // etiquetaId: {
      //   required
      // },
      // titulo: {
      //   required
      // },
      // genero: {
      //   required
      // },
      // volumen: {
      //   required
      // },
      // anioLanzamiento: {
      //   required
      // },
      // idioma: {
      //   required
      // },
      // sexo: {
      //   required
      // },
      // colorId: {
      //   required
      // },
      // tallaId: {
      //   required
      // }
    // }
  },
  computed: {
    // nombreErrors() {
    //   const errors = [];
    //   if (!this.$v.defaultItem.nombre.$dirty) return errors;
    //   !this.$v.defaultItem.nombre.required &&
    //     errors.push(this.$t("validator.required"));
    //   return errors;
    // },
    // categoriaIdErrors() {
    //   const errors = [];
    //   if (!this.$v.defaultItem.categoriaId.$dirty) return errors;
    //   !this.$v.defaultItem.categoriaId.required && errors.push(this.$t("validator.required"));
    //   return errors;
    // },
    // etiquetaIdErrors() {
    //   const errors = [];
    //   if (!this.$v.defaultItem.etiquetaId.$dirty) return errors;
    //   !this.$v.defaultItem.etiquetaId.required && errors.push(this.$t("validator.required"));
    //   return errors;
    // },
    // // Comics ----------------
    // tituloErrors() {
    //   const errors = [];
    //   if (!this.$v.defaultItem.titulo.$dirty) return errors;
    //   !this.$v.defaultItem.titulo.required && errors.push(this.$t("validator.required"));
    //   return errors;
    // },
    // generoErrors() {
    //   const errors = [];
    //   if (!this.$v.defaultItem.genero.$dirty) return errors;
    //   !this.$v.defaultItem.genero.required && errors.push(this.$t("validator.required"));
    //   return errors;
    // },
    // volumenErrors() {
    //   const errors = [];
    //   if (!this.$v.defaultItem.volumen.$dirty) return errors;
    //   !this.$v.defaultItem.volumen.required && errors.push(this.$t("validator.required"));
    //   return errors;
    // },
    // anioLanzamientoErrors() {
    //   const errors = [];
    //   if (!this.$v.defaultItem.anioLanzamiento.$dirty) return errors;
    //   !this.$v.defaultItem.anioLanzamiento.required && errors.push(this.$t("validator.required"));
    //   return errors;
    // },
    // idiomaErrors() {
    //   const errors = [];
    //   if (!this.$v.defaultItem.idioma.$dirty) return errors;
    //   !this.$v.defaultItem.idioma.required && errors.push(this.$t("validator.required"));
    //   return errors;
    // },
    // sexoErrors() {
    //   const errors = [];
    //   if (!this.$v.defaultItem.sexo.$dirty) return errors;
    //   !this.$v.defaultItem.sexo.required && errors.push(this.$t("validator.required"));
    //   return errors;
    // },
    // colorIdErrors() {
    //   const errors = [];
    //   if (!this.$v.defaultItem.colorId.$dirty) return errors;
    //   !this.$v.defaultItem.colorId.required && errors.push(this.$t("validator.required"));
    //   return errors;
    // },
    // tallaIdErrors() {
    //   const errors = [];
    //   if (!this.$v.defaultItem.tallaId.$dirty) return errors;
    //   !this.$v.defaultItem.tallaId.required && errors.push(this.$t("validator.required"));
    //   return errors;
    // }
  }
};
