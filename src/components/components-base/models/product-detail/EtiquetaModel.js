import { required } from 'vuelidate/lib/validators'

export default {
  data: () => ({
    defaultItem: {
      id: null,
      nombre: null,
    },
  }),
  validations: {
    defaultItem: {
      nombre: {
        required
      }
    }
  },
  computed: {
    nombreErrors () {
      const errors = []
      if (!this.$v.defaultItem.nombre.$dirty) return errors
      !this.$v.defaultItem.nombre.required && errors.push(this.$t('validator.required'))
      return errors
    },
  },
}
