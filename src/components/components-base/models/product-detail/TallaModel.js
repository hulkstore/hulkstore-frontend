import { required } from 'vuelidate/lib/validators'

export default {
  data: () => ({
    defaultItem: {
      id: null,
      talla: null,
    },
  }),
  validations: {
    defaultItem: {
      talla: {
        required
      }
    }
  },
  computed: {
    tallaErrors () {
      const errors = []
      if (!this.$v.defaultItem.talla.$dirty) return errors
      !this.$v.defaultItem.talla.required && errors.push(this.$t('validator.required'))
      return errors
    },
  },
}
