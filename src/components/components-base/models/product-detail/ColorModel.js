import { required } from 'vuelidate/lib/validators'

export default {
  data: () => ({
    defaultItem: {
      id: null,
      nombre: null,
      codigo: null, 
    },
  }),
  validations: {
    defaultItem: {
      nombre: {
        required
      },
      codigo: {
        required
      }
    }
  },
  computed: {
    nombreErrors () {
      const errors = []
      if (!this.$v.defaultItem.nombre.$dirty) return errors
      !this.$v.defaultItem.nombre.required && errors.push(this.$t('validator.required'))
      return errors
    },
    codigoErrors () {
      const errors = []
      if (!this.$v.defaultItem.codigo.$dirty) return errors
      !this.$v.defaultItem.codigo.required && errors.push(this.$t('validator.required'))
      return errors
    },
  },
}
