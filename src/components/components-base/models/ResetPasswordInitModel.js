import { required, email, minLength, sameAs } from 'vuelidate/lib/validators'

export default {
  data: () => ({
    email: null,
    defaultItem: {
      key: '',
      newPassword: '',
      repeatNewPassword: ''
    }
  }),
  validations: {
    email: {
      required,
      email
    },
    defaultItem: {
      newPassword: {
        required,
        minLength: minLength(6)
      },
      repeatNewPassword: {
        sameAsNewPassword: sameAs('newPassword')
      }
    }
  },
  computed: {
    emailErrors() {
      const errors = []
      if (!this.$v.email.$dirty) return errors
      !this.$v.email.required && errors.push(this.$t('validator.required'))
      !this.$v.email.email && errors.push(this.$t('validator.email'))
      return errors
    },
    newPasswordErrors () {
      const errors = []
      if (!this.$v.defaultItem.newPassword.$dirty) return errors
      !this.$v.defaultItem.newPassword.required && errors.push(this.$t('validator.required'))
      !this.$v.defaultItem.newPassword.minLength && errors.push('El password debe tener 5 caracteres minimo')
      return errors
    },
    repeatNewPasswordErrors () {
      const errors = []
      if (!this.$v.defaultItem.repeatNewPassword.$dirty) return errors
      !this.$v.defaultItem.repeatNewPassword.sameAsNewPassword && errors.push('Las contrasenas deben ser iguales')
      return errors
    },
  }
}
