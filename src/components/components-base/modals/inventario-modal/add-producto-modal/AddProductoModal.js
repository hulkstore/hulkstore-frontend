import ProductService from '../../../../../services/product/product.service'
import detalleEntradaService from '../../../../../services/detalleEntrada/detalleEntrada.service'
import ProductAuxModel from '../../../models/ProductoAuxModal'
export default {
    name: 'AddProductModal',
    mixins: [ ProductAuxModel ],
    data: () => ({
      searchProducto: null,
      productos: [],
      productoNow: null,
      base: process.env.VUE_APP_IMAGE_BASE_URL,
    }),
    props: {
      value: {
        type: Boolean,
        default: false
      },
      reloadFunc: {
        type: Function,
        default: null
      }
    },
    watch: {
      searchProducto (val){
        this.searchProductInventory(val)
      },
      value () {
        if (!this.value) {
          console.log('=============> cerrando')
        } else {
          console.log('=============> abierto')
        }
      },
    },
    methods: {
      checkFormAdd () {
        this.$v.$touch();
        if (!this.$v.$invalid && !this.$v.defaultItemDEx.$invalid) {
          this.updateProductoInventory();
        }else{
          console.log('no valido..........');
        }
      },
      async createMovimientoEntrada(){
        try {
          await detalleEntradaService.create(this.defaultItemDEx)
        } catch (error) {
          console.log(error);
        }
      },
      async updateProductoInventory(){
        if(this.defaultItemDEx.costoUnitario){
          this.defaultItem.costoUnitario = this.defaultItemDEx.costoUnitario
        }else{
          this.defaultItemDEx.costoUnitario = this.defaultItem.costoUnitario
        }
        this.defaultItemDEx.productoId = this.defaultItem.id
        this.defaultItem.estado = true
        try {
          await ProductService.update(this.defaultItem)
          this.createMovimientoEntrada(this.defaultItemDEx)
          this.$notify({
            group: 'foo',
            text: 'El Producto se agrego a inventario exitosamente!',
            type: 'success'
          });
          this.reloadFunc();
          this.dialogEvent();
        } catch (error) {
          this.$notify({
            group: 'foo',
            text: error.response.data.title,
            type: 'error'
          });
        }
      },
      async searchProductInventory (keyword) {
        try {
          const resp = await ProductService.searchNoInventory(keyword);
          this.productos = resp;
        } catch (error) {
          console.log(error)
        }
      },
      remove () {
        this.defaultItem.id =  null,
        this.defaultItem.nombre =  null,
        this.defaultItem.codigo =  null,
        this.defaultItem.fecha =  null,
        this.defaultItem.precioUnitario =  null,
        this.defaultItem.costoUnitario =  null,
        this.defaultItem.estado = null,
        this.defaultItem.colors =  [],
        this.defaultItem.tallas =  [],
        this.defaultItem.materials =  [],
        this.defaultItem.tipoTejidos =  [],
        this.defaultItem.tipoHilados =  [],
        this.defaultItem.tipoPrendas =  [],
        this.searchProducto = null,
        this.defaultItemDEx.id = null,
        this.defaultItemDEx.costoUnitario = null,
        this.defaultItemDEx.cantidad = null,
        this.defaultItemDEx.entradaId = null,
        this.defaultItemDEx.productoId = null,
        this.$v.$reset()
      },
      dialogEvent () {
        this.dialogAdd = false
        this.$emit('input')
        this.$v.$reset()
        this.remove()
      },
    }
}
