import DetalleVentaModel from '../../../models/DetalleVentaModel'
import detalleVentaService from '../../../../../services/detalleVenta/detalleVenta.service'
export default {
    name: 'AddProductModal',
    mixins: [ DetalleVentaModel ],
  data: () => ({
    min: 0,
    max: 100,
    slider: 0,
    }),
    model: {
      prop: 'value',
      event: 'input'
    },
    props: {
      value: {
        type: Boolean,
        default: false
      },
      defaultItem: {
        default: null,
        type: Object
      },
      findAllFunc: {
        default: null,
        type: Function
      },
    },
    watch: {
      value () {
        if (!this.value) {
          console.log('cerrando')
        } else {
          console.log('abierto')
        }
      },
    },
  methods: {
    async createDetalleVenta(){
      try {
        var resp = await detalleVentaService.create(this.defaultItemDV);
        console.log(resp);
      } catch (error) {
        console.log(error);
        this.$notify({
          group: 'foo',
          text: "Cantidad de decremento no valida!",
          type: 'error'
        });
      }
    },
    checkFormDec(){
      if(this.slider > this.defaultItem.stockTotal){
        this.$notify({
          group: 'foo',
          text: "Cantidad de decremento no valida!",
          type: 'error'
        });
      }else{
        this.defaultItemDV.cantidad = this.slider
        this.defaultItemDV.costoUnitario = null
        this.defaultItemDV.precioUnitario = null
        this.defaultItemDV.productoId = this.defaultItem.id
        this.createDetalleVenta()
        this.findAllFunc()
        this.dialogEvent()
      }
    },
    dialogEvent () {
      this.dialogDecr = false
      this.$emit('input')
      this.slider = 0
    },
  }
}
