import DetalleEntradaModel from '../../../models/DetalleEntradaModel'
import DetalleEntradaService from '../../../../../services/detalleEntrada/detalleEntrada.service'
export default {
    name: 'IncrementoStockModal',
    mixins: [ DetalleEntradaModel ],
    data: () => ({
      min: 1,
      max: 100,
      slider: 0,
      costoUnitario: 0,
    }),
    model: {
      prop: 'value',
      event: 'input'
    },
    props: {
      value: {
        type: Boolean,
        default: false
      },
      defaultItem: {
        default: null,
        type: Object
      },
      updateFunc: {
        default: null,
        type: Function
      },
    },
    watch: {
      value () {
        if (!this.value) {
          console.log('cerrando')
        } else {
          console.log('abierto')
        }
      },
    },
    methods: {
      async createDetalleEntrada(){
        try {
          var resp = await DetalleEntradaService.create(this.defaultItemDE)
          console.log(resp);
        } catch (error) {
          console.log(error);
          this.$notify({
            group: 'foo',
            text: error.response.data.title,
            type: 'error'
          });
        }
      },
      checkFormInc(){
        this.defaultItemDE.cantidad = this.slider
        this.defaultItemDE.productoId = this.defaultItem.id
        if(this.costoUnitario){
          this.defaultItem.costoUnitario = this.costoUnitario
          this.defaultItemDE.costoUnitario = this.costoUnitario
        }else{
          this.defaultItemDE.costoUnitario = this.defaultItem.costoUnitario
        }
        this.createDetalleEntrada()
        this.updateFunc()
        this.dialogEvent()
      },
      dialogEvent () {
        this.costoUnitario = 0
        this.dialogInc = false
        this.$emit('input')
        this.slider = 1
      },
    }
}
