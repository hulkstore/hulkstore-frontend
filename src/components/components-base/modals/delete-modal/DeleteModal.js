export default {
  name: "DeleteModal",
  data: () => ({}),
  model: {
    prop: "value",
    event: "input"
  },
  props: {
    value: {
      type: Boolean,
      default: false
    },
    defaultItem: {
      default: null,
      type: Object
    },
    deleteFunc: {
      default: null,
      type: Function
    },
    mensaje: {
      default: null,
      type: String
    }
  },
  watch: {
    value() {
      if (!this.value) {
        console.log("cerrando");
      } else {
        console.log("abierto");
      }
    }
  },
  methods: {
    dialogEvent() {
      this.dialogDelete = false;
      this.$emit("input");
    }
  }
};
