export default {
  data: () => ({
    ages:[
      '0-17',
      '18-29',
      '30-54',
      '54+'
    ]
  }),
  model: {
    prop: 'value',
    event: 'input'
  },
  props: {
    value: {
      type: Boolean,
      default: false
    },
    defaultItem: {
      default: null,
      type: Object
    }
  },
  watch: {
    value () {
      if (!this.value) {
        console.log('cerrado')
      } else {
        console.log('abierto')
      }
    }
  },
  methods: {
    dialogEvent () {
      this.dialog = false
      this.$emit('input')
    }
  }
}
