import DateService from "../../../../services/Date.service";
export default {
  data() {
    return {
      itemsX: {},
      arrayItems: []
    };
  },
  model: {
    prop: "value",
    event: "input"
  },
  props: {
    value: {
      type: Boolean,
      default: false
    },
    defaultItem: {
      type: Object,
      default: null
    }
  },
  watch: {
    value() {
      if (!this.value) {
        this.itemsX = {};
      } else {
        this.itemsX = Object.assign({}, this.defaultItem);
        console.log(this.itemsX);
        this.arrayItems = [
          // {
          //   name: "Razon Social/Nombre Proveedor",
          //   icon: "mdi-account-multiple",
          //   info: this.itemsX.invProveedor.razonSocial
          // },
          // {
          //   name: "NIT/CI",
          //   icon: "mdi-identifier",
          //   info: this.itemsX.invProveedor.nitCi
          // },
          // {
          //   name: "E-mail",
          //   icon: "mdi-card-account-mail",
          //   info: this.itemsX.invProveedor.email
          // },
          {
            name: "Fecha de Salida",
            icon: "mdi-calendar",
            info: DateService.parseDateToString(this.itemsX.fechaVenta)
          },
          // {
          //   name: "Tipo de Entrada",
          //   icon: "mdi-archive",
          //   info: this.itemsX.tipoEntrada
          // },
          // {
          //   name: "Monto Cancelado",
          //   icon: "mdi-cash",
          //   info: `${parseFloat(this.itemsX.montoCancelado).toFixed(2)} Bs.`
          // },
          {
            name: "Monto Total",
            icon: "mdi-currency-usd",
            info: `${parseFloat(this.itemsX.montoTotal).toFixed(2)} Bs.`
          },
          {
            name: "Creado por",
            icon: "mdi-tools",
            info: this.itemsX.createdBy
          },
          {
            name: "Creado en fecha y hora",
            icon: "mdi-tools",
            info: DateService.parseDateToString(this.itemsX.createdDate)
          },
          {
            name: "Modificado por",
            icon: "mdi-update",
            info: this.itemsX.lastModifiedBy
          },
          {
            name: "Modificado en fecha",
            icon: "mdi-update",
            info: DateService.parseDateToString(this.itemsX.lastModifiedDate)
          }
        ];
      }
    }
  },
  methods: {
    dialogEvent() {
      this.dialog = false;
      this.$emit("input");
    }
  }
};
