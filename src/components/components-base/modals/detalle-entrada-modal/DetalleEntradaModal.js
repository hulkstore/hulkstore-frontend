import DateService from "../../../../services/Date.service";
export default {
  data() {
    return {
      itemsX: {},
      arrayItems: []
    };
  },
  model: {
    prop: "value",
    event: "input"
  },
  props: {
    value: {
      type: Boolean,
      default: false
    },
    defaultItem: {
      type: Object,
      default: null
    }
  },
  watch: {
    value() {
      if (!this.value) {
        this.itemsX = {};
      } else {
        this.itemsX = Object.assign({}, this.defaultItem);
        console.log(this.itemsX);
        this.arrayItems = [
          {
            name: "Nombre Producto",
            icon: "mdi-dropbox",
            info: this.itemsX.producto.nombre
          },
          {
            name: "Codigo",
            icon: "mdi-qrcode",
            info: this.itemsX.producto.codigo
          },
          {
            name: "Tipo de Producto",
            icon: "mdi-archive",
            info: this.itemsX.producto.categoriaProducto
          },
          {
            name: "Unidad de Medida",
            icon: "mdi-ruler",
            info: `${this.itemsX.producto.unidadMedida.unidad} ${this.itemsX.producto.unidadMedida.sigla}`
          },
          {
            name: "Cantidad",
            icon: "mdi-counter",
            info: `${this.itemsX.cantidad} ${this.itemsX.producto.unidadMedida.sigla}`
          },
          {
            name: "Precio unitario",
            icon: "mdi-cash",
            info: `${parseFloat(this.itemsX.precioUnitarioCompra).toFixed(
              2
            )} Bs.`
          },
          {
            name: "Sub Total",
            icon: "mdi-currency-usd",
            info: `${parseFloat(this.itemsX.subTotal).toFixed(2)} Bs.`
          },
          {
            name: "Creado por",
            icon: "mdi-tools",
            info: this.itemsX.createdBy
          },
          {
            name: "Creado en fecha y hora",
            icon: "mdi-tools",
            info: DateService.parseDateToString(this.itemsX.createdDate)
          },
          {
            name: "Modificado por",
            icon: "mdi-update",
            info: this.itemsX.lastModifiedBy
          },
          {
            name: "Modificado en fecha",
            icon: "mdi-update",
            info: DateService.parseDateToString(this.itemsX.lastModifiedDate)
          }
        ];
      }
    }
  },
  methods: {
    dialogEvent() {
      this.dialog = false;
      this.$emit("input");
    }
  }
};
