import EtiquetaService from "../../../../../services/product-detail/etiqueta/etiqueta.service";
import EtiquetaModel from "../../../../components-base/models/product-detail/EtiquetaModel";
export default {
  name: "etiqueta-model",
  mixins: [EtiquetaModel],
  data: () => ({
    loading: false,
    menu: false,
    type: "hex"
  }),
  model: {
    prop: "value",
    event: "input"
  },
  props: {
    value: {
      type: Boolean,
      default: false
    },
    editItem: {
      default: null,
      type: Object
    },
    reloadFunc: {
      default: null,
      type: Function
    }
  },
  watch: {
    value() {
      if (!this.value) {
        this.defaultItem = {};
      } else {
        this.$v.$reset();
        this.defaultItem = Object.assign({}, this.editItem);
      }
    }
  },
  computed: {
    formTitle() {
      return this.defaultItem.id ? "Editar Etiqueta" : "Crear Etiqueta";
    },
    isLoading() {
      return this.loading;
    }
  },
  methods: {
    dialogEvent() {
      this.dialog = false;
      this.$v.$reset();
      this.$emit("input");
    },
    checkForm() {
      this.$v.$touch();
      if (!this.$v.$invalid) {
        if (this.defaultItem.id) {
          this.updateEtiqueta();
        } else {
          this.createEtiqueta();
        }
      }
    },
    async createEtiqueta() {
      this.loading = true;
      try {
        const response = await EtiquetaService.create(this.defaultItem);
        console.log(response);
        this.$notify({
          group: "foo",
          text: "El etiqueta se creo exitosamente!",
          type: "success"
        });
        this.loading = false;
        this.reloadFunc();
        this.dialogEvent();
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
      }
    },
    async updateEtiqueta() {
      this.loading = true;
      try {
        const response = await EtiquetaService.update(this.defaultItem);
        this.defaultItem = response;
        this.$notify({
          group: "foo",
          text: "El etiqueta se actualizo exitosamente!",
          type: "success"
        });
        this.loading = false;
        this.reloadFunc();
        this.dialogEvent();
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
      }
    }
  }
};
