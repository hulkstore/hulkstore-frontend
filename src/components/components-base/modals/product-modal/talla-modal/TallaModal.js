import TallaService from "../../../../../services/product-detail/talla/talla.service";
import TallaModel from "../../../../components-base/models/product-detail/TallaModel";
export default {
  name: "talla-model",
  mixins: [TallaModel],
  data: () => ({
    loading: false,
    menu: false,
    type: "hex"
  }),
  model: {
    prop: "value",
    event: "input"
  },
  props: {
    value: {
      type: Boolean,
      default: false
    },
    editItem: {
      default: null,
      type: Object
    },
    reloadFunc: {
      default: null,
      type: Function
    }
  },
  watch: {
    value() {
      if (!this.value) {
        this.defaultItem = {};
      } else {
        this.$v.$reset();
        this.defaultItem = Object.assign({}, this.editItem);
      }
    }
  },
  computed: {
    formTitle() {
      return this.defaultItem.id ? "Editar Talla" : "Crear Talla";
    },
    isLoading() {
      return this.loading;
    }
  },
  methods: {
    dialogEvent() {
      this.dialog = false;
      this.$v.$reset();
      this.$emit("input");
    },
    checkForm() {
      this.$v.$touch();
      if (!this.$v.$invalid) {
        if (this.defaultItem.id) {
          this.updateTalla();
        } else {
          this.createTalla();
        }
      }
    },
    async createTalla() {
      this.loading = true;
      try {
        const response = await TallaService.create(this.defaultItem);
        console.log(response);
        this.$notify({
          group: "foo",
          text: "El talla se creo exitosamente!",
          type: "success"
        });
        this.loading = false;
        this.reloadFunc();
        this.dialogEvent();
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
      }
    },
    async updateTalla() {
      this.loading = true;
      try {
        const response = await TallaService.update(this.defaultItem);
        this.defaultItem = response;
        this.$notify({
          group: "foo",
          text: "El talla se actualizo exitosamente!",
          type: "success"
        });
        this.loading = false;
        this.reloadFunc();
        this.dialogEvent();
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
      }
    }
  }
};
