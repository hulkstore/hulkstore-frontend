import CategoriaService from "../../../../../services/product-detail/categoria/categoria.service";
import CategoriaModel from "../../../../components-base/models/product-detail/CategoriaModel";
export default {
  name: "categoria-model",
  mixins: [CategoriaModel],
  data: () => ({
    loading: false,
    menu: false,
    type: "hex"
  }),
  model: {
    prop: "value",
    event: "input"
  },
  props: {
    value: {
      type: Boolean,
      default: false
    },
    editItem: {
      default: null,
      type: Object
    },
    reloadFunc: {
      default: null,
      type: Function
    }
  },
  watch: {
    value() {
      if (!this.value) {
        this.defaultItem = {};
      } else {
        this.$v.$reset();
        this.defaultItem = Object.assign({}, this.editItem);
      }
    }
  },
  computed: {
    formTitle() {
      return this.defaultItem.id ? "Editar Talla" : "Crear Talla";
    },
    isLoading() {
      return this.loading;
    }
  },
  methods: {
    dialogEvent() {
      this.dialog = false;
      this.$v.$reset();
      this.$emit("input");
    },
    checkForm() {
      this.$v.$touch();
      if (!this.$v.$invalid) {
        if (this.defaultItem.id) {
          this.updateCategoria();
        } else {
          this.createCategoria();
        }
      }
    },
    async createCategoria() {
      this.loading = true;
      try {
        const response = await CategoriaService.create(this.defaultItem);
        console.log(response);
        this.$notify({
          group: "foo",
          text: "El categoria se creo exitosamente!",
          type: "success"
        });
        this.loading = false;
        this.reloadFunc();
        this.dialogEvent();
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
      }
    },
    async updateCategoria() {
      this.loading = true;
      try {
        const response = await CategoriaService.update(this.defaultItem);
        this.defaultItem = response;
        this.$notify({
          group: "foo",
          text: "El categoria se actualizo exitosamente!",
          type: "success"
        });
        this.loading = false;
        this.reloadFunc();
        this.dialogEvent();
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
      }
    }
  }
};
