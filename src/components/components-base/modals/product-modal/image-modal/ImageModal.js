import ImageService from "../../../../../services/image/image.service";

export default {
  name: "image-modal",
  data: () => ({
    imageUrl: null,
    overlay: false,
    hover: false,
    hoverC: false,
    defaultItem: {},
    avatar: {},
    saving: false,
    saved: false,
    errorDialog: false, //---------------- IMAGE
    errorText: "",
    imageFile: null,
    uploadFieldName: "file",
    maxSize: 1024,
    serverUrl: process.env.VUE_APP_IMAGE_BASE_URL
  }),
  model: {
    prop: "value",
    event: "input",
    defaultImage: null
  },
  props: {
    value: {
      default: false,
      type: Boolean
    },
    productId: {
      default: null,
      type: Number
    },
    defaultImage: {
      default: null,
      type: Object
    },
    reloadFunc: {
      default: null,
      type: Function
    }
  },
  watch: {
    value() {
      if (!this.value) {
        this.defaultItem = {};
        this.avatar.imageURL = null;
        this.hover = false;
        this.overlay = false;
        this.hoverC = false;
        this.dialogEvent();
      } else {
        this.avatar.imageURL = null;
        this.avatar.imageFile = null;
        this.hover = false;
        this.overlay = false;
        this.defaultItem = Object.assign({}, this.defaultImage);
        if (this.defaultItem.url) {
          this.avatar.imageURL = this.serverUrl + this.defaultItem.url;
        }else{
          this.avatar.imageURL=null
        }
      }
    },
    imageFileExist() {
      if (this.avatar.imageFile === null) {
        this.avatar.imageURL == null;
      }
    },
  },
  computed: {
    formTitle() {
      return this.defaultItem.id ? "Editar Imagen" : "Nueva Imagen";
    }
  },
  methods: {
    dialogEvent() {
      this.dialog = false;
      this.$emit("input");
    },
    checkForm() {
      if (this.defaultItem.id) {
        this.updateImage();
      } else {
        this.createImage();
      }
    },
    launchFilePicker() {
      this.$refs.file.click();
    },
    closeDialog() {
      this.errorDialog = false;
    },
    onFileChange(fieldName, file) {
      const { maxSize } = this;
      let imageFile = file[0];
      if (file.length > 0) {
        let size = imageFile.size / maxSize / maxSize;
        if (!imageFile.type.match("image.*")) {
          this.errorDialog = true;
          this.errorText = "Elija un archivo de imagen";
        } else if (size > 1) {
          this.errorDialog = true;
          this.errorText =
            "¡Tu archivo es demasiado grande! Seleccione una imagen de menos de 1 MB";
          this.avatar.imageURL = null;
          this.avatar.imageFile = null;
          file = null;
        } else {
          this.avatar = {
            imageURL: URL.createObjectURL(imageFile),
            imageFile: imageFile,
            accessOne: false,
            accessTwo: true
          };
        }
      }
    },
    async createImage() {
      this.saving = true;
      let formData = new FormData();
      formData.append("productId", this.productId);
      formData.append("file", this.avatar.imageFile);
      try {
        const response = await ImageService.createImage(formData);
        const img = process.env.VUE_APP_IMAGE_BASE_URL + response.imageUrl;
        this.avatar = {
          imageURL: img,
          accessOne: true,
          accessTwo: false
        };
        this.saved = false;
        this.saving = false;
        this.$notify({
          group: "foo",
          text: "Exito: Su imagen fue guardada exitosamente",
          type: "success"
        });
        this.reloadFunc();
        this.dialogEvent();
        // }, 1000)
      } catch (error) {
        this.$notify({
          group: "foo",
          text: "Error: " + error.response.data.error,
          type: "error"
        });
      }
    },
    async updateImage() {
      this.saving = true;
      let formData = new FormData();
      formData.append("imageId", this.defaultItem.id);
      formData.append("file", this.avatar.imageFile);
      try {
        const response = await ImageService.updateImage(formData);
        const img = process.env.VUE_APP_IMAGE_BASE_URL + response.imageUrl;
        this.avatar = {
          imageURL: img,
          accessOne: true,
          accessTwo: false
        };
        this.saved = false;
        this.saving = false;
        this.$notify({
          group: "foo",
          text: "Exito: Su imagen fue actualizada exitosamente",
          type: "success"
        });
        this.reloadFunc();
        this.dialogEvent();
        // }, 1000)
      } catch (error) {
        this.$notify({
          group: "foo",
          text: "Error: " + error.response.data.error,
          type: "error"
        });
      }
    }
  }
};
