import ColorService from "../../../../../services/product-detail/color/color.service";
import ColorModel from "../../../../components-base/models/product-detail/ColorModel";
export default {
  name: "color-model",
  mixins: [ColorModel],
  data: () => ({
    loading: false,
    menu: false,
    type: "hex"
  }),
  model: {
    prop: "value",
    event: "input"
  },
  props: {
    value: {
      type: Boolean,
      default: false
    },
    editItem: {
      default: null,
      type: Object
    },
    reloadFunc: {
      default: null,
      type: Function
    }
  },
  watch: {
    value() {
      if (!this.value) {
        this.defaultItem = {};
      } else {
        this.$v.$reset();
        this.defaultItem = Object.assign({}, this.editItem);
      }
    }
  },
  computed: {
    formTitle() {
      return this.defaultItem.id ? "Editar Color" : "Crear Color";
    },
    swatchStyle() {
      const { color, menu } = this;
      return {
        backgroundColor: color,
        height: "50px",
        width: "60px",
        marginLeft: "30px",
        borderRadius: menu ? "50%" : "4px",
        transition: "border-radius 300ms ease-in-out"
      };
    },
    isLoading() {
      return this.loading;
    }
  },
  methods: {
    dialogEvent() {
      this.dialog = false;
      this.$v.$reset();
      this.$emit("input");
    },
    checkForm() {
      this.$v.$touch();
      if (!this.$v.$invalid) {
        if (this.defaultItem.id) {
          this.updateColor();
        } else {
          this.createColor();
        }
      }
    },
    async createColor() {
      this.loading = true;
      try {
        console.log(this.defaultItem);
        const response = await ColorService.create(this.defaultItem);
        console.log(response);
        this.$notify({
          group: "foo",
          text: "El color se creo exitosamente!",
          type: "success"
        });
        this.loading = false;
        this.reloadFunc();
        this.dialogEvent();
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
      }
    },
    async updateColor() {
      this.loading = true;
      try {
        const response = await ColorService.update(this.defaultItem);
        this.defaultItem = response;
        console.log("esto es la actualizacion");
        this.$notify({
          group: "foo",
          text: "El color se actualizo exitosamente!",
          type: "success"
        });
        this.loading = false;
        this.reloadFunc();
        this.dialogEvent();
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
      }
    }
  }
};
