import DateService from "../../../../services/Date.service";
export default {
  data: () => ({
    base: process.env.VUE_APP_IMAGE_BASE_URL,
    itemsX: {},
    arrayItems: [],
    auxarray: []
  }),
  model: {
    prop: "value",
    event: "input"
  },
  props: {
    value: {
      type: Boolean,
      default: false
    },
    defaultItem: {
      default: null,
      type: Object
    }
  },
  watch: {
    value() {
      if (!this.value) {
        this.itemsX = {};
      } else {
        this.itemsX = Object.assign({}, this.defaultItem);
        console.log(this.itemsX);
        this.arrayItems = [
          {
            name: "Nombres",
            icon: "mdi-account-tie",
            info: this.itemsX.nombre
          },
          {
            name: "Usuario",
            icon: "mdi-account-box",
            info: this.itemsX.username
          },
          {
            name: "Correo Email",
            icon: "mdi-email",
            info: this.itemsX.email
          },
          {
            name: "Nro. Tarjeta",
            icon: "mdi-account-cash",
            info: this.itemsX.nroTarjeta
          },
          {
            name: "Sucursal",
            icon: "mdi-store",
            info: `Sucursal ${this.itemsX.sucursalId}`
          },
          {
            name: "Cuenta Activa",
            icon: "mdi-marker",
            info: this.itemsX.estado ? "Si" : "No"
          },
          {
            name: "Roles",
            icon: "mdi-marker",
            info: "nothing"
          },
          {
            name: "Creado por",
            icon: "mdi-tools",
            info: this.itemsX.createdBy
          },
          {
            name: "Creado en fecha",
            icon: "mdi-tools",
            info: DateService.parseDateToString(this.itemsX.createdDate)
          },
          {
            name: "Modificado por",
            icon: "mdi-update",
            info: this.itemsX.lastModifiedBy
          },
          {
            name: "Modificado en fecha",
            icon: "mdi-update",
            info: DateService.parseDateToString(this.itemsX.lastModifiedDate)
          },
          {
            name: "Imagen",
            icon: "mdi-picture",
            info: this.itemsX.logo1,
            type: this.itemsX.logo1ContentType
          }
        ];
      }
    },
  },
  methods: {
    dialogEvent() {
      this.dialog = false;
      this.$emit("input");
    },
  },
  mounted() {
    console.log(this.itemsX);
  }
};
