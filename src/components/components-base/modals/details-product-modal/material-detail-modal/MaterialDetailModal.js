import DateService from "../../../../../services/Date.service";
export default {
  data() {
    return {
      itemsX: {},
      arrayItems: []
    };
  },
  model: {
    prop: "value",
    event: "input"
  },
  props: {
    value: {
      type: Boolean,
      default: false
    },
    defaultItem: {
      type: Object,
      default: null
    }
  },
  watch: {
    value() {
      if (!this.value) {
        this.itemsX = {};
      } else {
        this.itemsX = Object.assign({}, this.defaultItem);
        this.arrayItems = [
          {
            name: "Material",
            icon: "mdi-hammer",
            info: this.itemsX.nombre
          },
          {
            name: "Descripcion",
            icon: "mdi-marker",
            info: this.itemsX.descripcion
          },
          {
            name: "Creado por",
            icon: "mdi-tools",
            info: this.itemsX.createdBy
          },
          {
            name: "Creado en fecha y hora",
            icon: "mdi-tools",
            info: DateService.parseDateToString(this.itemsX.createdDate)
          },
          {
            name: "Modificado por",
            icon: "mdi-update",
            info: this.itemsX.lastModifiedBy
          },
          {
            name: "Modificado en fecha",
            icon: "mdi-update",
            info: DateService.parseDateToString(this.itemsX.lastModifiedDate)
          }
        ];
      }
    }
  },
  methods: {
    dialogEvent() {
      this.dialog = false;
      this.$emit("input");
    }
  }
};
