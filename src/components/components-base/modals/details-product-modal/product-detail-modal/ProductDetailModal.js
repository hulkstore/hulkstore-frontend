import DateService from "../../../../../services/Date.service";
export default {
  data() {
    return {
      itemsX: {},
      arrayItems: []
    };
  },
  model: {
    prop: "value",
    event: "input"
  },
  props: {
    value: {
      type: Boolean,
      default: false
    },
    defaultItem: {
      type: Object,
      default: null
    }
  },
  watch: {
    value() {
      if (!this.value) {
        this.itemsX = {};
      } else {
        this.itemsX = Object.assign({}, this.defaultItem);
        this.arrayItems = [
          {
            name: "Nombre de Producto",
            icon: "mdi-dropbox",
            info: this.itemsX.nombre
          },
          {
            name: "Codigo",
            icon: "mdi-data-matrix-plus",
            info: this.itemsX.codigo
          },
          {
            name: "Tipo de producto",
            icon: "mdi-tshirt-crew",
            info: this.itemsX.categoriaProducto
          },
          {
            name: "Unidad de Medida",
            icon: "mdi-ruler",
            info: `${this.itemsX.unidadMedida.unidad} ${this.itemsX.unidadMedida.sigla}`
          },
          {
            name: "Calidad",
            icon: "mdi-ticket",
            info: this.itemsX.calidad
          },
          {
            name: "Color",
            icon: "mdi-format-color-fill",
            info: this.itemsX.color.color
          },
          {
            name: "Marca",
            icon: "mdi-shopping",
            info: this.itemsX.marca.nombre
          },
          {
            name: "Estado",
            icon: "mdi-information-variant",
            info: this.itemsX.estado ? "Activo" : "Inactivo"
          },
          {
            name: "Creado por",
            icon: "mdi-tools",
            info: this.itemsX.createdBy
          },
          {
            name: "Creado en fecha y hora",
            icon: "mdi-tools",
            info: DateService.parseDateToString(this.itemsX.createdDate)
          },
          {
            name: "Modificado por",
            icon: "mdi-update",
            info: this.itemsX.lastModifiedBy
          },
          {
            name: "Modificado en fecha",
            icon: "mdi-update",
            info: DateService.parseDateToString(this.itemsX.lastModifiedDate)
          }
        ];
      }
    }
  },
  methods: {
    dialogEvent() {
      this.dialog = false;
      this.$emit("input");
    }
  }
};
