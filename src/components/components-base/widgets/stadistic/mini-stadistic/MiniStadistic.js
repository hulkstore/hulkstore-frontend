export default {
  props: {
    icon: String,
    title: String,
    subTitle: String,
    color: String,
  }
}
