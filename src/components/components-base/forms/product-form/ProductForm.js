import ProductService from "../../../../services/product/product.service";
import ColorService from "../../../../services/product-detail/color/color.service";
import CategoriaService from "../../../../services/product-detail/categoria/categoria.service";
import TallaService from "../../../../services/product-detail/talla/talla.service";
import EtiquetaService from "../../../../services/product-detail/etiqueta/etiqueta.service";
import ImageService from "../../../../services/image/image.service";
import ProductModel from "../../models/ProductModel";

import DeleteModal from "../../modals/delete-modal";
import ImageModal from "../../modals/product-modal/image-modal";

export default {
  title: "- Crear/Editar Producto",
  name: "product-form",
  mixins: [ProductModel],
  components: { DeleteModal, ImageModal },
  data: () => ({
    e1: 1,
    temporada: [],
    productId: null,
    productId1: null,
    temporadaDialog: false,
    temporadaItem: {},
    temporadaRules: {
      categoria: [v => !!v || "Es requerido"],
      fechaIni: [v => !!v || "Se requiere Fecha de Inicio"],
      fechaFin: [v => !!v || "Se requiere Fecha Final"]
    },
    isEditing: false,
    loadingTemporada: false,

    loading: false,
    colors: [],
    categoriaProductos: [],
    unidadesMedida: [],
    marcas: [],
    tiposProducto: ["REPUESTO", "TELA", "ACCESORIO"],
    calidadProducto: ["PRIMERA", "SEGUNDA", "TERCERA"],
    temporadasItems: ["BAJA", "MEDIA", "ALTA"],
    sortBy: [""],
    sortDesc: [true, false],
    hover: false,
    hoverC: false,
    dialog: false,
    dialogDelete: false,
    deleteFunc: null,
    defaultImage: {},
    // Image
    images: [],
    serverUrl: process.env.VUE_APP_IMAGE_BASE_URL,
    avatar: {},
    saving: false,
    saved: false,
    errorDialog: null, //---------------- IMAGE
    errorText: "",
    imageFile: null,
    uploadFieldName: "file",
    maxSize: 1024,
    message: "",
    colorsCopy: [],
    searchTerm: "",
    tallas: [],
    categorias: [],
    etiquetas: [],
    generos: ["TERROR", "ACCION", "COMEDIA", 'FICCION', 'AVENTURA', 'DRAMA', 'FANTASIA', 'SUSPENSO', 'RELIGION' ],
    sexo: ["MUJER", "VARON", "UNISEX"],
    idiomas: ["ES", "EN", "JP"],
    currentCategoria: null,
    rules: {
      productos: {
        nombre: [v => !!v || "El campo es requerido"],
        categoriaId: [v => !!v || "El campo es requerido"],
        etiquetaId: [v => !!v || "El campo es requerido"]
      },
      comics: {
        titulo: [v => !!v || "El campo es requerido"],
        genero: [v => !!v || "El campo es requerido"],
        volumen: [v => !!v || "El campo es requerido"],
        anioLanzamiento: [v => !!v || "El campo es requerido"],
        idioma: [v => !!v || "El campo es requerido"]
      },
      camisetas: {
        sexo: [v => !!v || "El campo es requerido"],
        tallaId: [v => !!v || "El campo es requerido"],
        colorId: [v => !!v || "El campo es requerido"]
      }
    },
  }),
  watch: {
  },
  mounted() {
    this.findAllColors();
    this.findAllCategoria();
    this.findAllEtiqueta();
    this.findAllTallas();
    if (this.$route.params.id) {
      this.productId1 = parseInt(this.$route.params.id)
      this.getProductByProductId();
      this.findAllImages();
    }
  },
  computed: {

    formTitle() {
      return this.$route.params.id ? "Editar Producto" : "Crear Producto";
    },
    buttonText() {
      if (parseInt(this.e1) === 2) return "siguiente";
      else if (parseInt(this.e1) === 3) return "Finalizar";
      else
        return this.$route.params.id || this.productId ? "Actualizar" : "Crear";
    },
    isLoading() {
      return this.loading;
    },
    isEditingStep() {
      return this.$route.params.id && true;
    }
  },
  methods: {
    async findAllTemporadas() {
      this.temporada = [];
      if (this.$route.params.id || this.productId) {
        try {
          const res = await TemporadaService.findAllByProductId(
            this.$route.params.id || this.productId
          );
          this.temporada = res;
        } catch (error) {
          //console.log(error);
        }
      } else this.temporada = [];
    },
    async getProductByProductId() {
      try {
        const response = await ProductService.findOneVM(this.$route.params.id);
        this.defaultItem = response;
      } catch (error) {
        this.$router.push({ name: 'no encontrado', params: { pathMatch: ["edit-product", this.$route.params.id] }})
        //console.log(error);
      }
    },
    checkForm() {
      if (parseInt(this.e1) === 1) {  
        let data = this.currentCategoria === 'COMICS' ? this.$refs.formComic.validate() : this.currentCategoria === 'CAMISETA'? this.$refs.formCamiseta.validate() : true;
        if (this.$refs.formProduct.validate() && data) {
          if (this.$route.params.id) {
            this.updateProduct();
            // this.e1 = 2;
          } else {
            this.createProduct();
            // this.e1 = 2;
          }
        }
      } else if (parseInt(this.e1) === 2) this.e1 = 3;
      else if (parseInt(this.e1) === 3) {
        this.$router.push({ name: "productos" });
        this.$notify({
          group: "foo",
          text: "Se guardaron los cambios con exito",
          type: "success"
        });
      }
    },
    async createProduct() {
      this.loading = true;
      try {
        this.defaultItem.estado = false;
        const res = await ProductService.create(this.defaultItem);
        this.productId = res.id;
        this.defaultItem = res;
        this.$notify({
          group: "foo",
          text: "El producto se creo exitosamente!",
          type: "success"
        });
        this.loading = false;
        const id = res.id;
        this.$router.push({ name: "editar producto", params: { id } });
        this.e1 = 2;
      } catch (error) {
        //console.log(error);
        this.$notify({
          group: "foo",
          text: "El producto no se pudo crear! </br> " + error.response.data.title,
          type: "error"
        });
        this.loading = false;
      }
    },
    async updateProduct() {
      this.loading = true;
      try {
        this.defaultItem.id = parseInt(this.$route.params.id)
        const response = await ProductService.update(this.defaultItem);
        //console.log(response);
        this.defaultItem = response;
        this.productId = response.id;
        this.loading = false;
        this.$notify({
          group: "foo",
          text: "El producto se actualizo exitosamente!",
          type: "success"
        });
        this.e1 = 2;
        //this.$router.push({ name: "productos" });
      } catch (error) {
        //console.log(error);
        this.$notify({
          group: "foo",
          text: "El producto no se pudo actualizar! </br>" + error.response.data.error,
          type: "error"
        });
        this.loading = false;
      }
    },
    // CALL COMPLEMENTS
    async findAllColors() {
      try {
        const response = await ColorService.findAll(
          0,
          200,
          this.sortBy,
          this.sortDesc
        );
        ////console.log('Colores :', response.data)
        this.colors = response.data;
        this.colorsCopy = [...response.data];
      } catch (error) {
        //console.log(error);
      }
    },
    // CATEGORIAS
    async findAllCategoria() {
      try {
        const response = await CategoriaService.findAll(0, 100, this.sortBy, this.sortDesc);
        this.categorias = response.data;
      } catch (error) {
        //console.log(error)
      }
    },
    // TALLAS
    async findAllTallas() {
      try {
        const response = await TallaService.findAll(
          0,
          100,
          this.sortBy,
          this.sortDesc
        );
        this.tallas = response.data;
      } catch (error) {
        //console.log(error);
      }
    },
    async findAllEtiqueta() {
      try {
        const response = await EtiquetaService.findAll(
          0,
          100,
          this.sortBy,
          this.sortDesc
        );
        this.etiquetas = response.data;
      } catch (error) {
        //console.log(error);
      }
    },
    openModalUploadImage(item) {
      this.dialog = true;
      if (item) {
        this.defaultImage = Object.assign({}, item);
      } else {
        this.defaultImage = Object.assign({}, {});
      }
      ////console.log("Aparezco...",this.defaultImage)
    },
    openModalDeleteImage(item) {
      this.dialogDelete = true;
      this.defaultImage = Object.assign({}, item);
      this.deleteFunc = this.deleteImage;
      this.message = "imagen";
    },
    async findAllImages() {
      try {
        const response = await ImageService.findAll(parseInt(this.$route.params.id));
        this.images = response.data;
      } catch (error) {
        //console.log(error);
      }
    },
    async deleteImage() {
      try {
        const response = await ImageService.delete(this.defaultImage.id);
        console.log(response)
        this.dialogDelete = false;
        this.findAllImages();
        this.$notify({
          group: "foo",
          text: "La imgen se elimino exitosamente!",
          type: "success"
        });
        //console.log(response);
      } catch (error) {
        this.$notify({
          group: "foo",
          text: "La imagen no se pudo eliminar!",
          type: "error"
        });
      }
    },
    searchColors() {
      if (!this.searchTerm) {
        this.colors = this.colorsCopy;
      }
      this.colors = this.colorsCopy.filter((color) => {
        return this.getItemText(color).toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1
      })
    },
    getItemText(item) {
      return `${item.id} ${item.nombre} ${item.codigo}`;
    },
    getCategoria(valueCat) {
      this.currentCategoria = valueCat
      if (this.currentCategoria !== 'COMICS') {
        this.defaultItem.titulo = null
        this.defaultItem.genero = null
        this.defaultItem.volumen = null
        this.defaultItem.anioLanzamiento = null
        this.defaultItem.idioma = null
      }
      if (this.currentCategoria !== 'CAMISETA') {
        this.defaultItem.sexo = null
        this.defaultItem.colorId = null
        this.defaultItem.tallaId = null
      }
      return this.currentCategoria
    }
  }
};
