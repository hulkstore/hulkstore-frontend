import UserService from "../../../../services/user/user.service";
import { mapState, mapMutations, mapActions } from "vuex";
import UserModel from "../../models/UserModel";
import RolesService from '../../../../services/role/role.service'
// import AccountService from "../../../../services/account/account.service";
export default {
  title: "- Crear/Editar Usuario",
  name: "user-form",
  mixins: [UserModel],
  data: () => ({
    showPassword: false,
    password: "",
    passwordRules: {
      passwordCreate: [
        v => !!v || "Contraseña es requerida",
        // v => v.length >= 4 || "Contraseña debe tener al menos 4 caracteres"
      ],
      passwordEdit: [
        v => v >= 4 || "Contraseña debe tener al menos 4 caracteres"
      ]
    },
    roles: [],
  }),
  mounted() {
    this.findAllRoles()
    if (this.$route.params.id) {
      this.getUserById();
    }
  },
  computed: {
    ...mapState(["accountX", "account"]),
    formTitle() {
      return this.$route.params.id ? "Editar Usuario" : "Crear Usuario";
    },
    buttonText() {
      return this.$route.params.id ? "Actualizar" : "Crear";
    },
  },
  methods: {
    ...mapMutations({ setAccountX: "SET_ACCOUNT" }),
    ...mapActions("accountModule", ["currentAccount"]),
    async findAllRoles() {
      try {
        const response = await RolesService.findAllRoles()
        response.map(item => this.roles.push(item.nombre))
      } catch(error) {
        console.log(error)
      }
    },
    async getUserById() {
      try {
        const response = await UserService.findOne(this.$route.params.id);
        // console.log(response);
        this.defaultItem = response;
      } catch (error) {
        this.$router.push({ name: "404" });
        console.log(error);
      }
    },
    checkForm() {
      this.$v.$touch();
      if (!this.$v.$invalid && this.$refs.formUser.validate()) {
        if (this.$route.params.id) {
          this.updateUser();
        } else {
          this.createUser();
        }
      } else {
        this.$notify({
          group: "foo",
          text: "Por favor llene todos los campos",
          type: "error"
        });
      }
    },
    async createUser() {
      try {
        this.defaultItem.password = this.password;
        // console.log("---- create metod------------");
        // console.log(this.defaultItem);
        await UserService.create(this.defaultItem);
        this.$router.push({ name: "lista de usuarios" });
        this.$notify({
          group: "foo",
          text: "El usuario se creo exitosamente!",
          type: "success"
        });
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
      }
    },
    async updateUser() {
      try {
        // console.log(this.defaultItem);
        // const res = await AccountService.findAccount();
        // console.log(res);
        this.defaultItem.password = this.password;
        await UserService.update(this.defaultItem);
        this.$router.push({ name: "lista de usuarios" });
        this.$notify({
          group: "foo",
          text: "El usuario se actualizo exitosamente!",
          type: "success"
        });
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
      }
    }
  }
};
