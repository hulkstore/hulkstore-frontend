import DosageService from "../../../services/dosage/dosage.service";
import DeleteModal from "../../components-base/modals/delete-modal";
import DetailDosageModal from "../../components-base/modals/detail-dosage-modal";
import cajaDosficacionService from "../../../services/cajaDosificacion/cajaDosficacion.service";
import { mapState } from "vuex";
export default {
  title: "- Dosificaciones",
  name: "dosage-component",
  components: { DetailDosageModal, DeleteModal },
  data: () => ({
    search: "",
    elementsPerPage: 10,
    pagination: {},
    loading: false,
    page: 1,
    size: 5,
    dialogDelete: false,
    dialogDetail: false,
    defaultItem: {},
    deleteFunc: null,
    mensaje: "dosificacion...",
    dialogLCD: false,
    flagError: false,
    complex: {
      headers: [
        {
          text: "#",
          value: "#",
          align: "center",
          sortable: false
        } /*
        {
          text: "Nro. Autorizacion",
          value: "numeroAutorizacion",
          sortable: true
        },*/,
        {
          text: "Nombre",
          value: "nombre",
          sortable: true,
          align: "center"
        },
        {
          text: "Codigo",
          value: "codigoDosificacion",
          sortable: true,
          align: "center"
        },
        {
          text: "Nro. Recibos",
          value: "numeroFactura",
          sortable: true,
          align: "center"
        },
        {
          text: "Valor Inicial",
          value: "valorInicio",
          sortable: true,
          align: "center"
        },
        {
          text: "Valor Final",
          value: "valorFinal",
          sortable: true,
          align: "center"
        },
        {
          text: "Tipo de Dosificacion",
          value: "tipoDosificacion",
          sortable: true,
          align: "center"
        },
        {
          text: "Fecha Limite de Emision",
          value: "fechaLimiteEmision",
          sortable: true,
          align: "center"
        },
        {
          text: "Estado",
          value: "estado",
          sortable: true,
          align: "center"
        },
        {
          text: "Acciones",
          value: "action",
          sortable: false,
          align: "center"
        }
      ],
      items: []
    },
    actions: [
      {
        text: "Detalle",
        icon: "mdi-eye"
      },
      {
        text: "Editar",
        icon: "mdi-pencil"
      },
      {
        text: "Eliminar",
        icon: "mdi-close"
      }
    ],
    sortBy: [""],
    sortDesc: [true, false]
  }),
  watch: {
    page() {
      this.initialize();
    },
    elementsPerPage() {
      this.initialize();
    },
    sortDesc: {
      handler() {
        this.initialize();
      }
    },
    sucursal() {
      this.initialize();
    }
  },
  computed: {
    ...mapState("sucursalModule", ["sucursal"])
  },
  mounted() {
    this.initialize();
  },
  methods: {
    initialize() {
      if (parseInt(this.search.length) >= 3) {
        console.log("1");
        this.searchByKeyword(this.search);
      } else {
        if (parseInt(this.search.length) === 0) {
          this.findAllDosages();
          console.log("2");
        }
      }
    },
    async findAllDosages() {
      this.loading = true;
      try {
        const response = await DosageService.findAllBySucursalId(
          this.page - 1,
          this.elementsPerPage,
          this.sortBy,
          this.sortDesc,
          this.sucursal.id || 1
        );
        this.complex.items = response.data;
        console.log(this.complex.items);

        this.pagination.totalElements = parseInt(response.elements);
        this.loading = false;
      } catch (error) {
        console.log(error);
      }
    },
    openModal(item, event) {
      this.defaultItem = Object.assign({}, item);
      const text = event.target.innerText;
      if (this.actions[2].text === text) {
        this.dialogDelete = true;
        this.deleteFunc = this.deleteItem;
      } else if (this.actions[1].text === text) {
        const id = item.id;
        this.$router.push({ name: "editar talonario", params: { id } });
      } else {
        if (this.actions[0].text === text) {
          this.dialogDetail = true;
        }
      }
    },
    flagDeleteRelation() {
      try {
        DosageService.delete(this.defaultItem.id);
        this.deleteFunc = this.deleteItem;
        this.dialogDelete = false;
        this.findAllDosages();
        this.flagError = false;
        this.$notify({
          group: "foo",
          text: "La dosificacion se elimino exitosamente!",
          type: "success"
        });
      } catch (error) {
        this.$notify({
          group: "foo",
          text: "La dosificasion no se pudo eliminar!",
          type: "error"
        });
      }
    },
    async deleteItem() {
      try {
        let response = await cajaDosficacionService.findAll(
          this.page - 1,
          10,
          this.sortBy,
          this.sortDesc
        );
        response.data.forEach(element => {
          if (element.dosificacionId === this.defaultItem.id) {
            this.flagError = true;
          }
        });
        if (this.flagError) {
          this.flagError = false;
          this.dialogDelete = false;
          this.dialogLCD = true;
        } else {
          this.flagError = false;
          this.flagDeleteRelation();
        }
      } catch (error) {
        console.log(error);
      }
    },
    async searchByKeyword(event) {
      this.search = event;
      if (parseInt(this.search.length) >= 3) {
        this.loading = true;
        try {
          const response = await DosageService.searchBySucursalId(
            this.search,
            this.page - 1,
            this.elementsPerPage,
            this.sortBy,
            this.sortDesc,
            this.sucursal.id || localStorage.getItem("currentSuc")
          );
          this.complex.items = response.data;
          this.pagination.totalElements = parseInt(response.elements);
          this.loading = false;
        } catch (error) {
          console.log(error);
        }
      } else {
        if (parseInt(this.search.length) === 0) {
          this.findAllDosages();
        }
      }
    },
    getColor(estado) {
      if (!estado) return "error";
      else return "success";
    },
    getColorText(text) {
      if (text === "FACTURA") return "indigo--text";
      else return "cyan--text";
    },
    completeNum(str) {
      return String(str).padStart(8, "0");
    },
    fecha(date) {
      const [year, month, day] = date.split("-");
      return `${day}/${month}/${year}`;
    }
  }
};
