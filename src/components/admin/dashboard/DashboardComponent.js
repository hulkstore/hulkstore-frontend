import MovimientoCajaService from '../../../services/movimiento-caja/movimientocaja.service'
import SaleService from '../../../services/sale/sale.service'
import MiniStadistic from '../../components-base/widgets/stadistic/mini-stadistic'
import UserService from '../../../services/user/user.service'
import ClienteService from '../../../services/client/client.service'
import ProductService from '../../../services/product/product.service'
import VWidget from '../../components-base/widgets/VWidget.vue'
import EChart from '../../components-base/widgets/chart/echart'
import Material from 'vuetify/es5/util/colors'
import LineChart from '../../components-base/widgets/linearChart/LinearChart'
import BarChart from '../../components-base/widgets/barChart/BarChart'

export default {
  title: '- Dashboard',
  name: 'dashboard-component',
  components: {
    VWidget,
    EChart,
    MiniStadistic,
    LineChart,
    BarChart
  },
  data() {
    return {

      // BARCHART
      dataMonthBar: {},
      labelMov: [],
      retiros: [],
      ingresos: [],
      dataBar: {},
      color: Material,
      datas: [],
      userId: "",
      cajaId: {},
      userNro: "0",
      clienteNro: "0",
      productosNro: "0",
      salesNro: "0",
      cash: "0",
      inventoryNro: "0",
      invoiceNro: "0",
      receiptsNro: "0",
      loading: false,
      loadingTwo: true,
      datacollection: {},
      etiquetas :[],
      datos: [],
      selectYear: "",
      yearsSale: [],
      menu1: false,
      filMonth: "",
      monthLabel: "",
      menu: false,
      years: [],
      movMonths: [],
      year: "",
      inventoryProducts: [],
      minInventoryProducts: {
          nombre: 'Otros',
          stockTotal: 0
      },
      sortBy: [''],
      sortDesc: [true, false],

      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          xAxes: [{
            stacked: false,
            ticks: {
              beginAtZero: false
            },
            gridLines: {
              display: true
            }
          }],
          yAxes: [{
            stacked: false,
            ticks: {
              beginAtZero: false
            },
            gridLines: {
              display: true
            }
          }]
        }
      }

    }
  },
  computed: {
    locationData() {
      const products = this.inventoryProducts.map((m) => {
        return {
          Producto: m.nombre,
          value: m.stockTotal
        }
      })
      return products;
    },
    arrayData() {
      this.datas = [
        {
          icon: 'mdi-dropbox',
          color: 'indigo',
          title: this.productosNro,
          subTitle: 'Productos Registrados'
        },
        {
          icon: 'mdi-cart',
          color: 'red',
          title: this.salesNro,
          subTitle: 'Ventas'
        },
        {
          icon: 'mdi-account-cash',
          color: 'light-blue',
          title: this.clienteNro,
          subTitle: 'Clientes'
        },
        {
          icon: 'mdi-account',
          color: 'primary',
          title: this.userNro,
          subTitle: 'Usuarios'
        },
        {
          icon: 'mdi-cash',
          color: 'grey',
          title: `${this.cash} $`,
          subTitle: 'Total Cajas'
        },
        {
          icon: 'mdi-transfer',
          color: 'green',
          title: this.inventoryNro,
          subTitle: 'Inventario'
        },
      ]
      return this.datas;
    }
  },
  mounted() {
    // this.countUser();
    // this.countClient();
    // this.countProduct();
    // this.countInventory();
    // this.countCash();
    // this.countInvoices();
    // this.countReceipts();
    // this.countSales();
    // this.findAllProducts();
    // this.stadisticSaleDate();
    // this.stadisticSaleDay();
    // this.stadisticSaleMonth();
    // this.findAmountMovimientoCaja();
    // this.stackYears();
  },
  methods: {
    async countUser() {
      try {
        const res = await UserService.countAll();
        this.userNro = res.toString();
      } catch (error) {
        console.log(error);
      }
    },
    async countClient() {
      try {
        const res = await ClienteService.countAll();
        this.clienteNro = res.toString();
      } catch (error) {
        console.log(error);
      }
    },
    async countProduct() {
      try {
        const res = await ProductService.countAll();
        this.productosNro = res.toString();
      } catch (error) {
        console.log(error);
      }
    },
    async countInventory() {
      try {
        const res = await ProductService.countProductsInventory();
        this.inventoryNro = res.toString();
      } catch (error) {
        console.log(error);
      }
    },
    async countSales() {
      try {
        const res = await SaleService.countSales();
        this.salesNro = res.toString();
      } catch (error) {
        console.log(error);
      }
    },
    async countInvoices() {
      try {
        const res = await SaleService.countInvoices();
        this.invoiceNro = res.toString();
      } catch (error) {
        console.log(error);
      }
    },
    async countReceipts() {
      try {
        const res = await SaleService.countReceipts();
        this.receiptsNro = res.toString();
      } catch (error) {
        console.log(error);
      }
    },
    async countCash() {
      try {
        const res = await MovimientoCajaService.countCash();
        this.cash = res.toString();
      } catch (error) {
        console.log(error);
      }
    },
    async findAllProducts() {
      try {
        this.inventoryProducts = []
        this.loadingTwo = true
        const response = await ProductService.getAllProductsInventory()
        response.map(product => product.stockTotal < 4? this.minInventoryProducts.stockTotal = this.minInventoryProducts.stockTotal + product.stockTotal : this.inventoryProducts.push(product));
        this.inventoryProducts.push(this.minInventoryProducts);
        this.loadingTwo = false
      }catch (error) {
        console.log(error);
      }
    },
  }
}
