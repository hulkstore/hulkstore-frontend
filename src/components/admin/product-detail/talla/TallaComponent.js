import Tallaservice from "../../../../services/product-detail/talla/talla.service";
import DeleteModal from "../../../components-base/modals/delete-modal";
import TallaModal from "../../../components-base/modals/product-modal/talla-modal";
import TallaDetailModal from "../../../components-base/modals/details-product-modal/talla-detail-modal";

export default {
  title: '- Tallas',
  name: "talla-component",
  components: { DeleteModal, TallaModal, TallaDetailModal },
  data: () => ({
    search: "",
    elementsPerPage: 10,
    pagination: {},
    loading: true,
    page: 1,
    size: 10,
    dialog: false,
    dialogDelete: false,
    dialogDetail: false,
    defaultItem: {},
    deleteFunc: null,
    mensaje: "talla...",
    dialogRCP: false,
    flagExist: false,
    complex: {
      selected: [],
      headers: [
        {
          text: "#",
          value: "#",
          align: "left",
          sortable: false
        },
        {
          text: "Talla",
          value: "talla",
          sortable: true
        },
        {
          text: "Acciones",
          value: "action",
          sortable: false
        }
      ],
      items: []
    },
    actions: [
      {
        text: "Detalle",
        icon: "mdi-eye"
      },
      {
        text: "Editar",
        icon: "mdi-pencil"
      },
      {
        text: "Eliminar",
        icon: "mdi-close"
      }
    ],
    sortBy: [""],
    sortDesc: [true, false]
  }),
  watch: {
    page() {
      this.initialize();
    },
    elementsPerPage() {
      this.initialize();
    },
    sortDesc: {
      handler() {
        this.initialize();
      }
    }
  },
  mounted() {
    this.initialize();
  },
  methods: {
    initialize() {
      if (parseInt(this.search.length) >= 3) {
        this.searchByKeyword(this.search);
      } else {
        if (parseInt(this.search.length) === 0) {
          this.findAllTallas();
        }
      }
    },
    async findAllTallas() {
      this.loading = true;
      try {
        const response = await Tallaservice.findAll(
          this.page - 1,
          this.elementsPerPage,
          this.sortBy,
          this.sortDesc
        );
        console.log('------------------------')
        console.log('------------------------')
        console.log(response.data)
        console.log('------------------------')
        console.log('------------------------')
        this.complex.items = response.data;
        this.pagination.totalElements = parseInt(response.elements);
        this.loading = false;
      } catch (error) {
        console.log(error);
      }
    },
    openModal(item, event) {
      this.defaultItem = Object.assign({}, item);
      const text = event.target.id
      if (this.actions[2].text === text) {
        this.dialogDelete = true;
        this.deleteFunc = this.deleteItem;
      } else if (this.actions[1].text === text) {
        this.dialog = true;
      } else {
        if (this.actions[0].text === text) {
          this.dialogDetail = true;
        }
      }
    },
    openModalCreate() {
      this.dialog = true;
      this.defaultItem = Object.assign({});
    },
    async deleteItem() {
      try {
        const response = await Tallaservice.delete(this.defaultItem.id);
        this.$notify({
          group: "foo",
          text: "El talla se elimino exitosamente! " + response,
          type: "success"
        });
      } catch (error) {
        this.$notify({
          group: "foo",
          text: "El talla no se pudo eliminar!",
          type: "error"
        });
        this.dialogDelete = false;
      } finally {
        this.dialogDelete = false;
        const response = this.complex.items.length === 1 ? this.page - 1 : this.page
        this.page = this.page > 1 ? response : 1;
        this.initialize()
      }
    },
    async searchByKeyword(event) {
      this.search = event;
      if (parseInt(this.search.length) >= 3) {
        this.loading = true;
        try {
          const response = await Tallaservice.search(
            this.search,
            this.page - 1,
            this.elementsPerPage,
            this.sortBy,
            this.sortDesc
          );
          this.complex.items = response.data;
          this.pagination.totalElements = parseInt(response.elements);
          this.loading = false;
        } catch (error) {
          console.log(error);
        }
      } else {
        if (parseInt(this.search.length) === 0) {
          this.findAllTallas();
        }
      }
    }
  }
};
