import EtiquetaService from "../../../../services/product-detail/etiqueta/etiqueta.service";
import DeleteModal from "../../../components-base/modals/delete-modal";
import EtiquetaModal from "../../../components-base/modals/product-modal/etiqueta-modal";
import EtiquetaDetailModal from "../../../components-base/modals/details-product-modal/etiqueta-detail-modal";

export default {
  title: '- Etiquetas Producto',
  name: "etiqueta-component",
  components: { DeleteModal, EtiquetaModal, EtiquetaDetailModal },
  data: () => ({
    search: "",
    elementsPerPage: 10,
    pagination: {},
    loading: true,
    page: 1,
    size: 10,
    dialog: false,
    dialogDelete: false,
    dialogDetail: false,
    defaultItem: {},
    deleteFunc: null,
    mensaje: "etiqueta...",
    dialogRCP: false,
    flagExist: false,
    complex: {
      selected: [],
      headers: [
        {
          text: "#",
          value: "#",
          align: "left",
          sortable: false
        },
        {
          text: "Nombre",
          value: "nombre",
          sortable: true
        },
        {
          text: "Acciones",
          value: "action",
          sortable: false
        }
      ],
      items: []
    },
    actions: [
      {
        text: "Detalle",
        icon: "mdi-eye"
      },
      {
        text: "Editar",
        icon: "mdi-pencil"
      },
      {
        text: "Eliminar",
        icon: "mdi-close"
      }
    ],
    sortBy: [""],
    sortDesc: [true, false]
  }),
  watch: {
    page() {
      this.initialize();
    },
    elementsPerPage() {
      this.initialize();
    },
    sortDesc: {
      handler() {
        this.initialize();
      }
    }
  },
  mounted() {
    this.initialize();
  },
  methods: {
    initialize() {
      if (parseInt(this.search.length) >= 3) {
        this.searchByKeyword(this.search);
      } else {
        if (parseInt(this.search.length) === 0) {
          this.findAllEtiquetas();
        }
      }
    },
    async findAllEtiquetas() {
      this.loading = true;
      try {
        const response = await EtiquetaService.findAll(
          this.page - 1,
          this.elementsPerPage,
          this.sortBy,
          this.sortDesc
        );
        this.complex.items = response.data;
        this.pagination.totalElements = parseInt(response.elements);
        this.loading = false;
      } catch (error) {
        console.log(error);
      }
    },
    openModal(item, event) {
      this.defaultItem = Object.assign({}, item);
      const text = event.target.id
      if (this.actions[2].text === text) {
        this.dialogDelete = true;
        this.deleteFunc = this.deleteItem;
      } else if (this.actions[1].text === text) {
        this.dialog = true;
      } else {
        if (this.actions[0].text === text) {
          this.dialogDetail = true;
        }
      }
    },
    openModalCreate() {
      this.dialog = true;
      this.defaultItem = Object.assign({});
    },
    async deleteItem() {
      try {
        const response = await EtiquetaService.delete(this.defaultItem.id);
        this.$notify({
          group: "foo",
          text: "El etiqueta se elimino exitosamente! " + response,
          type: "success"
        });
      } catch (error) {
        this.$notify({
          group: "foo",
          text: "El etiqeuta no se pudo eliminar!",
          type: "error"
        });
        this.dialogDelete = false;
      } finally {
        this.dialogDelete = false;
        const response = this.complex.items.length === 1 ? this.page - 1 : this.page
        this.page = this.page > 1 ? response : 1;
        this.initialize()
      }
    },
    async searchByKeyword(event) {
      this.search = event;
      if (parseInt(this.search.length) >= 3) {
        this.loading = true;
        try {
          const response = await EtiquetaService.search(
            this.search,
            this.page - 1,
            this.elementsPerPage,
            this.sortBy,
            this.sortDesc
          );
          this.complex.items = response.data;
          this.pagination.totalElements = parseInt(response.elements);
          this.loading = false;
        } catch (error) {
          console.log(error);
        }
      } else {
        if (parseInt(this.search.length) === 0) {
          this.findAllEtiquetas();
        }
      }
    }
  }
};
