import DetalleEntradaModal from "../../../components-base/modals/detalle-entrada-modal";
import EntradaModal from "../../../components-base/modals/entrada-modal";
import DetalleEntradaServices from "../../../../services/detalleEntrada/detalleEntrada.service";
import EntradaService from "../../../../services/entrada/entrada.service";
import DateService from "../../../../services/Date.service";
import { mapState } from "vuex";
export default {
  title: "- Movimientos de Entradas",
  components: {
    DetalleEntradaModal,
    EntradaModal
  },
  data: () => ({
    search: "",
    startDate: "",
    endDate: "",
    elementsPerPage: 10,
    pagination: {},
    loading: true,
    page: 1,
    size: 5,
    dialogEntrada: false,
    dialogDetalleEntrada: false,
    defaultItem: {},
    menu: false,
    menu2: false,
    base: process.env.VUE_APP_IMAGE_BASE_URL,
    anularDialog: false,
    anularItem: {},
    entradas: {
      selected: [],
      headers: [
        {
          text: "#",
          value: "#",
          align: "center",
          sortable: false
        },
        {
          text: "Fecha",
          value: "fecha",
          align: "center",
          sortable: true
        },
        {
          text: "Hora",
          value: "hora",
          align: "center",
          sortable: false
        },
        {
          text: "Proveedor",
          value: "invProveedor.razonSocial",
          align: "center",
          sortable: true
        },
        {
          text: "Tipo de Entrada",
          value: "tipoEntrada",
          align: "center",
          sortable: true
        },
        {
          text: "Monto Total",
          value: "montototal",
          align: "center",
          sortable: true
        },
        {
          text: "Monto Cancelado",
          value: "montoCancelado",
          align: "center",
          sortable: true
        },
        {
          text: "Movimiento Entrada",
          value: "tipoMovEntrada",
          align: "center",
          sortable: true
        },
        {
          text: "Estado",
          value: "estado",
          align: "center",
          sortable: true
        },
        {
          text: "Acciones",
          value: "action",
          align: "center",
          sortable: false
        },
        {
          text: "",
          value: "data-table-expand"
        }
      ],
      items: []
    },
    expanded: [],
    detalle_entrada: {
      headers: [
        {
          text: "#",
          value: "#",
          align: "center"
        },
        {
          text: "Producto",
          value: "producto.nombre",
          align: "center"
        },
        {
          text: "Código",
          value: "producto.codigo",
          align: "center"
        },
        {
          text: "Tipo de Producto",
          value: "tipo",
          align: "center"
        },
        {
          text: "Marca",
          value: "marca",
          align: "center"
        },
        {
          text: "Color",
          value: "color",
          align: "center"
        },
        {
          text: "Unidad de Medida",
          value: "um",
          align: "center"
        },
        {
          text: "Cantidad",
          value: "cantidad",
          align: "center"
        },
        {
          text: "Precio Unitario",
          value: "precioUnitarioCompra",
          align: "center"
        },
        {
          text: "Sub Total",
          value: "subTotal",
          align: "center"
        },
        {
          text: "Acciones",
          value: "action",
          align: "center"
        }
      ],
      items: []
    },
    sortBy: ["fecha"],
    sortDesc: [false, false],
    rules: {
      anulacionRules: [v => !!v || "El Campo es requerido"]
    },
    loadingAnulacion: false
  }),
  computed: {
    ...mapState("sucursalModule", ["sucursal"])
  },
  watch: {
    page() {
      this.initialize();
    },
    elementsPerPage() {
      this.initialize();
    },
    sortDesc: {
      handler() {
        this.initialize();
      }
    },
    search(val) {
      if (val.length > 2) this.searchByKeyword(val);
      else if (val.length === 0) this.findAllEntradas();
    },
    expanded() {
      //console.log(this.expanded);
      if (this.expanded.length > 0) {
        this.findAllDetalleEntrada(this.expanded[0].id);
      }
    },
    sucursal() {
      this.initialize();
    }
  },
  mounted() {
    this.initialize();
  },
  methods: {
    initialize() {
      if (
        this.search === null ||
        this.startDate === null ||
        this.endDate === null
      ) {
        this.search = "";
        this.startDate = "";
        this.endDate = "";
      }
      if (
        parseInt(this.search.length) > 0 &&
        parseInt(this.startDate.length) == 0 &&
        parseInt(this.endDate.length) == 0
      ) {
        this.searchByKeyword();
      } else {
        if (
          parseInt(this.startDate.length) > 0 &&
          parseInt(this.endDate.length) > 0
        ) {
          this.searchByKeyword();
        } else {
          if (
            parseInt(this.startDate.length) == 0 &&
            parseInt(this.endDate.length) == 0 &&
            parseInt(this.search.length) == 0
          ) {
            this.findAllEntradas();
          }
        }
      }
    },
    fechaTo(fechm) {
      return DateService.getDate(fechm);
    },
    horaTo(fechm) {
      return DateService.getTime(fechm);
    },
    async findAllEntradas() {
      this.loading = true;
      try {
        const res = await EntradaService.findAllBySucursalId(
          this.page - 1,
          this.elementsPerPage,
          this.sortBy,
          this.sortDesc,
          this.sucursal.id || 1
        );
        this.entradas.items = res.data;
        this.pagination.totalElements = parseInt(res.elements);
        this.loading = false;
      } catch (error) {
        console.log(error);
      }
    },
    openAnularDialog(item) {
      this.$refs.anulacionForm?.reset();
      this.anularDialog = true;
      this.anularItem = Object.assign({}, item);
    },
    closeAnularDialog() {
      this.anularDialog = false;
      this.anularItem = Object.assign({}, {});
      this.$refs.anulacionForm.reset();
    },
    anularForm() {
      if (this.$refs.anulacionForm.validate()) {
        this.anularEntrada();
      }
    },
    async anularEntrada() {
      this.loadingAnulacion = true;
      try {
        this.anularItem.estado = false;
        await EntradaService.update(this.anularItem);
        this.initialize();
        this.$notify({
          group: "foo",
          text: "La entrada se anulo exitosamente...",
          type: "success"
        });
        this.loadingAnulacion = false;
        this.anularDialog = false;
      } catch (error) {
        this.loadingAnulacion = false;
        console.log(error);
      } finally {
        this.anularItem = Object.assign({}, {});
      }
    },
    async findAllDetalleEntrada(id) {
      try {
        const res = await DetalleEntradaServices.findAllByEntradaId(id);
        this.detalle_entrada.items = res;
        //console.log(this.detalle_entrada.items);
      } catch (error) {
        console.log(error);
      }
    },
    searchByKeywordOption() {
      if (
        this.search === null ||
        this.startDate === null ||
        this.endDate === null
      ) {
        this.search = "";
        this.startDate = "";
        this.endDate = "";
      }
      if (
        parseInt(this.search.length) > 0 &&
        parseInt(this.startDate.length) == 0 &&
        parseInt(this.endDate.length) == 0
      ) {
        this.searchByKeyword();
      } else {
        if (
          parseInt(this.startDate.length) > 0 &&
          parseInt(this.endDate.length) > 0
        ) {
          this.searchByKeyword();
        }
      }
    },
    async searchByKeyword() {
      this.loading = true;
      try {
        const response = await EntradaService.searchBySucursal(
          this.search,
          this.startDate,
          this.endDate,
          this.page - 1,
          this.elementsPerPage,
          [""],
          [true, false],
          this.sucursal.id || localStorage.getItem("currentSuc")
        );
        this.entradas.items = response.data;
        this.pagination.totalElements = parseInt(response.elements);
        this.loading = false;
      } catch (error) {
        console.log(error);
      }
    },
    openModal(item, index) {
      if (index === 1) this.dialogEntrada = true;
      else this.dialogDetalleEntrada = true;
      this.defaultItem = Object.assign({}, item);
    }
  }
};
