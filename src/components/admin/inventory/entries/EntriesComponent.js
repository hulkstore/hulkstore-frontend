import EntriesModal from '../../../components-base/modals/entries-modal'
import DetalleEntradaServices from '../../../../services/detalleEntrada/detalleEntrada.service'
import DateService from '../../../../services/Date.service'
export default {
  components: {
    EntriesModal
  },
  data: () => ({
    search: '',
    startDate: '',
    endDate: '',
    elementsPerPage: 10,
    pagination: {},
    loading: true,
    page: 1,
    size: 5,
    dialog: false,
    defaultItem: {},
    menu: false,
    menu2: false,
    base: process.env.VUE_APP_IMAGE_BASE_URL,
    complex: {
      selected: [],
      headers: [
        {
          text: '#',
          value: '#',
          align: 'left',
          sortable: false
        },
        {
          text: 'Fecha',
          value: 'createdDate',
          sortable: true
        },
        {
          text: 'Hora',
          value: 'hora',
          sortable: false
        },
        {
          text: 'Nombre',
          value: 'producto.nombre',
          sortable: true
        },
        {
          text: 'Codigo',
          value: 'productoId',
          sortable: true
        },
        {
          text: 'stock/Cantidad',
          value: 'cantidad',
          sortable: true
        },
        {
          text: 'Costo Unitario',
          value: 'costoUnitario',
          sortable: true
        },
        {
          text: 'Operacion',
          value: 'action',
          sortable: false
        }
      ],
      items: []
    },
    sortBy: [''],
    sortDesc: [true, false]
  }),
  watch: {
    page () {
      this.initialize()
    },
    elementsPerPage () {
      this.initialize()
    },
    sortDesc: {
      handler () {
        this.initialize()
      }
    },
  },
  mounted () {
    this.initialize()
  },
  methods: {
    initialize () {
      if(this.search === null || this.startDate === null || this.endDate === null){
        this.search = ''
        this.startDate = ''
        this.endDate = ''
      }
      if(parseInt(this.search.length) > 0 && (parseInt(this.startDate.length) == 0 && parseInt(this.endDate.length) == 0)){
        console.log("................Producto........................................"+ this.search)
        this.searchByKeyword();
      }else{
        if(parseInt(this.startDate.length) > 0 && parseInt(this.endDate.length) > 0){
          console.log("......................startDate........................." + this.startDate);
          console.log("......................endDate..........................." + this.endDate);
          this.searchByKeyword();
        }else{
          if(parseInt(this.startDate.length) == 0 && parseInt(this.endDate.length) == 0 && parseInt(this.search.length) == 0){
            this.findAllDetalleEntrada()
          }
        }
      }
    },
    fechaTo(fechm){
      return DateService.getDate(fechm)
    },
    horaTo(fechm){
      return DateService.getTime(fechm)
    },
    async findAllDetalleEntrada () {
      this.loading = true
      try {
        const response = await DetalleEntradaServices.findAll((this.page - 1), this.elementsPerPage, this.sortBy, this.sortDesc)
        this.complex.items = response.data
        this.pagination.totalElements = parseInt(response.elements)
        this.loading = false
      }catch (error) {
        console.log(error)
      }
    },
    searchByKeywordOption() {
      console.log('desde ===================>');
      if(this.search === null || this.startDate === null || this.endDate === null){
        this.search = ''
        this.startDate = ''
        this.endDate = ''
      }
        if(parseInt(this.search.length) > 0 && (parseInt(this.startDate.length) == 0 && parseInt(this.endDate.length) == 0)){
          console.log("................Producto..................................."+ this.search)
          this.searchByKeyword();
        }else{
          if(parseInt(this.startDate.length) > 0 && parseInt(this.endDate.length) > 0){
            console.log("......................startDate........................." + this.startDate);
            console.log("......................endDate..........................." + this.endDate);
            this.searchByKeyword();
          }
        }
    },
    async searchByKeyword () {
        this.loading = true
        try {
          console.log('.................buscando');
          const response = await DetalleEntradaServices.search(this.search, this.startDate, this.endDate, (this.page - 1), this.elementsPerPage, this.sortBy, this.sortDesc)
          this.complex.items = response.data
          console.log(this.complex.items);
          this.pagination.totalElements = parseInt(response.elements)
          this.loading = false
        } catch (error) {
          console.log(error)
        }
    },
    openModal (item) {
      this.dialog = true
      this.defaultItem = Object.assign({}, item)
    },
  }
}