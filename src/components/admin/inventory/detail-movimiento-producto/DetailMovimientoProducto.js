import ProductService from '../../../../services/product/product.service'
import ImageService from '../../../../services/image/image.service'
import DetalleEntradaService from '../../../../services/detalleEntrada/detalleEntrada.service'
import DetalleVentaService from '../../../../services/detalleVenta/detalleVenta.service'
import DetalleVentaModel from '../../../components-base/models/DetalleVentaModel'
import DetailMovModal from '../../../components-base/modals/detailMovProd/detailMovEntrada'
import DetailMovVeModal from '../../../components-base/modals/detailMovProd/detailMovVenta'
import DateService from '../../../../services/Date.service'
export default{
  components: { DetailMovModal, DetailMovVeModal },
  mixins:[ DetalleVentaModel ],
  data() {
    return {
      search: '',
      startDate: '',
      endDate: '',
      menu: false,
      menu2: false,
      dialogE: false,
      elementsPerPage: 10,
      pagination: {},
      loading: false,
      page: 1,
      defaultItem: {},
      defaultItemE: {},
      base: process.env.VUE_APP_IMAGE_BASE_URL,
      images: [],
      basicEntrada: {
        headers: [
          {
            text: 'Fecha',
            align: 'left',
            value: 'fecha',
            sortable: false
          },
          {
            text: 'Hora',
            value: 'hora',
            sortable: false
          },
          {
            text: 'Cantidad Mov.',
            value: 'cantidadX',
          },
          {
            text: 'Costo',
            align: 'left',
            value: 'costoUnitarioX'
          },
          {
            text: 'Actions',
            align: 'center',
            value: 'actions',
            sortable: false
          },
        ],
        items: []
      },
      sortBy: [''],
      sortDesc: [true, false],
      searchV: '',
      startDateV: '',
      endDateV: '',
      menuV: false,
      menu2V: false,
      dialogV: false,
      elementsPerPageV: 10,
      paginationV: {},
      loadingV: false,
      pageV: 1,
      defaultItemV: {},
      basicVenta: {
        headers: [
          {
            text: 'Fecha',
            align: 'left',
            value: 'fecha',
            sortable: false
          },
          {
            text: 'Hora',
            value: 'hora',
            sortable: false
          },
          {
            text: 'Cantidad Mov.',
            value: 'cantidadX',
          },
          {
            text: 'Costo',
            value: 'costoUnitarioX'
          },
          {
            text: 'Precio',
            value: 'precioUnitarioX'
          },
          {
            text: 'Actions',
            value: 'actions',
            sortable: false
          },
        ],
        items: []
      },
      sortByV: [''],
      sortDescV: [true, false],
    }
  },
  watch: {
    page () {
      this.initializeE()
    },
    elementsPerPage () {
      this.initializeE()
    },
    sortDesc: {
      handler () {
        this.initializeE()
      }
    },
    pageV () {
      this.initializeV()
    },
    elementsPerPageV () {
      this.initializeV()
    },
    sortDescV: {
      handler () {
        this.initializeV()
      }
    },
  },
  mounted() {
    if (this.$route.params.id) {
      this.getProductById()
      this.findAllImages()
      this.getDEProductById()
      this.getEVProductoById()
    }
  },
  methods: {
    initializeE(){
      if(parseInt(this.startDate.length) === 0 || parseInt(this.endDate.length) === 0){
        console.log('.......................1....................');
        this.getDEProductById()
      }else if(parseInt(this.startDate.length) > 0 && parseInt(this.endDate.length) > 0){
        console.log('.......................2....................');
        this.searchDEByID()
      }
    },
    initializeV(){
      if(parseInt(this.startDateV.length) === 0 || parseInt(this.endDateV.length) === 0){
        console.log('.......................1....................');
        this.getEVProductoById()
      }else if(parseInt(this.startDate.length) > 0 && parseInt(this.endDate.length) > 0){
        console.log('.......................2....................');
        this.searchDVByID()
      }
    },
    searchByDateIdE(){
      if(parseInt(this.startDate.length) === 0 || parseInt(this.endDate.length) === 0){
        this.startDate = ''
        this.endDate = ''
      }else if(parseInt(this.startDate.length) > 0 && parseInt(this.endDate.length) > 0){
        this.searchDEByID()
      }
    },
    searchByDateIdV(){
      if(parseInt(this.startDateV.length) === 0 || parseInt(this.endDateV.length) === 0){
        this.startDateV = ''
        this.endDateV = ''
      }else if(parseInt(this.startDateV.length) > 0 && parseInt(this.endDateV.length) > 0){
        this.searchDVByID()
      }
    },
    async searchDEByID(){
      this.loading = true
      try {
        var resp = await DetalleEntradaService.searchProdId(this.$route.params.id, this.startDate, this.endDate, (this.page - 1), this.elementsPerPage, this.sortBy, this.sortDesc)
        this.basicEntrada.items = resp.data
        this.pagination.totalElements = parseInt(resp.elements)
        console.log(resp)
        this.loading = false
      } catch (error) {
        console.log(error)
      }
    },
    async searchDVByID(){
      this.loadingV = true
      try {
        var resp = await DetalleVentaService.searchProdId(this.$route.params.id ,this.startDateV, this.endDateV, (this.pageV - 1), this.elementsPerPageV, this.sortByV, this.sortDescV)
        this.basicVenta.items = resp.data
        this.paginationV.totalElements = parseInt(resp.elements)
        console.log(resp)
        this.loadingV = false
      } catch (error) {
        console.log(error)
      }
    },
    fechaTo(fechm){
      return DateService.getDate(fechm)
    },
    horaTo(fechm){
      return DateService.getTime(fechm);
    },
    async getProductById () {
      try {
        const resp = await ProductService.findOne(this.$route.params.id)
        this.defaultItem = resp
        console.log(this.defaultItem);
      } catch (error) {
        console.log(error);
      }
    },
    async findAllImages () {
      try {
        const response = await ImageService.findAll(this.$route.params.id)
        this.images = response.data
      } catch (error) {
        console.log(error)
      }
    },
    async getDEProductById () {
      this.loading = true
      try {
        var resp = await DetalleEntradaService.findAllProdById(this.$route.params.id, (this.page - 1), this.elementsPerPage, this.sortBy, this.sortDesc)
        this.basicEntrada.items = resp.content
        this.pagination.totalElements = parseInt(resp.totalElements)
        this.loading = false
      } catch(error) {
        console.log(error)
      }
    },
    async getEVProductoById () {
      this.loadingV = true
      try {
        var resp = await DetalleVentaService.findAllProdById(this.$route.params.id, (this.pageV - 1), this.elementsPerPageV, this.sortByV, this.sortDescV)
        this.basicVenta.items = resp.content
        this.paginationV.totalElements = parseInt(resp.totalElements)
        this.loadingV = false
      } catch (error) {
        console.log(error);
      }
    },
    async reiniciarStock(){
      try {
        this.defaultItemDV.cantidad = this.defaultItem.stockTotal
        this.defaultItemDV.costoUnitario = null
        this.defaultItemDV.precioUnitario = null
        this.defaultItemDV.productoId = this.defaultItem.id
        await DetalleVentaService.create(this.defaultItemDV)
        this.getEVProductoById()
        this.getProductById()
        this.$notify({
          group: 'foo',
          text: "Se reinicio el Stock exitosamente!",
          type: 'success'
        });
      } catch (error) {
        this.$notify({
          group: 'foo',
          text: "Error al reiniciar el Stock",
          type: 'error'
        });
      }
    },
    OpenModalE(item){
      this.dialogE = true
      this.defaultItemE = Object.assign({}, item)
    },
    OpenModalV(item){
      this.dialogV = true
      this.defaultItemV = Object.assign({}, item)
      console.log(this.defaultItemV);
    }
  },
}
