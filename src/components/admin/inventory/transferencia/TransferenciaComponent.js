import DateService from "../../../../services/Date.service";
import ProveedorService from "../../../../services/proveedor/proveedor.service";
import ProductService from "../../../../services/product/product.service";
import EntradaService from "../../../../services/entrada/entrada.service";
import SalidaService from "../../../../services/salidas/salidas.service";
import DetalleEntradaService from "../../../../services/detalleEntrada/detalleEntrada.service";
import ProductDetailModal from "../../../components-base/modals/details-product-modal/product-detail-modal";
import { mapState } from "vuex";
import colorService from "../../../../services/product-detail/color/color.service";
import marcaService from "../../../../services/product-detail/marca/marca.service";
import unidadMedidaService from "../../../../services/product-detail/unidad-medida/unidad-medida.service";
export default {
  title: "- Trasnsferencia entre sucursales",
  components: { ProductDetailModal },
  data: () => ({
    detalle_entrada: {
      headers: [
        {
          text: "#",
          value: "#",
          align: "left"
        },
        {
          text: "Fecha de Entrada del Lote",
          value: "createdDate",
          align: "center"
        },
        {
          text: "Nombre",
          value: "producto.nombre",
          align: "center"
        },
        {
          text: "Codigo",
          value: "producto.codigo",
          align: "center"
        },
        {
          text: "Unidad de Medida",
          value: "producto.unidadMedida.unidad",
          align: "center"
        },
        {
          text: "Tipo Producto",
          value: "producto.categoriaProducto",
          align: "center",
          sortable: false
        },
        {
          text: "Marca",
          value: "producto.marca.nombre",
          align: "center",
          sortable: false
        },
        {
          text: "Color",
          value: "producto.color.color",
          align: "center",
          sortable: false
        },
        {
          text: "Stock Lote",
          value: "cantidad",
          align: "center"
        },
        {
          text: "Estado de Lote",
          value: "totalRestante",
          align: "center"
        },
        {
          text: "Precio de Compra",
          value: "precioUnitarioCompra",
          align: "center"
        },
        {
          text: "Precio de Venta",
          value: "producto.precioUnitarioVenta",
          align: "center"
        },
        {
          text: "Cantidad Salida",
          value: "cantidadSalida",
          align: "center",
          width: "5%"
        },
        {
          text: "Proveedor",
          value: "invEntrada.invProveedor.razonSocial",
          align: "center"
        },
        {
          text: "Sub Total",
          value: "subTotal",
          align: "center"
        },
        {
          text: "Acciones",
          value: "action",
          align: "center"
        }
      ],
      items: []
    },
    productos_entrada: {
      headers: [
        {
          text: "#",
          value: "#",
          align: "left"
        },
        {
          text: "Nombre",
          value: "producto.nombre",
          align: "center"
        },
        {
          text: "Codigo",
          value: "producto.codigo",
          align: "center"
        },
        {
          text: "Unidad de Medida",
          value: "producto.unidadMedida.unidad",
          align: "center"
        },
        {
          text: "Tipo Producto",
          value: "producto.categoriaProducto",
          align: "center",
          sortable: false
        },
        {
          text: "Marca",
          value: "producto.marca.nombre",
          align: "center",
          sortable: false
        },
        {
          text: "Color",
          value: "producto.color.color",
          align: "center",
          sortable: false
        },
        {
          text: "Stock Lote",
          value: "cantidad",
          align: "center"
        },
        {
          text: "Estado de Lote",
          value: "totalRestante",
          align: "center"
        },
        {
          text: "Precio de Compra",
          value: "precioUnitarioCompra",
          align: "center"
        },
        {
          text: "Precio de Venta",
          value: "producto.precioUnitarioVenta",
          align: "center"
        },
        {
          text: "Cantidad Salida",
          value: "cantidadSalida",
          align: "center",
          width: "5%"
        },
        {
          text: "Proveedor",
          value: "invEntrada.invProveedor.razonSocial",
          align: "center"
        },
        {
          text: "Sub Total",
          value: "subTotal",
          align: "center"
        },
        {
          text: "Acciones",
          value: "action",
          align: "center"
        }
      ],
      items: []
    },
    loading: false,
    entradaId: null,
    productDialog: false,
    isEditing: false,
    editId: null,
    productos: [],
    proveedores: [],
    defaultItem: {},
    entradaItem: {},
    invEntradaItem: {},

    productItem: {},
    productFormDialog: false,
    productFormItems: {
      tiposProducto: ["REPUESTO", "TELA", "ACCESORIO"],
      unidadesMedida: [],
      colores: [],
      marcas: [],
      rules: {
        nombreError: [v => !!v || "El nombre del producto es requerido"],
        codigoError: [v => !!v || "El codigo es requerido"],
        categoriaError: [v => !!v || "Debe seleccionar un tipo de producto"],
        unidadMedidaError: [
          v => !!v || "Debe seleccionar una unidad de medida"
        ],
        colorError: [v => !!v || "Debe seleccionar un color"],
        marcaError: [v => !!v || "Debe seleccionar una marca"]
      }
    },

    proveedorItem: {},
    proveedorFormDialog: false,
    proveedorRules: {
      razonSocialError: [v => !!v || "El campo es requerido"],
      nitCiError: [v => !!v || "El NIT o CI es requerido"]
    },

    dialogDetail: false,
    detailProduct: {},

    sortBy: [""],
    sortDesc: [true, false],

    search: null,
    sucursalTwo: {},
    rules: {
      productRules: {
        productError: [v => !!v || "El Campo es requerido"],
        stockSalidaError: [
          v => !!v || "Cantidad requerida",
          v => v > 0 || "Cantidad > 0"
        ]
      },
      salidaTwoRules: {
        sucursalTwoError: [v => !!v || "Debe selecionar una sucursal"],
        itemsError: [
          v => v > 0 || "Debe existir al menos un item para crear la salida"
        ]
      }
    },
    messageAlert: {
      action: false,
      type: "success",
      message: null
    },
    transferenciaDialog: false,
    loadingButton: false
  }),
  mounted() {
    this.findAllProductsBySucursalId();
  },
  watch: {
    "entradaItem.product": function(newVal) {
      if (newVal?.id) {
        this.findAllDetalleEntradaByProductId(newVal.id);
      }
    },
    sucursal() {
      this.sucursalTwo = Object.assign({}, {})
      this.findAllProductsBySucursalId();
      this.productos_entrada.items = [];
      this.$refs.salidaForm?.reset()
    }
  },
  computed: {
    ...mapState("sucursalModule", ["sucursal"]),
    ...mapState("sucursalModule", ["listaSucursales"]),
    currentDate() {
      return DateService.parseDateToString(new Date());
    },
    product() {
      return this.entradaItem?.product || {};
    },
    modalTitle() {
      return this.isEditing ? "Editar" : "Añadir";
    },
    cantidadEntrada() {
      return `${this.entradaItem?.cantidad || 0} ${this.entradaItem?.product
        ?.unidadMedida?.sigla || ""}`;
    },
    getPrecioUnitario() {
      return (
        (this.entradaItem.precioUnitario =
          this.entradaItem?.subTotal / this.entradaItem?.cantidad) || 0
      );
    },
    getUnidadMedida() {
      return this.entradaItem?.product?.unidadMedida || {};
    },
    total() {
      var total = 0.0;
      this.productos_entrada?.items?.forEach(e => {
        total += e.cantidadSalida * e.precioUnitarioCompra;
      });
      return total;
    },
    getMontoCancelado() {
      return this.defaultItem?.montoCancelado || 0;
    },
    getSaldo() {
      let aux =
        parseFloat(this.total) - parseFloat(this.getMontoCancelado) || 0;
      return aux >= 0 ? aux : 0;
    }
  },
  methods: {
    async findAllProveedores() {
      try {
        const res = await ProveedorService.listAll();
        this.proveedores = res;
      } catch (error) {
        console.log(error);
      }
    },
    async findAllProductsBySucursalId() {
      if (this.sucursal?.id) {
        try {
          const res = await ProductService.listAll(this.sucursal.id);
          this.productos = res;
        } catch (error) {
          console.log(error);
        }
      }
    },
    async createEntrada() {
      if (this.$refs.entradaForm.validate()) {
        this.loading = true;
        try {
          this.defaultItem.fecha = new Date().toISOString();
          this.defaultItem.montoCancelado = parseFloat(
            this.defaultItem.montoCancelado
          );
          this.defaultItem.sucursalIdAux = this.sucursal.id;
          this.defaultItem.estado = true;
          //console.log(this.defaultItem);
          const res = await EntradaService.create(this.defaultItem);
          this.entradaId = res.id;
          this.createArrayDetalleEntradas();
          console.log(res);
        } catch (error) {
          console.log(error);
          this.$notify({
            group: "foo",
            text: "No se pudo crear la entrada!",
            type: "error"
          });
        }
      }
    },
    createArrayDetalleEntradas() {
      this.productos_entrada.items.forEach(el => {
        this.createDetalleEntrada(el);
      });
      this.$notify({
        group: "foo",
        text: "La entrada se creó exitosamente!",
        type: "success"
      });
      this.loading = false;
      this.resetForm();
    },
    async createDetalleEntrada(item) {
      try {
        this.invEntradaItem.productoId = item.product.id;
        this.invEntradaItem.cantidad = item.cantidad;
        this.invEntradaItem.subTotal = item.subTotal;
        this.invEntradaItem.precioUnitarioCompra = item.precioUnitario;
        this.invEntradaItem.entradaId = this.entradaId;
        const res = await DetalleEntradaService.create(this.invEntradaItem);
        this.updateProduct(item);
        console.log(res);
      } catch (error) {
        console.log(error);
      }
    },
    async createProduct() {
      this.loading = true;
      try {
        this.productItem.sucursalId = this.sucursal.id;
        const res = await ProductService.create(this.productItem);
        console.log(res);
        this.loading = false;
        this.closeProductFormModal();
        this.findAllProductsBySucursalId();
        this.$notify({
          group: "foo",
          text:
            "El producto se creó exitosamente, Puede editarlo en la lista de productos",
          type: "success"
        });
      } catch (error) {
        console.log(error);
      }
    },
    async updateProduct(item) {
      try {
        this.productItem = item.product;
        this.productItem.estado = true;
        const res = await ProductService.update(this.productItem);
        console.log(res)
      } catch (error) {
        console.log(error);
      }
    },
    addProduct() {
      if (this.$refs.productosForm.validate()) {
        this.productos_entrada.items.push(this.entradaItem);
        console.log(this.productos_entrada.items);
        this.closeProductDialog();
      }
    },
    editProduct() {
      if (this.$refs.productosForm.validate()) {
        this.productos_entrada.items[this.editId] = this.entradaItem;
        this.closeProductDialog();
      }
    },
    deleteProduct(id) {
      const aux = this.productos_entrada.items.filter(
        item => this.productos_entrada.items.indexOf(item) !== id
      );
      this.productos_entrada.items = aux;
    },
    openEdit(id) {
      this.editId = id;
      this.entradaItem = this.productos_entrada.items[id];
      this.isEditing = true;
      this.productDialog = true;
    },
    openCreate() {
      this.isEditing = false;
      this.productDialog = true;
      this.entradaItem = Object.assign({}, {});
      this.detalle_entrada.items = [];
      this.messageAlert.action = false;
      this.messageAlert.type = "success";
      this.messageAlert.message = null;
    },
    actionProduct() {
      if (this.isEditing) {
        this.editProduct();
      } else {
        this.addProduct();
      }
    },
    closeProductDialog() {
      this.productDialog = false;
    },
    openProductDetailModal(item) {
      this.detailProduct = Object.assign({}, item);
      this.dialogDetail = true;
    },
    //Product Form
    async findAllColors() {
      try {
        const response = await colorService.findAll(
          0,
          100,
          this.sortBy,
          this.sortDesc
        );
        this.productFormItems.colores = response.data;
      } catch (error) {
        console.log(error);
      }
    },
    async findAllMarcas() {
      try {
        const response = await marcaService.findAll(
          0,
          100,
          this.sortBy,
          this.sortDesc
        );
        this.productFormItems.marcas = response.data;
      } catch (error) {
        console.log(error);
      }
    },
    async findAllUnidadesMedidia() {
      try {
        const response = await unidadMedidaService.findAll(
          0,
          100,
          this.sortBy,
          this.sortDesc
        );
        this.productFormItems.unidadesMedida = response.data;
      } catch (error) {
        console.log(error);
      }
    },
    openProductFormModal() {
      this.productFormDialog = true;
      this.findAllColors();
      this.findAllMarcas();
      this.findAllUnidadesMedidia();
      this.messageAlert.action = false;
      this.messageAlert.type = "success";
      this.messageAlert.message = null;
    },
    closeProductFormModal() {
      this.$refs.productForm.reset();
      this.productFormDialog = false;
      this.productItem = Object.assign({}, {});
      this.messageAlert.action = false;
      this.messageAlert.type = "success";
      this.messageAlert.message = null;
    },
    checkProductForm() {
      if (this.$refs.productForm.validate()) {
        this.createProduct();
      }
    },
    //Proveedor Form
    openProveedorFormModal() {
      this.proveedorFormDialog = true;
    },
    closeProveedorFormModal() {
      this.proveedorFormDialog = false;
      this.$refs.proveedorForm.reset();
      this.proveedorItem = Object.assign({}, {});
    },
    checkProveedorForm() {
      this.$refs.proveedorForm.validate() && this.createProveedor();
    },
    async createProveedor() {
      this.loading = true;
      try {
        this.proveedorItem.estado = true;
        const res = await ProveedorService.create(this.proveedorItem);
        this.defaultItem.invProveedorId = res.id;
        this.loading = false;
        this.findAllProveedores();
        this.closeProveedorFormModal();
        this.$notify({
          group: "foo",
          text:
            "El proveedor se creó exitosamente, Puede editarlo en la lista de productos",
          type: "success"
        });
      } catch (error) {
        console.log(error);
      }
    },
    // ------------------------ GET INVDETALLEENTRADA --------------------------
    async findAllDetalleEntradaByProductId(productId) {
      try {
        const response = await DetalleEntradaService.findAllLoteDetalleEntradasByProductId(
          productId
        );
        this.detalle_entrada.items = response;
      } catch (error) {
        console.log(error);
      }
      this.detalle_entrada.items.map(modal => {
        if (
          this.productos_entrada.items.filter(e => e.id === modal.id).length > 0
        ) {
          modal.cantidadSalida = parseInt(
            this.productos_entrada.items.filter(e => e.id === modal.id)[0]
              .cantidadSalida
          );
          return modal;
        } else {
          return modal;
        }
      });
    },
    addDetalleSalidaProducto(detalleEntradaProducto) {
      const data1 =
        parseInt(detalleEntradaProducto.cantidadSalida) === 0
          ? parseInt(detalleEntradaProducto.cantidadSalida)
          : detalleEntradaProducto.cantidadSalida === undefined
          ? 0
          : detalleEntradaProducto.cantidadSalida;
      const data = data1 !== 0 ? true : false;
      if (data) {
        if (
          this.productos_entrada.items.filter(
            e => e.id === detalleEntradaProducto.id
          ).length > 0
        ) {
          this.productos_entrada.items.map(e => {
            if (e.id == detalleEntradaProducto.id) {
              e.cantidadSalida = detalleEntradaProducto.cantidadSalida;
              return e;
            } else {
              return e;
            }
          });
        } else {
          this.productos_entrada.items.push(detalleEntradaProducto);
        }
        this.messageAlert.action = true;
        this.messageAlert.type = "success";
        this.messageAlert.message = "EL PRODUCTO SE ADICIONO CON EXITO" + ' || LOTE: '+ detalleEntradaProducto.id +' || PRODUCTO: ' + detalleEntradaProducto.producto.nombre + ' || CANTIDAD SALIDA: ' + detalleEntradaProducto.cantidadSalida + ' ' + detalleEntradaProducto.producto.unidadMedida.sigla;
      } else {
        this.messageAlert.action = true;
        this.messageAlert.type = "error";
        this.messageAlert.message =
          "PARA PODER ADICIONAR PRODUCTOS, TIENE QUE TENER UNA CANTIDAD DE SALIDA > 0";
      }
    },
    setCurrentSucursal(item) {
      this.sucursalTwo = Object.assign({}, item);
    },
    async saveTransaccion() {
      const itemTransferencia = {};
      itemTransferencia.subTotal = 0.0;
      itemTransferencia.descuento = 0.0;
      itemTransferencia.precioTotal = 0.0;
      itemTransferencia.preInvDetalleEntradaVMs = this.productos_entrada.items;
      itemTransferencia.montoRecibido = 0.0;
      itemTransferencia.cambio = 0.0;
      itemTransferencia.sucursalIdAux = this.sucursal.id;
      itemTransferencia.sucursalIdAuxTwo = this.sucursalTwo.id;
      let formData = new FormData();
      formData.append(
        "preTransferenciaVM",
        new Blob([JSON.stringify(itemTransferencia)], {
          type: "application/json"
        })
      );
      this.loadingButton = true;
      try {
        const response = await SalidaService.createInvSalidaInvEntradaTransferencia(
          formData
        );
        console.log(response);
        this.$notify({
          group: "foo",
          text: "La transaccion entre sucursales se creó exitosamente!",
          type: "success"
        });
        this.resetForm();
        this.transferenciaDialog = false;
        this.loadingButton = false;
      } catch (error) {
        console.log(error);
        this.$notify({
          group: "foo",
          text: "No se pudo relizar la transaccion entre sucursales!",
          type: "error"
        });
        this.loadingButton = false;
      }
    },
    resetForm() {
      this.productos_entrada.items = [];
      this.detalle_entrada.items = [];
      this.entradaItem = Object.assign({}, {});
      this.sucursalTwo = Object.assign({}, {});
      this.$refs.salidaForm.reset();
      // MESSAGE
      this.messageAlert.action = false;
      this.messageAlert.type = "success";
      this.messageAlert.message = null;
    },
    openModalTransaccion() {
      if (this.$refs.salidaForm.validate()) {
        this.transferenciaDialog = true;
      }
    },
    getItemText(item) {
      return `${item.nombre} ${item.color.descripcion} ${item.color.codigo} ${item.categoriaProducto} ${item.unidadMedida.unidad}`;
    },
    fechaTo(fechm) {
      return DateService.getDate(fechm);
    }
  }
};
