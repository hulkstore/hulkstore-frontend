import ProductService from "../../../../services/product/product.service";
//import DetalleEntradaService from "../../../../services/detalleEntrada/detalleEntrada.service";
import DateService from "../../../../services/Date.service";
import { mapState } from "vuex";

export default {
  title: " - Administrar Inventario",
  components: {},
  data() {
    return {
      search: "",

      elementsPerPage: 10,
      pagination: {},
      loading: false,
      page: 1,
      size: 5,

      base: process.env.VUE_APP_IMAGE_BASE_URL,

      defaultItem: {},
      detalles_entrada: {
        headers: [
          {
            text: "#",
            value: "#",
            align: "center"
          },
          {
            text: "Fecha",
            value: "fecha",
            align: "center"
          },
          {
            text: "Proveedor",
            value: "razonSocial",
            align: "center"
          },
          {
            text: "Precio Unitario Compra",
            value: "precioUnitarioCompra",
            align: "center"
          }
        ],
        items: []
      },
      precioRules: [
        v => !!v || "El Precio de Venta es requerido...",
        v => v > 0 || "El Precio de venta debe ser mayor a 0"
      ],
      precioVentaDialog: false,

      complex: {
        headers: [
          {
            text: "Avatar",
            value: "avatar",
            align: "center",
            sortable: false
          },
          {
            text: "Nombre Producto",
            value: "nombre",
            align: "center",
            sortable: true
          },
          {
            text: "Codigo",
            value: "codigo",
            align: "center",
            sortable: true
          },
          {
            text: "Tipo Producto",
            value: "categoriaProducto",
            align: "center",
            sortable: false
          },
          {
            text: "Calidad",
            value: "calidad",
            align: "center",
            sortable: true
          },
          {
            text: "Marca",
            value: "marca.nombre",
            align: "center",
            sortable: false
          },
          {
            text: "Color",
            value: "color",
            align: "center",
            sortable: false
          },
          {
            text: "Stock Total",
            value: "stockTotal",
            align: "center",
            sortable: false
          },
          {
            text: "Precio Unitario Venta",
            value: "precioUnitarioVenta",
            align: "center",
            sortable: false
          },
          {
            text: "Acciones",
            value: "action",
            align: "center",
            sortable: false
          }
        ],
        items: []
      },
      sortBy: [""],
      sortDesc: [true, false],
      retirarProductoDialog: false,
      loading1: false,
      loading2: false,
      adicionarProductoDialog: false,
      productos: [],
      productItem: {
        precioUnitarioVenta: null
      },
      rules: {
        productoRules: {
          productError: [v => !!v || "El Campo es requerido"],
          precioRules: [
            v => !!v || "El Precio de Venta es requerido...",
            v => v > 0 || "El Precio de venta debe ser mayor a 0"
          ],
          itemsError: [
            v =>
              v > 0 ||
              "Debe existir al menos un item para adicionar un producto al inventario"
          ]
        }
      }
    };
  },
  watch: {
    sucursal() {
      this.initialize();
      this.findAllProductsByEstadoAndSucursalId();
    },
    'productItem.producto': function(newVal) {
      if (newVal?.id) {
        this.findAllEntradasByProductId(newVal.id);
        this.productItem.precioUnitarioVenta = newVal.precioUnitarioVenta
      }
    },
    page() {
      this.initialize();
    },
    elementsPerPage() {
      this.initialize();
    },
    sortDesc: {
      handler() {
        this.initialize();
      }
    }
  },
  computed: {
    ...mapState("sucursalModule", ["sucursal"])
  },
  mounted() {
    this.findAllProducts();
    this.findAllProductsByEstadoAndSucursalId();
  },
  methods: {
    initialize() {
      if (parseInt(this.search.length) >= 2)
        this.searchProductoInInventory(this.search);
      else if (parseInt(this.search.length) === 0) this.findAllProducts();
    },
    openModal(item) {
      this.precioVentaDialog = true;
      this.defaultItem = Object.assign({}, item);
      this.findAllEntradasByProductId(this.defaultItem.id);
    },
    updatePrecioVenta() {
      if (this.$refs.precioVentaForm.validate()) {
        this.defaultItem.precioUnitarioVenta = parseFloat(
          this.defaultItem.precioUnitarioVenta
        );
        this.updateProductoInventoryPrecioVenta();
      }
    },
    closeModal() {
      this.$refs.precioVentaForm.reset();
      this.precioVentaDialog = false;
    },
    fechaTo(date) {
      return DateService.getDate(date);
    },
    async findAllEntradasByProductId(id) {
      try {
        const res = await ProductService.findPreciosUV(id);
        this.detalles_entrada.items = res;
      } catch (error) {
        console.log(error);
      }
    },
    async updateProductoInventoryPrecioVenta() {
      try {
        await ProductService.update(this.defaultItem);
        this.$notify({
          group: "foo",
          text: "Se actualizo el precio de Venta del Producto!",
          type: "success"
        });
        this.closeModal();
        this.findAllProducts();
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
      }
    },
    async findAllProducts() {
      this.loading = true;
      try {
        const response = await ProductService.findAllProductoEnInventario(
          this.page - 1,
          this.elementsPerPage,
          this.sortBy,
          this.sortDesc,
          this.sucursal.id || 1
        );
        this.complex.items = response.data;
        this.pagination.totalElements = parseInt(response.elements);
        this.loading = false;
      } catch (error) {
        console.log(error);
      }
    },
    async searchProductoInInventory(event) {
      this.search = event;
      this.loading = true;
      if (parseInt(this.search.length) >= 2) {
        try {
          const resp = await ProductService.searchInInventory(
            this.search,
            this.page - 1,
            this.elementsPerPage,
            this.sortBy,
            this.sortDesc,
            this.sucursal.id || 1
          );
          this.complex.items = resp.data;
          this.pagination.totalElements = parseInt(resp.elements);
          this.loading = false;
        } catch (error) {
          console.log(error);
        }
      } else {
        this.findAllProducts();
      }
    },
    openModalRetirarProducto(item) {
      this.retirarProductoDialog = true;
      this.defaultItem = Object.assign({}, item);
    },
    closeModalRerirarProducto() {
      this.retirarProductoDialog = false;
      this.defaultItem = Object.assign({}, {});
    },
    async updateRetiroProducto() {
      this.defaultItem.estado = false;
      this.loading1 = true;
      try {
        await ProductService.update(this.defaultItem);
        this.$notify({
          group: "foo",
          text: "El producto se retiro con exito!",
          type: "success"
        });
        this.loading1 = false;
        this.closeModalRerirarProducto();
        this.findAllProducts();
        this.findAllProductsByEstadoAndSucursalId();
      } catch (error) {
        this.$notify({
          group: "foo",
          text: error.response.data.title,
          type: "error"
        });
        this.loading1 = false;
      }
    },
    openModalAdicionarProducto() {
      this.$refs.productoForm?.reset();
      this.adicionarProductoDialog = true;
      this.productItem = Object.assign({}, {});
      this.detalles_entrada.items = [];
    },
    closeModalAdicionarProducto() {
      this.$refs.productoForm?.reset();
      this.adicionarProductoDialog = false;
      this.productItem = Object.assign({}, {});
      this.detalles_entrada.items = [];
    },
    async findAllProductsByEstadoAndSucursalId() {
      if (this.sucursal?.id) {
        try {
          const response = await ProductService.getAllProductsInventoryByEstado(
            this.sucursal?.id
          );
          this.productos = response;
        } catch (error) {
          console.log(error);
        }
      }
    },
    async addProductToInventory() {
      if (this.$refs.productoForm.validate()) {
        this.loading2 = true;
        this.productItem.producto.estado = true;
        this.productItem.producto.precioUnitarioVenta = this.productItem.precioUnitarioVenta
        try {
          await ProductService.update(this.productItem?.producto);
          this.$notify({
            group: "foo",
            text: "El producto se adiciono con exito!",
            type: "success"
          });
          this.loading2 = false;
          this.closeModalRerirarProducto();
          this.findAllProducts();
          this.findAllProductsByEstadoAndSucursalId();
          this.closeModalAdicionarProducto();
        } catch (error) {
          this.$notify({
            group: "foo",
            text: error.response.data.title,
            type: "error"
          });
          this.loading2 = false;
        }
      }
    }
  }
};
