import DetalleSalidaModal from "../../../components-base/modals/detalle-salida-modal";
import SalidaModal from "../../../components-base/modals/salida-modal";
// import DetalleEntradaServices from "../../../../services/detalleEntrada/detalleEntrada.service";
import DetalleSalidaServices from "../../../../services/detalleSalida/detalleSalida.service";
//import EntradaService from "../../../../services/entrada/entrada.service";
import SalidaService from "../../../../services/salidas/salidas.service";
import DateService from "../../../../services/Date.service";
import { mapState } from "vuex";

export default {
  title: "- Movimientos de Salidas",
  name: "detalle-salida-component",
  components: {
    DetalleSalidaModal,
    SalidaModal
  },
  data: () => ({
    search: "",
    startDate: "",
    endDate: "",
    elementsPerPage: 10,
    pagination: {},
    loading: true,
    page: 1,
    size: 5,
    dialogEntrada: false,
    dialogDetalleEntrada: false,
    defaultItem: {},
    menu: false,
    menu2: false,
    base: process.env.VUE_APP_IMAGE_BASE_URL,
    anularDialog: false,
    entradas: {
      selected: [],
      headers: [
        {
          text: "#",
          value: "#",
          align: "center",
          sortable: false
        },
        {
          text: "Fecha",
          value: "fechaVenta",
          align: "center",
          sortable: true
        },
        {
          text: "Hora",
          value: "hora",
          align: "center",
          sortable: false
        },
        // {
        //   text: "Proveedor",
        //   value: "invProveedor.razonSocial",
        //   align: "center",
        //   sortable: true
        // },
        {
          text: "Estado",
          value: "estado",
          align: "center",
          sortable: true
        },
        {
          text: "Tipo de Pago",
          value: "tipoPago",
          align: "center",
          sortable: true
        },
        {
          text: "Tipo de Doc.",
          value: "tipoVenta",
          align: "center",
          sortable: true
        },
        {
          text: "Tipo Salida",
          value: "tipoMovSalida",
          align: "center",
          sortable: true
        },
        {
          text: "Nro. Recibo",
          value: "numeroFacturaRecibo",
          align: "center",
          sortable: true
        },
        {
          text: "Monto Total",
          value: "montoTotal",
          align: "center",
          sortable: true
        },
        {
          text: "Acciones",
          value: "action",
          align: "center",
          sortable: false
        },
        {
          text: "",
          value: "data-table-expand"
        }
      ],
      items: []
    },
    expanded: [],
    detalle_entrada: {
      headers: [
        {
          text: "#",
          value: "#",
          align: "center"
        },
        {
          text: "Producto",
          value: "producto.nombre",
          align: "center"
        },
        {
          text: "Código",
          value: "producto.codigo",
          align: "center"
        },
        {
          text: "Tipo de Producto",
          value: "producto.categoriaProducto",
          align: "center"
        },
        {
          text: "Unidad de Medida",
          value: "producto.unidadMedida.unidad",
          align: "center"
        },
        {
          text: "Color",
          value: "producto.color.color",
          align: "center"
        },
        {
          text: "Marca",
          value: "producto.marca.nombre",
          align: "center"
        },
        {
          text: "Cantidad",
          value: "cantidad",
          align: "center"
        },
        {
          text: "Precio Unitario Compra",
          value: "precioUnitarioCompra",
          align: "center"
        },
        {
          text: "Precio Unitario Venta",
          value: "precioUnitarioVenta",
          align: "center"
        },
        {
          text: "Sub Total",
          value: "subTotal",
          align: "center"
        },
        {
          text: "Acciones",
          value: "action",
          align: "center"
        }
      ],
      items: []
    },
    sortBy: ["fechaVenta"],
    sortDesc: [false, false],
    anularItem: {},
    rules: {
      anulacionRules: [v => !!v || "El Campo es requerido"]
    },
    loadingAnulacion: false
  }),
  computed: {
    ...mapState("sucursalModule", ["sucursal"])
  },
  watch: {
    expanded() {
      //console.log(this.expanded);
      if (this.expanded.length > 0) {
        this.findAllDetalleSalidaBySalidaId(this.expanded[0].id);
      }
    },
    search(val) {
      if (val.length > 2) this.searchByKeyword(val);
      else if (val.length === 0) this.findAllEntradas();
    },
    page() {
      this.initialize();
    },
    elementsPerPage() {
      this.initialize();
    },
    sortDesc: {
      handler() {
        this.initialize();
      }
    },
    sucursal() {
      this.initialize();
    }
  },
  mounted() {
    this.initialize();
  },
  methods: {
    initialize() {
      if (
        this.search === null ||
        this.startDate === null ||
        this.endDate === null
      ) {
        this.search = "";
        this.startDate = "";
        this.endDate = "";
      }
      if (
        parseInt(this.search.length) > 0 &&
        parseInt(this.startDate.length) == 0 &&
        parseInt(this.endDate.length) == 0
      ) {
        this.searchByKeyword();
      } else {
        if (
          parseInt(this.startDate.length) > 0 &&
          parseInt(this.endDate.length) > 0
        ) {
          this.searchByKeyword();
        } else {
          if (
            parseInt(this.startDate.length) == 0 &&
            parseInt(this.endDate.length) == 0 &&
            parseInt(this.search.length) == 0
          ) {
            this.findAllEntradas();
          }
        }
      }
    },
    fechaTo(fechm) {
      return DateService.getDate(fechm);
    },
    horaTo(fechm) {
      return DateService.getTime(fechm);
    },
    async findAllEntradas() {
      this.loading = true;
      try {
        const res = await SalidaService.findAllBySucursalId(
          this.page - 1,
          this.elementsPerPage,
          this.sortBy,
          this.sortDesc,
          this.sucursal.id || 1
        );
        this.entradas.items = res.data;
        this.pagination.totalElements = parseInt(res.elements);
        this.loading = false;
      } catch (error) {
        console.log(error);
      }
    },
    async findAllDetalleSalidaBySalidaId(id) {
      try {
        const res = await DetalleSalidaServices.findAllBySalidaId(id);
        this.detalle_entrada.items = res;
        //console.log(this.detalle_entrada.items);
      } catch (error) {
        console.log(error);
      }
    },
    searchByKeywordOption() {
      if (
        this.search === null ||
        this.startDate === null ||
        this.endDate === null
      ) {
        this.search = "";
        this.startDate = "";
        this.endDate = "";
      }
      if (
        parseInt(this.search.length) > 0 &&
        parseInt(this.startDate.length) == 0 &&
        parseInt(this.endDate.length) == 0
      ) {
        this.searchByKeyword();
      } else {
        if (
          parseInt(this.startDate.length) > 0 &&
          parseInt(this.endDate.length) > 0
        ) {
          this.searchByKeyword();
        }
      }
    },
    async searchByKeyword() {
      this.loading = true;
      try {
        const response = await SalidaService.searchBySucursal(
          this.search,
          this.startDate,
          this.endDate,
          this.page - 1,
          this.elementsPerPage,
          [""],
          [true, false],
          this.sucursal.id || localStorage.getItem("currentSuc")
        );
        this.entradas.items = response.data;
        //console.log(this.complex.items);
        this.pagination.totalElements = parseInt(response.elements);
        this.loading = false;
      } catch (error) {
        console.log(error);
      }
    },
    openModal(item, index) {
      if (index === 1) this.dialogEntrada = true;
      else this.dialogDetalleEntrada = true;
      this.defaultItem = Object.assign({}, item);
    },
    openAnularDialog(item) {
      this.$refs.anulacionForm?.reset();
      this.anularDialog = true;
      this.anularItem = Object.assign({}, item);
    },
    closeAnularDialog() {
      this.anularDialog = false;
      this.anularItem = Object.assign({}, {});
      this.$refs.anulacionForm.reset();
    },
    anularForm() {
      if (this.$refs.anulacionForm.validate()) {
        this.anularEntrada();
      }
    },
    async anularEntrada() {
      this.loadingAnulacion = true;
      try {
        this.anularItem.estado = "ANULADO";
        await SalidaService.update(this.anularItem);
        this.initialize();
        this.$notify({
          group: "foo",
          text: "La salida se anulo exitosamente...",
          type: "success"
        });
        this.anularDialog = false;
        this.loadingAnulacion = false;
      } catch (error) {
        this.loadingAnulacion = false;
        console.log(error);
      } finally {
        this.anularItem = Object.assign({}, {});
      }
    }
  }
};
