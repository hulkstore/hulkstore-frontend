import SalidaService from '../../../../services/salidas/salidas.service'
import InvDetalleSalida from '../../../../services/detalleSalida/detalleSalida.service'
import DateService from '../../../../services/Date.service'
import ProductService from "../../../../services/product/product.service";
import ProductDetailModal from "../../../components-base/modals/details-product-modal/product-detail-modal";
import DetalleEntradaService from "../../../../services/detalleEntrada/detalleEntrada.service";
import { mapState } from "vuex";

export default {
  title:"- Salidas de Inventario",
  name: "salidas",
  components: { ProductDetailModal },
  data: () => ({
    loading: false,
    elementsPerPage: 10,
    pagination: {},
    page: 1,
    size: 5,
    search: null,
    detalle_entrada: {
      headers: [
        {
          text: "#",
          value: "#",
          align: "left"
        },
        {
          text: "Fecha de Entrada del Lote",
          value: "createdDate",
          align: "center"
        },
        {
          text: "Nombre",
          value: "producto.nombre",
          align: "center"
        },
        {
          text: "Codigo",
          value: "producto.codigo",
          align: "center"
        },
        {
          text: "Unidad de Medida",
          value: "producto.unidadMedida.unidad",
          align: "center"
        },
        {
          text: "Tipo Producto",
          value: "producto.categoriaProducto",
          align: "center",
          sortable: false
        },
        {
          text: "Marca",
          value: "producto.marca.nombre",
          align: "center",
          sortable: false
        },
        {
          text: "Color",
          value: "producto.color.color",
          align: "center",
          sortable: false
        },
        {
          text: "Stock Lote",
          value: "cantidad",
          align: "center"
        },
        {
          text: "Estado de Lote",
          value: "totalRestante",
          align: "center"
        },
        {
          text: "Precio de Compra",
          value: "precioUnitarioCompra",
          align: "center"
        },
        {
          text: "Precio de Venta",
          value: "producto.precioUnitarioVenta",
          align: "center"
        },
        {
          text: "Cantidad Salida",
          value: "cantidadSalida",
          align: "center",
          width: "5%"
        },
        {
          text: "Proveedor",
          value: "invEntrada.invProveedor.razonSocial",
          align: "center"
        },
        {
          text: "Sub Total",
          value: "subTotal",
          align: "center"
        },
        {
          text: "Acciones",
          value: "action",
          align: "center"
        }
      ],
      items: []
    },
    productos_salida: {
      headers: [
        {
          text: "#",
          value: "#",
          align: "left"
        },
        {
          text: "Nombre",
          value: "producto.nombre",
          align: "center"
        },
        {
          text: "Codigo",
          value: "producto.codigo",
          align: "center"
        },
        {
          text: "Unidad de Medida",
          value: "producto.unidadMedida.unidad",
          align: "center"
        },
        {
          text: "Tipo Producto",
          value: "producto.categoriaProducto",
          align: "center",
          sortable: false
        },
        {
          text: "Marca",
          value: "producto.marca.nombre",
          align: "center",
          sortable: false
        },
        {
          text: "Color",
          value: "producto.color.color",
          align: "center",
          sortable: false
        },
        {
          text: "Stock Lote",
          value: "cantidad",
          align: "center"
        },
        {
          text: "Estado de Lote",
          value: "totalRestante",
          align: "center"
        },
        {
          text: "Precio de Compra",
          value: "precioUnitarioCompra",
          align: "center"
        },
        {
          text: "Precio de Venta",
          value: "producto.precioUnitarioVenta",
          align: "center"
        },
        {
          text: "Cantidad Salida",
          value: "cantidadSalida",
          align: "center",
          width: "5%"
        },
        {
          text: "Proveedor",
          value: "invEntrada.invProveedor.razonSocial",
          align: "center"
        },
        {
          text: "Sub Total",
          value: "subTotal",
          align: "center"
        },
        {
          text: "Acciones",
          value: "action",
          align: "center"
        }
      ],
      items: []
    },
    rules: {
      productRules: {
        productError: [v => !!v || "El Campo es requerido"],
        stockSalidaError: [
          v => !!v || "La cantidad es requerida",
          v => v > 0 || "La cantidad debe ser mayor a 0"
        ]
      },
      salidaRules: {
        itemsError: [
          v => v > 0 || "Debe existir al menos un item para crear la salida"
        ],
        observacion: [
          v => !!v || "El Campo es requerido"
        ],
        tipoSalida: [
          v => !!v || "El Campo es requerido"
        ]
      }
    },
    sortBy: [''],
    sortDesc: [true, false],
    productDialog: false,
    productos: [],
    invSalidaItem: {},
    invDetalleSalidas: [],
    invDetalleEntradas: [],
    searchProduct: null,
    salidaItem: {},
    dialogDetail: false,
    detailProduct: {},
    isEditing: false,
    productList: [],
    messageAlert: {
      action: false,
      type: "success",
      message: null
    },
    tipoMovimientoSalida: [ 'RETIRO_SIMPLE', 'DEVUELTO', 'CADUCADO' ],
    itemInvDetalleSalida: {},
  }),
  watch: {
    "salidaItem.product": function(newVal) {
      if (newVal !== undefined) {
        this.findAllDetalleEntradaByProductId(newVal.id);
      }
    },
    sucursal() {
      this.findAllProductsBySucursalId();
      this.productos_salida.items = [];
    }
  },
  computed: {
    ...mapState("sucursalModule", ["sucursal"]),
    currentDate() {
      return DateService.parseDateToString(new Date());
    },
    product() {
      return this.salidaItem?.product || {};
    },
    modalTitle() {
      return this.isEditing ? "Editar" : "Añadir";
    },
    getUnidadMedida() {
      return this.salidaItem?.product?.unidadMedida || {};
    },
    total() {
      let total = 0.00;
      this.productos_salida?.items?.forEach(e => {
        total += e.producto.precioUnitarioVenta * e.cantidadSalida
      });
      return parseFloat(total).toFixed(2) || 0.00;
    }
  },
  mounted () {
    this.findAllProductsBySucursalId();
  },
  methods: {
    async findAllProductsBySucursalId() {
      if (this.sucursal?.id) {
        try {
          const res = await ProductService.listAll(this.sucursal.id);
          this.productos = res;
        } catch (error) {
          console.log(error);
        }
      }
    },
    openModal() {
      this.isEditing = false;
      this.productDialog = true;
      this.salidaItem = Object.assign({}, {});
      //this.detalle_entrada.items = []
      //this.$refs.productosForm?.reset();
    },
    closeProductDialog() {
      this.productDialog = false;
      this.detalle_entrada.items = []
      //this.salidaItem = Object.assign({}, {});
      //this.$refs.productosForm.reset();
    },
    async findAllInvDetalleSalida (productId) {
      try {
        const response = await InvDetalleSalida.searchProdId(productId, (this.page - 1), this.elementsPerPage, this.sortBy, this.sortDesc)
        console.log(response.data)
      } catch (error) {
        console.log(error)
      }
    },
    async createSalida() {
      if (this.$refs.salidaForm.validate()) {
        console.log('----------------')
        console.log(this.itemInvDetalleSalida)
        console.log('----------------')
        const itemInvSalida = {};
        itemInvSalida.subTotal = this.total;
        itemInvSalida.descuento = 0.00;
        itemInvSalida.precioTotal = this.total;
        itemInvSalida.preInvDetalleEntradaVMs = this.productos_salida.items 
        itemInvSalida.montoRecibido = 0.00;
        itemInvSalida.cambio = 0.00;
        itemInvSalida.observacionSalida = this.itemInvDetalleSalida.observacionSalida
        itemInvSalida.tipoMovSalida = this.itemInvDetalleSalida.tipoMovSalida
        itemInvSalida.sucursalIdAux = this.sucursal.id;
        let formData = new FormData();
        formData.append(
          "preSalidaVM",
          new Blob([JSON.stringify(itemInvSalida)], {
            type: "application/json"
          })
        );
        console.log('--------------------------')
        console.log('--------------------------')
        console.log(itemInvSalida)
        console.log('--------------------------')
        console.log('--------------------------')
        try {
          const response = await SalidaService.createInvSalida(formData);
          console.log(response)
          this.$notify({
            group: "foo",
            text: "La salida se creó exitosamente!",
            type: "success"
          });
          this.resetForm()
          this.loading = false
        } catch (error) {
          console.log(error);
          this.$notify({
            group: "foo",
            text: "No se pudo crear la entrada!",
            type: "error"
          });
        }
      }
    },
    createArrayDetalleSalidas() {
      const itemTwo = []
      this.productos_salida.items.forEach(el => {
        const data = {}
        data.id = null
        data.cantidad = parseInt(el.product.stockSalida)
        data.montoTotal = el.stockSalida * el.product.precioUnitarioVenta
        data.precioUnitarioCompra = 0.00
        data.precioUnitarioVenta = el.product.precioUnitarioVenta
        data.invSalidaId = null
        data.invSalida = null
        data.productoId = el.product.id 
        data.producto = el.product
        itemTwo.push(data)
      });
      return itemTwo
    },
    resetForm() {
      this.productos_salida.items = [];
      this.salidaItem = Object.assign({}, {});
      this.$refs.salidaForm.reset();
    },
    deleteProduct(id) {
      const aux = this.productos_salida.items.filter(
        item => this.productos_salida.items.indexOf(item) !== id
      );
      this.productos_salida.items = aux;
    },
    openEdit(id) {
      this.editId = id;
      this.salidaItem = this.productos_salida.items[id];
      this.isEditing = true;
      this.productDialog = true;
    },
    addProduct() {
      if (this.$refs.productosForm.validate()) {
        this.productos_salida.items.push(this.salidaItem);
        this.closeProductDialog();
      }
    },
    editProduct() {
      if (this.$refs.productosForm.validate()) {
        this.productos_salida.items[this.editId] = this.salidaItem;
        this.closeProductDialog();
      }
    },
    openProductDetailModal(item) {
      this.detailProduct = Object.assign({}, item);
      this.dialogDetail = true;
    },
    actionProduct() {
      if (this.isEditing) {
        this.editProduct();
      } else {
        this.addProduct();
      }
    },
    // ------------------- SEARCH PRODUCT BY LOTE ---------------------
    async findAllDetalleEntradaByProductId(productId) {
      try {
        const response = await DetalleEntradaService.findAllLoteDetalleEntradasByProductId(productId);
        this.detalle_entrada.items = response;
      } catch (error) {
        console.log(error);
      }
      this.detalle_entrada.items.map(modal => {
        if (
          this.productos_salida.items.filter(e => e.id === modal.id).length > 0
        ) {
          modal.cantidadSalida = parseInt(
            this.productos_salida.items.filter(e => e.id === modal.id)[0].cantidadSalida
          );
          return modal;
        } else {
          return modal;
        }
      });
    },
    addDetalleSalidaProducto(detalleEntradaProducto) {
      const data1 = parseInt(detalleEntradaProducto.cantidadSalida) === 0 ? parseInt(detalleEntradaProducto.cantidadSalida) : detalleEntradaProducto.cantidadSalida === undefined ? 0: detalleEntradaProducto.cantidadSalida === null? 0 : detalleEntradaProducto.cantidadSalida;
      const data = data1 !== 0 ? true : false;
      if (data) {
        if (
          this.productos_salida.items.filter(
            e => e.id === detalleEntradaProducto.id
          ).length > 0
        ) {
          this.productos_salida.items.map(e => {
            if (e.id == detalleEntradaProducto.id) {
              e.cantidadSalida = detalleEntradaProducto.cantidadSalida;
              return e;
            } else {
              return e;
            }
          });
        } else {
          this.productos_salida.items.push(detalleEntradaProducto);
        }
        this.messageAlert.action = true;
        this.messageAlert.type = "success";
        this.messageAlert.message = "EL PRODUCTO SE ADICIONO CON EXITO" + ' || LOTE: '+ detalleEntradaProducto.id +' || PRODUCTO: ' + detalleEntradaProducto.producto.nombre + ' || CANTIDAD SALIDA: ' + detalleEntradaProducto.cantidadSalida + ' ' + detalleEntradaProducto.producto.unidadMedida.sigla;
      } else {
        this.messageAlert.action = true;
        this.messageAlert.type = "error";
        this.messageAlert.message =
          "PARA PODER ADICIONAR PRODUCTOS, TIENE QUE TENER UNA CANTIDAD DE SALIDA > 0";
      }
    },
    getItemText(item) {
      return `${item.nombre} ${item.color.descripcion}`;
    },
    fechaTo(fechm) {
      return DateService.getDate(fechm);
    }
  }
}
