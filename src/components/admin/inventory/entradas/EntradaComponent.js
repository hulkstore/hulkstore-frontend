import DateService from "../../../../services/Date.service";
import ProductService from "../../../../services/product/product.service";
import EntradaService from "../../../../services/entrada/entrada.service";
import ProductDetailModal from "../../../components-base/modals/details-product-modal/product-detail-modal";
export default {
  title: "- Entradas de Inventario",
  components: { ProductDetailModal },
  data: () => ({
    productos_entrada: {
      headers: [
        {
          text: "#",
          value: "#",
          align: "left",
        },
        {
          text: "Nombre",
          value: "producto",
          align: "center",
        },
        {
          text: "Codigo",
          value: "codigo",
          align: "center",
        },
        {
          text: "Tipo Producto",
          value: "tipo",
          align: "center",
          sortable: false,
        },
        {
          text: "Etiqueta",
          value: "marca",
          align: "center",
          sortable: false,
        },
        {
          text: "Cantidad",
          value: "cantidad",
          align: "center",
        },
        {
          text: "Costo por unidad",
          value: "costoUnitario",
          align: "center",
        },
        {
          text: "Sub Total",
          value: "subTotal",
          align: "center",
        },
        {
          text: "Acciones",
          value: "action",
          align: "center",
        },
      ],
      items: [],
    },
    rules: {
      productRules: {
        productError: [(v) => !!v || "El Campo es requerido"],
        cantidadError: [
          (v) => !!v || "La cantidad es requerida",
          (v) => v > 0 || "La cantidad debe ser mayor a 0",
        ],
        precioTotalError: [
          (v) => !!v || "El precio es requerido",
          (v) => v > 0 || "El precio debe ser mayor a 0",
        ],
      },
      entradaRules: {
        itemsError: [
          (v) => v > 0 || "Debe existir al menos un item para crear la entrada",
        ],
      },
    },
    loading: false,
    entradaId: null,
    productDialog: false,
    isEditing: false,
    editId: null,
    productos: [],
    defaultItem: {},
    entradaItem: {},
    invEntradaItem: {},

    productItem: {},
    productFormDialog: false,
    productFormItems: {
      tiposProducto: ["REPUESTO", "TELA", "ACCESORIO"],
      unidadesMedida: [],
      colores: [],
      marcas: [],
      rules: {
        nombreError: [(v) => !!v || "El nombre del producto es requerido"],
        codigoError: [(v) => !!v || "El codigo es requerido"],
        categoriaError: [(v) => !!v || "Debe seleccionar un tipo de producto"],
        unidadMedidaError: [
          (v) => !!v || "Debe seleccionar una unidad de medida",
        ],
        colorError: [(v) => !!v || "Debe seleccionar un color"],
        marcaError: [(v) => !!v || "Debe seleccionar una marca"],
      },
    },

    dialogDetail: false,
    detailProduct: {},

    sortBy: [""],
    sortDesc: [true, false],
    addedProducts: [],
    invEntrada: {},
  }),
  mounted() {
    this.findAllProductsBySucursalId();
  },
  watch: {
    sucursal() {
      this.findAllProductsBySucursalId();
      this.productos_entrada.items = [];
    },
  },
  computed: {
    currentDate() {
      return DateService.parseDateToString(new Date());
    },
    product() {
      return this.entradaItem?.product || {};
    },
    modalTitle() {
      return this.isEditing ? "Editar" : "Añadir";
    },
    cantidadEntrada() {
      return `${this.entradaItem?.cantidad || 0}`;
    },
    subTotal() {
      return this.entradaItem?.subTotal || 0;
    },
    getPrecioUnitario() {
      return (
        (this.entradaItem.costoUnitario =
          this.entradaItem?.subTotal / this.entradaItem?.cantidad) || 0
      );
    },
    getUnidadMedida() {
      return this.entradaItem?.product?.unidadMedida || {};
    },
    total() {
      var total = 0;
      this.productos_entrada?.items?.forEach((e) => {
        total += parseFloat(e.subTotal);
        //console.log(total);
      });
      return (this.defaultItem.montototal = total || 0);
    },
    getMontoCancelado() {
      return this.defaultItem?.montoCancelado || 0;
    },
    getSaldo() {
      let aux =
        parseFloat(this.total) - parseFloat(this.getMontoCancelado) || 0;
      return aux >= 0 ? aux : 0;
    },
    tipoEntrada() {
      return (
        (this.defaultItem.tipoEntrada =
          this.getSaldo > 0 ? "DEUDA" : "CANCELADO") || "---"
      );
    },
  },
  methods: {
    async findAllProductsBySucursalId() {
      try {
        const res = await ProductService.findAllVM(
          0,
          10,
          this.sortBy,
          this.sortDesc
        );
        this.productos = res.data;
      } catch (error) {
        console.log(error);
      }
    },
    async updateProduct(item) {
      try {
        this.productItem = item.product;
        /*this.productItem.cantidad += parseFloat(item.cantidad);
        this.productItem.precioUnitarioCompra = item.precioUnitario;*/
        //console.log(this.productItem);
        this.productItem.estado = true;
        const res = await ProductService.update(this.productItem);
        console.log(res);
      } catch (error) {
        console.log(error);
      }
    },
    addProduct() {
      if (this.$refs.productosForm.validate()) {
        this.productos_entrada.items.push(this.entradaItem);
        console.log(this.productos_entrada.items);
        this.closeProductDialog();
      }
    },
    editProduct() {
      if (this.$refs.productosForm.validate()) {
        this.productos_entrada.items[this.editId] = this.entradaItem;
        this.closeProductDialog();
      }
    },
    deleteProduct(id) {
      const aux = this.productos_entrada.items.filter(
        (item) => this.productos_entrada.items.indexOf(item) !== id
      );
      this.productos_entrada.items = aux;
    },
    openEdit(id) {
      this.editId = id;
      this.entradaItem = this.productos_entrada.items[id];
      this.isEditing = true;
      this.productDialog = true;
    },
    openCreate() {
      this.isEditing = false;
      this.productDialog = true;
      this.entradaItem = Object.assign({}, {});
      this.$refs.productosForm?.reset();
    },
    actionProduct() {
      if (this.isEditing) {
        this.editProduct();
      } else {
        this.addProduct();
      }
    },
    closeProductDialog() {
      this.entradaItem = Object.assign({}, {});
      this.productDialog = false;
      this.$refs.productosForm.reset();
    },
    resetForm() {
      this.productos_entrada.items = [];
      this.defaultItem = Object.assign({}, {});
      this.$refs.entradaForm.reset();
    },
    openProductDetailModal(item) {
      this.detailProduct = Object.assign({}, item);
      this.dialogDetail = true;
    },

    //Product Form

    closeProductFormModal() {
      this.$refs.productForm.reset();
      this.productFormDialog = false;
      this.productItem = Object.assign({}, {});
    },

    async createEntrada() {
      this.productos_entrada.items.forEach((el) => {
        const data = {
          costoUnitario: el.costoUnitario,
          cantidad: el.cantidad,
          productoId: el.product.id,
          subTotal: el.subTotal,
        };
        this.addedProducts.push(data);
      });
      if (this.$refs.entradaForm.validate()) {
        this.invEntrada.invDetalleEntradaVMs = this.addedProducts;
        let formData = new FormData();
        formData.append(
          "entradaVM",
          new Blob([JSON.stringify(this.invEntrada)], {
            type: "application/json",
          })
        );
        this.loading = true;
        try {
          const res = await EntradaService.createVM(this.invEntrada);
          console.log(res);
          this.$notify({
            group: "foo",
            text: "La entrada se creó exitosamente!",
            type: "success",
          });
          this.resetForm();
        } catch (error) {
          this.$notify({
            group: "foo",
            text: "No se pudo crear la entrada!",
            type: "error",
          });
        }
        this.loading = false;
      }
    },
  },
};
