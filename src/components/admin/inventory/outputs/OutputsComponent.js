import OutputsModal from '../../../components-base/modals/outputs-modal'
import DetalleVentaServices from '../../../../services/detalleVenta/detalleVenta.service'
import DateService from  "../../../../services/Date.service"
export default {
  components: {
    OutputsModal
  },
  data: () => ({
    dialog: false,
    defaultItem: {},
    elementsPerPage: 10,
    pagination: {},
    loading: true,
    page: 1,
    size: 5,
    search: '',
    startDate: '',
    endDate: '',
    menu: false,
    menu2: false,
    base: process.env.VUE_APP_IMAGE_BASE_URL,
    complex: {
      headers: [
        {
          text: '#',
          value: '#',
          align: 'left',
          sortable: false
        },
        {
          text: 'Fecha',
          value: 'createdDate',
          sortable: true
        },
        {
          text: 'Hora',
          value: 'hora',
          sortable: false
        },
        {
          text: 'Nombre',
          value: 'producto.nombre',
          sortable: true
        },
        {
          text: 'Codigo',
          value: 'productoId',
          sortable: true
        },
        {
          text: 'Cantidad',
          value: 'cantidad',
          sortable: true
        },
        {
          text: 'Precio Unitario',
          value: 'precioUnitario',
          sortable: true
        },
        {
          text: 'Costo Unitario',
          value: 'costoUnitario',
          sortable: true
        },
        {
          text: 'Operacion',
          value: 'action',
          sortable: false
        }
      ],
      items: []
    },
    sortBy: [''],
    sortDesc: [true, false]
  }),
  watch: {
    page () {
      this.initialize()
    },
    elementsPerPage () {
      this.initialize()
    },
    sortDesc: {
      handler () {
        this.initialize()
      }
    },
  },
  mounted () {
    this.initialize()

  },
  methods: {
    initialize () {
      if(this.search === null || this.startDate === null || this.endDate === null){
        this.search = ''
        this.startDate = ''
        this.endDate = ''
      }
      if(parseInt(this.search.length) > 0 && (parseInt(this.startDate.length) == 0 && parseInt(this.endDate.length) == 0)){
          this.searchByKeyword();
        }else{
          if(parseInt(this.startDate.length) > 0 && parseInt(this.endDate.length) > 0){
            this.searchByKeyword();
          }else{
            if(parseInt(this.startDate.length) == 0 && parseInt(this.endDate.length) == 0 && parseInt(this.search.length) == 0){
              this.findAllDetalleVenta();
            }
          }
        }
      },
    fechaTo(fechm){
      return DateService.getDate(fechm)
    },
    horaTo(fechm){
      return DateService.getTime(fechm)
    },
    async findAllDetalleVenta () {
      this.loading = true
      try {
        const response = await DetalleVentaServices.findAll((this.page - 1), this.elementsPerPage, this.sortBy, this.sortDesc)
        this.complex.items = response.data
        this.pagination.totalElements = parseInt(response.elements)
        this.loading = false
        console.log("Detalle Venta PRODUCTS-----------------")
        console.log(this.complex.items)
      }catch (error) {
        console.log(error)
      }
    },
    searchByKeywordOption() {
      console.log('desde ===================>');
      if(this.search === null || this.startDate === null || this.endDate === null){
        this.search = ''
        this.startDate = ''
        this.endDate = ''
      }
        if(parseInt(this.search.length) > 0 && (parseInt(this.startDate.length) == 0 && parseInt(this.endDate.length) == 0)){
          console.log("Producto....."+ this.search)
          this.searchByKeyword(this.search);
        }else{
          if(parseInt(this.startDate.length) > 0 && parseInt(this.endDate.length) > 0){
            console.log("startDate....." + this.startDate);
            console.log("endDate....." + this.endDate);
            this.searchByKeyword(this.startDate, this.endDate);
          }
        }
    },
    async searchByKeyword () {
        this.loading = true
        try {
          const response = await DetalleVentaServices.search(this.search, this.startDate, this.endDate, (this.page - 1), this.elementsPerPage, this.sortBy, this.sortDesc)
          this.complex.items = response.data
          this.pagination.totalElements = parseInt(response.elements)
          this.loading = false
        } catch (error) {
          console.log(error)
        }
    },
    openModal (item) {
      this.dialog = true
      this.defaultItem = Object.assign({}, item)
    },
  }
}