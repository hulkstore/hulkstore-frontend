import ProductService from "../../../services/product/product.service";
import DeleteModal from "../../components-base/modals/delete-modal";
import ProductDetailModal from "../../components-base/modals/details-product-modal/product-detail-modal";

export default {
  title: '- Productos',
  name: "product-component",
  components: { DeleteModal, ProductDetailModal },
  data: () => ({
    search: "",
    elementsPerPage: 10,
    pagination: {},
    loading: false,
    page: 1,
    size: 5,
    dialog: false,
    dialogDelete: false,
    dialogDetail: false,
    dialogTemporada: false,
    defaultItem: {},
    deleteFunc: null,
    mensaje: "producto...",
    base: process.env.VUE_APP_IMAGE_BASE_URL,
    complex: {
      selected: [],
      headers: [
        {
          text: "#",
          value: "#",
          align: "center",
          sortable: false
        },
        {
          text: "Nombre",
          value: "nombre",
          align: "center",
          sortable: true
        },
        {
          text: "Codigo",
          value: "codigo",
          align: "center",
          sortable: true
        },
        {
          text: "Descripcion",
          value: "descripcion",
          align: "center",
          sortable: true
        },
        {
          text: "Estado en Inventario",
          value: "estado",
          align: "center",
          sortable: true
        },
        {
          text: "Acciones",
          value: "action",
          align: "center",
          sortable: false
        }
      ],
      items: []
    },
    actions: [
      {
        text: "Detalle",
        icon: "mdi-eye"
      },
      {
        text: "Editar",
        icon: "mdi-pencil"
      },
      {
        text: "Eliminar",
        icon: "mdi-close"
      }
    ],
    sortBy: [""],
    sortDesc: [true, false]
  }),
  watch: {
    page() {
      this.initialize();
    },
    elementsPerPage() {
      this.initialize();
    },
    sortDesc: {
      handler() {
        this.initialize();
      }
    }
  },
  computed: {
  },
  mounted() {
    this.initialize()
  },
  methods: {
    initialize() {
      if (parseInt(this.search.length) >= 3) {
        this.searchByKeyword(this.search);
      } else {
        if (parseInt(this.search.length) === 0) {
          this.findAllProducts();
        }
      }
    },
    async findAllProducts() {
      this.loading = true;
      try {
        const response = await ProductService.findAll(
          this.page - 1,
          this.elementsPerPage,
          this.sortBy,
          this.sortDesc
        );
        this.complex.items = response.data;
        console.log(this.complex.items);
        this.pagination.totalElements = parseInt(response.elements);
        this.loading = false;
      } catch (error) {
        console.log(error, "de aqui viene...");
      }
    },
    openModal(item, event) {
      this.defaultItem = Object.assign({}, item);
      const text = event.target.id;
      if (this.actions[2].text === text) {
        this.dialogDelete = true;
        this.deleteFunc = this.deleteItem;
      } else if (this.actions[1].text === text) {
        const id = item.id;
        this.$router.push({ name: "editar producto", params: { id } });
      } else if (this.actions[0].text === text) {
        this.dialogDetail = true;
      } else {
        if (this.actions[3].text === text) {
          this.dialogTemporada = true;
        }
      }
    },
    async deleteItem() {
      try {
        const response = await ProductService.delete(this.defaultItem.id);
        this.$notify({
          group: "foo",
          text: "El producto se elimino exitosamente! " + response,
          type: "success"
        });
      } catch (error) {
        this.$notify({
          group: "foo",
          text: "El producto no se pudo eliminar!",
          type: "error"
        });
        this.dialogDelete = false;
      } finally {
        this.dialogDelete = false;
        const response = this.complex.items.length === 1 ? this.page - 1 : this.page
        this.page = this.page > 1 ? response : 1;
        this.initialize()
      }
    },
    async searchByKeyword(event) {
      this.search = event;
      if (parseInt(this.search.length) >= 3) {
        this.loading = true;
        try {
          const response = await ProductService.search(
            this.search,
            this.page - 1,
            this.elementsPerPage,
            this.sortBy,
            this.sortDesc
          );
          this.complex.items = response.data;
          this.pagination.totalElements = parseInt(response.elements);
          this.loading = false;
        } catch (error) {
          console.log(error);
        }
      } else {
        if (parseInt(this.search.length) === 0) {
          this.findAllProducts()
        }
      }
    }
  }
};
