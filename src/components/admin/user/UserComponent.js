import UserService from "../../../services/user/user.service";
import DeleteModal from "../../components-base/modals/delete-modal";
import DetailUserModal from "../../components-base/modals/detail-user-modal";
// import CajaService from "../../../services/caja/caja.service";
// import AccountService from "../../../services/account/account.service";
import { mapState } from "vuex";

export default {
  title:"- Usuarios",
  name: "user-component",
  components: { DeleteModal, DetailUserModal },
  data() {
    return {
      search: "",
      elementsPerPage: 10,
      pagination: {},
      loading: true,
      page: 1,
      size: 5,
      dialog: false,
      dialogDelete: false,
      defaultItem: {},
      deleteFunc: null,
      base: process.env.VUE_APP_IMAGE_BASE_URL,
      defavatar: "../../../assets/person_avaar.png",
      mensaje: "usuario...",
      dialogRUC: false,
      errorUser: false,
      errorExist: false,
      complex: {
        selected: [],
        headers: [
          {
            text: "#",
            value: "#",
            align: "left",
            sortable: false
          },
          {
            text: "Avatar",
            value: "imagen",
            sortable: false,
            align: "center"
          },
          {
            text: "Usuario",
            value: "username",
            sortable: false
          },
          {
            text: "Nombres",
            value: "nombre",
            sortable: true
          },
          {
            text: "Email",
            value: "email",
            sortable: true
          },
          {
            text: "Nro. Tarjeta",
            value: "nroTarjeta",
            align: "center",
            sortable: true
          },
          {
            text: "Roles",
            value: "roles",
            sortable: false,
            align: "center",
            width: "20%"
          },
          {
            text: "Acciones",
            value: "action",
            sortable: false
          }
        ],
        items: []
      },
      actions: [
        {
          text: "Detalle",
          icon: "mdi-eye"
        },
        {
          text: "Editar",
          icon: "mdi-pencil"
        },
        {
          text: "Eliminar",
          icon: "mdi-close"
        }
      ],
      sortBy: [""],
      sortDesc: [true, false]
    };
  },
  computed: {
    ...mapState(["drawer", "accountX"]),
    formTitle() {
      return this.basic.editedIndex === -1 ? "New Item" : "Edit Item";
    }
  },
  watch: {
    page() {
      this.initialize();
    },
    elementsPerPage() {
      this.initialize();
    },
    sortDesc: {
      handler() {
        this.initialize();
      }
    },
  },
  mounted() {
    this.initialize();
  },
  methods: {
    initialize() {
      if (parseInt(this.search.length) >= 3) {
        // this.searchByKeyword(this.search);
      } else {
        if (parseInt(this.search.length) === 0) {
          this.findAllUsers();
        }
      }
    },
    async findAllUsers() {
      this.loading = true;
      try {
        const response = await UserService.findAll(
          this.page - 1,
          this.elementsPerPage,
          this.sortBy,
          this.sortDesc
        );
        this.complex.items = response.data;
        console.log(this.complex.items);
        this.pagination.totalElements = parseInt(response.elements);
        this.loading = false;
      } catch (error) {
        console.log(error);
      }
    },
    openModal(item, event) {
      this.defaultItem = Object.assign({}, item);
      console.log("----------delete--------------");
      console.log(this.defaultItem);
      const text = event.target.innerText;
      if (this.actions[2].text === text) {
        this.dialogDelete = true;
        this.deleteFunc = this.deleteFinal;
      } else if (this.actions[1].text === text) {
        const id = item.id;
        this.$router.push({ name: "editar usuario", params: { id } });
      } else {
        if (this.actions[0].text === text) {
          this.dialog = true;
        }
      }
    },
    deleteFinal() {
      try {
        const response = UserService.delete(this.defaultItem.id);
        this.dialogDelete = false;
        console.log(response);
        // this.initialize()
        this.findAllUsers()
        this.$notify({
          group: "foo",
          text: "El usuario se elimino exitosamente!",
          type: "success"
        });
      } catch (error) {
        this.$notify({
          group: "foo",
          text: "El usuario no se pudo eliminar!",
          type: "error"
        });
      }
    },
    // async searchByKeyword(event) {
    //   this.search = event;
    //   if (parseInt(this.search.length) >= 3) {
    //     this.loading = true;
    //     try {
    //       const response = await UserService.search(
    //         this.search,
    //         this.page - 1,
    //         this.elementsPerPage,
    //         this.sortBy,
    //         this.sortDesc
    //       );
    //       this.complex.items = response.data;
    //       this.pagination.totalElements = parseInt(response.elements);
    //       this.loading = false;
    //     } catch (error) {
    //       console.log(error);
    //     }
    //   } else {
    //     if (parseInt(this.search.length) === 0) {
    //       this.findAllUsersBySucursal();
    //     }
    //   }
    // },
  }
};
