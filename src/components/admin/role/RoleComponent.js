import RolesService from '../../../services/role/role.service'

export default {
  title:"- Roles de usuario",
  name: 'role-component',
  data: () => ({
    rol: [
      {
        id: 0,
        name: "",
        icon: 'mdi-account-tie',
        description: "Acceso total, altas, bajas y actualizacion de datos"
      },
      {
        id: 1,
        name: "",
        icon: 'mdi-account-cash',
        description: "Acceso a la seccion de tienda"
      },
    ],
  }),
  mounted () {
    this.findAllRoles()
  },
  methods: {
    async findAllRoles() {
      try {
        const response = await RolesService.findAllRoles()
        console.log(response);
        this.rol.map(item => item.name = response[item.id].nombre)
      } catch(error) {
        console.log(error)
      }
    }
  }
  
}
