import AccountService from "../../services/account/account.service";
import ResetPasswordInitModel from "../components-base/models/ResetPasswordInitModel";

export default {
  title: 'forgot password',
  name: "forgot-password",
  mixins: [ResetPasswordInitModel],
  data: () => ({
    email: null,
    step: 1
  }),
  methods: {
    checkForm() {
      this.$v.email.$touch();
      if (!this.$v.email.$invalid) {
        this.resetPasswordInit();
      }
    },
    async resetPasswordInit() {
      try {
        //console.log(this.email);
        await AccountService.resetPasswordInit(this.email);
        //console.log(response);
        this.$notify({
          group: "foo",
          text:
            "Se le envio un correo electronico, revise su bandeja de entrada y siga las instrucciones...",
          type: "info"
        });
        this.step = 2;
        //this.$router.push({ name: "login" });
      } catch (error) {
        this.$notify({
          group: "foo",
          text: "El correo electronico es invalido o no esta en el sistema",
          type: "error"
        });
      }
    }
  }
};
