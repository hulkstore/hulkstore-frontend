import AccountService from "../../services/account/account.service";
import ResetPasswordInitModel from "../components-base/models/ResetPasswordInitModel";

export default {
  title: 'reset password',
  name: "reset-password-component",
  mixins: [ResetPasswordInitModel],
  data: () => ({
    showPassword: false
  }),
  methods: {
    checkForm() {
      console.log(this.$route.query.key);
      this.defaultItem.key = this.$route.query.key;
      this.$v.defaultItem.$touch();
      if (!this.$v.defaultItem.$invalid) {
        console.log(this.defaultItem);
        this.resetPasswordFinish();
      }
    },
    goToLogin() {
      this.$router.push({ name: "login" });
    },
    async resetPasswordFinish() {
      try {
        const response = await AccountService.resetPasswordFinish(
          this.defaultItem
        );
        console.log(response);
        this.$notify({
          group: "foo",
          text:
            "Se ha cambiado su contraseña, ahora puede ingresar al sistema...",
          type: "success"
        });
        this.goToLogin();
      } catch (error) {
        console.log(error);
        this.$notify({
          group: "foo",
          text: "La llave expiro, solicite una llave valida!",
          type: "error"
        });
      }
    }
  }
};
