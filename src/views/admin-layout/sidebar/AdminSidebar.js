// Utilities
import { mapMutations, mapState } from "vuex";
import { routes } from "@/router";
import AccountService from "../../../services/account/account.service";
export default {
  data() {
    return {
      admins: [
        ["Management", "mdi-account-multiple-outline"],
        ["Settings", "mdi-cog-outline"]
      ],
      base: process.env.VUE_APP_IMAGE_BASE_URL,
      account: {
        id: "",
        username: "",
        nombre: "",
        email: "",
        roles: []
      }
    };
  },
  mounted() {
    this.serviceAccount();
  },
  watch: {
    // sucursal() {
    //   console.log("------------------Sucursal-----------------");
    //   // console.log(this.sucursal);
    // }
  },
  computed: {
    ...mapState(["drawer", "accountX"]),
    // ...mapState("sucursalModule", ["sucursal"]),
    // tipoSucursal() {
    //   return this.sucursal.tipoSucursal?.replace("_", " ");
    // },
    mini: {
      get: function() {
        return this.$store.state.drawer;
      },
      set: function(val) {
        this.$store.commit("SET_DRAWER", val);
      }
    },
    computedMenu() {
      return routes[4].children;
    }
  },
  methods: {
    ...mapMutations({ setAccountX: "SET_ACCOUNT" }),
    async serviceAccount() {
      try {
        const resp = await AccountService.findAccount();
        console.log(resp)
        // this.account.id = resp.id;
        // this.account.username = resp.username;
        // this.account.nombre = resp.nombre;
        // this.account.email = resp.email;
        // this.account.roles = resp.roles;
        this.account = Object.assign({}, resp)
        // if (resp.logo1) {
        //   // const img = this.base + resp.imageUrl;
        //   this.account.logo1 = resp.logo1;
        //   this.account.logo1ContentType = resp.logo1ContentType;
        // } else {
        //   this.account.logo1 = null;
        //   this.account.logo1ContentType = "";
        // }
        this.setAccountX(Object.assign({}, this.account));
      } catch (error) {
        console.log("Error data account");
      }
    },
    mostrarestado() {
      alert(routes);
    },
    created: function() {},
    authRoles(roles) {
      let nro_auths = 0;
      this.account.roles.forEach(el => {
        if (roles.includes(el)) nro_auths += 1;
      });
      return nro_auths > 0;
    }
  }
};
