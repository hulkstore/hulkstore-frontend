import { mapState, mapMutations, mapActions } from "vuex";
import jwt_decode from "jwt-decode";
// import SucursalService from "../../../services/sucursal/sucursal.service";
export default {
  data: () => ({
    nombre: "Home",
    sucursales: [],
    fullScreen: false,
    model: 1,
    overlay: false
  }),
  mounted() {
    // this.findAllSucursales();
    const user = jwt_decode(localStorage.getItem("isu-token"));
    // this.setCurrentSucursal(user.sucursalId || 1);
    /*
    user.auth.includes("GERENTE")
      ? this.setCurrentSucursal(localStorage.getItem("currentSuc"))
      : this.setCurrentSucursal(user.sucursalId);*/
    // this.setListaSucursales();
  },
  watch: {
    overlay(val) {
      val &&
        setTimeout(() => {
          this.overlay = false;
        }, 400);
    },
    // sucursal() {
    //   //this.model = localStorage.getItem("currentSuc");
    //   console.log("Cambio////");
    // }
  },
  computed: {
    ...mapState(["drawer", "accountX"]),
    // ...mapState("sucursalModule", ["listaSucursales"]),
    isFullScreen() {
      return this.fullScreen;
    },
    rutasText() {
      const { matched } = this.$route;
      return matched.map((route, index) => {
        const to =
          index === matched.length - 1
            ? this.$route.path
            : route.path || route.redirect;
        const text = this.$route.matched[index].name;
        return {
          text: text,
          to: to,
          exact: true,
          disabled: false
        };
      });
    },
    esGerente() {
      if (!this.accountX.authorities) return false;
      return this.accountX.authorities.includes("ROLE_GERENTE") || false;
    }
  },
  methods: {
    // ...mapActions("sucursalModule", ["currentSucursal"]),
    // ...mapActions("sucursalModule", ["allSucursales"]),
    // setCurrentSucursal(id) {
      // this.currentSucursal(id);
      //localStorage.setItem("currentSuc", id);
      // this.overlay = true;
      /*this.$notify({
        group: "foo",
        text: "Se cambio de sucursal...",
        type: "info"
      });*/
    // },
    // cambiarSucursal(id) {
    //   this.overlay = true;
    //   setTimeout(() => {
    //     this.overlay = false;
    //     this.setCurrentSucursal(id);
    //   }, 400);
    // },
    // setListaSucursales() {
    //   this.allSucursales();
    //   console.log(this.listaSucursales);
    // },
    ...mapMutations({ setDrawer: "SET_DRAWER" }),
    ...mapActions("authModule", ["logout"]),

    // async findAllSucursales() {
    //   try {
    //     const response = await SucursalService.findAll(0, 100, true, "desc");
    //     console.log(response.data);
    //     this.sucursales = response.data;
    //   } catch (error) {
    //     console.log(error);
    //   }
    // },
    handleFullScreen() {
      let doc = window.document;
      let docEl = doc.documentElement;
      let requestFullScreen =
        docEl.requestFullscreen ||
        docEl.mozRequestFullScreen ||
        docEl.webkitRequestFullScreen ||
        docEl.msRequestFullscreen;
      let cancelFullScreen =
        doc.exitFullscreen ||
        doc.mozCancelFullScreen ||
        doc.webkitExitFullscreen ||
        doc.msExitFullscreen;
      if (
        !doc.fullscreenElement &&
        !doc.mozFullScreenElement &&
        !doc.webkitFullscreenElement &&
        !doc.msFullscreenElement
      ) {
        requestFullScreen.call(docEl);
        this.fullScreen = true;
      } else {
        cancelFullScreen.call(doc);
        this.fullScreen = false;
      }
    }
  }
};
